/*
 *  ======== getLibs ========
 */
function getLibs(prog)
{
    if (xdc.om.$name == "cfg" && this.Settings.type == null) {
        print("warning: " + this + " is not suppling a library");
        return (null);
    }

    var lib = "lib/ed";
    if (this.Settings.type == this.Settings.ACCESS_POINT) {
        lib = "lib/ap";
    }

    return (lib + ".a" + prog.build.target.suffix);
    return (null);
}

/*
 *  ======== validate ========
 */
function validate()
{
    if (xdc.om.$name == "cfg" && this.Settings.type == null) {
        throw new Error("you must configure " + this.$name + ".Settings.type");
    }
}
