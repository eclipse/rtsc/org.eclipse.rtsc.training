metaonly module Settings
{
    enum DeviceType {
        ACCESS_POINT,
        END_DEVICE
    };

    config DeviceType type;
}
