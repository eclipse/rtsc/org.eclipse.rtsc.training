/*
 *  ======== Atomic.c ========
 */

#include <xdc/std.h>

#include "bsp.h"
#include "mrfi.h"

#include "package/internal/Atomic.xdc.h"

/*
 *  ======== Atomic_set ========
 */
Int Atomic_set(Int *pold, Int nval)
{
    Int state;
    Int old;
    
    BSP_ENTER_CRITICAL_SECTION(state);
    old = *pold;
    *pold = nval;
    BSP_EXIT_CRITICAL_SECTION(state);

    return (old);
}

/*
 *  ======== Atomic_inc ========
 */
Int Atomic_inc(Int *pold)
{
    Int state;
    Int old;
    
    BSP_ENTER_CRITICAL_SECTION(state);
    old = *pold;
    *pold = *pold + 1;
    BSP_EXIT_CRITICAL_SECTION(state);

    return (old);
}

/*
 *  ======== Atomic_dec ========
 */
Int Atomic_dec(Int *pold)
{
    Int state;
    Int old;
    
    BSP_ENTER_CRITICAL_SECTION(state);
    old = *pold;
    *pold = *pold - 1;
    BSP_EXIT_CRITICAL_SECTION(state);

    return (old);
}

/*
 *  ======== Atomic_clear ========
 */
Int Atomic_clear(Int *pold)
{
    Int state;
    Int old;
    
    BSP_ENTER_CRITICAL_SECTION(state);
    old = *pold;
    *pold = 0;
    BSP_EXIT_CRITICAL_SECTION(state);

    return (old);
}

/*
 *  ======== Atomic_set8 ========
 */
Int8 Atomic_set8(Int8 *pold, Int8 nval)
{
    Int state;
    Int8 old;
    
    BSP_ENTER_CRITICAL_SECTION(state);
    old = *pold;
    *pold = nval;
    BSP_EXIT_CRITICAL_SECTION(state);

    return (old);
}

/*
 *  ======== Atomic_inc8 ========
 */
Int8 Atomic_inc8(Int8 *pold)
{
    Int state;
    Int8 old;
    
    BSP_ENTER_CRITICAL_SECTION(state);
    old = *pold;
    *pold = *pold + 1;
    BSP_EXIT_CRITICAL_SECTION(state);

    return (old);
}

/*
 *  ======== Atomic_dec8 ========
 */
Int8 Atomic_dec8(Int8 *pold)
{
    Int state;
    Int8 old;
    
    BSP_ENTER_CRITICAL_SECTION(state);
    old = *pold;
    *pold = *pold - 1;
    BSP_EXIT_CRITICAL_SECTION(state);

    return (old);
}

/*
 *  ======== Atomic_clear8 ========
 */
Int8 Atomic_clear8(Int8 *pold)
{
    Int state;
    Int8 old;
    
    BSP_ENTER_CRITICAL_SECTION(state);
    old = *pold;
    *pold = 0;
    BSP_EXIT_CRITICAL_SECTION(state);

    return (old);
}

