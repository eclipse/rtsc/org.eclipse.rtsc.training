/*
 *  ======== Bsp.c ========
 */
#include <xdc/std.h>
#include <xdc/runtime/Startup.h>

#include "bsp.h"
#include "mrfi.h"
#include "nwk_types.h"
#include "nwk_api.h"
#include "bsp_leds.h"
#include "bsp_buttons.h"

#include "package/internal/Bsp.xdc.h"

/*
 *  ======== Bsp_Module_startup ========
 */
Int Bsp_Module_startup(Int stat)
{
    BSP_Init();
    return (Startup_DONE);
}

/*
 *  ======== Bsp_led ========
 */
Bool Bsp_led(Int id)
{
    return (id == 1 ? BSP_LED1_IS_ON() : (id == 2 ? BSP_LED2_IS_ON() : FALSE));
}

/*
 *  ======== Bsp_toggleLed ========
 */
Void Bsp_toggleLed(Int id)
{
    if (1 == id) {
        BSP_TOGGLE_LED1();
    }
    else if (2 == id) {
        BSP_TOGGLE_LED2();
    }
}

/*
 *  ======== Bsp_turnOnLed ========
 */
Void Bsp_turnOnLed(Int id)
{
    if (1 == id) {
        BSP_TURN_ON_LED1();
    }
    else if (2 == id) {
        BSP_TURN_ON_LED2();
    }
}

/*
 *  ======== Bsp_turnOffLed ========
 */
Void Bsp_turnOffLed(Int id)
{
    if (1 == id) {
        BSP_TURN_OFF_LED1();
    }
    else if (2 == id) {
        BSP_TURN_OFF_LED2();
    }
}

/*
 *  ======== Bsp_delay ========
 */
Void Bsp_delay(Int time)
{
    NWK_DELAY(time);
}

/*
 *  ======== Bsp_button ========
 */
Bool Bsp_button(Int id)
{
    return (id == 1 ? BSP_BUTTON1() : (id == 2 ? BSP_BUTTON2() : FALSE));
}

/*
 *  ======== Bsp_reset ========
 */
Void Bsp_reset(void)
{
    __disable_interrupt();
    TAR = 0x3FFF;           /* Opcode for "jmp $" */
    ((void (*)())0x170)();  /* Invalid fetch ("call #0170h") */
    ;                       /* ** Should never get here ** */   
}
