import xdc.runtime.Log;
import xdc.runtime.Diags;

/*!
 *  ======== Radio ========
 *  Wapper for the SimpliciTI radio stack
 *
 *  This module is used at runtime by applications using the
 *  Simplicity stack to communicate between devices.  By using this module, in
 *  lieu of directly calling the SimpliciTI functions, clients can easily
 *  monitor all use of the radio and avoid having to include multiple headers
 *  from various directories.
 */
module Radio {

    /*!
     *  ======== Status ========
     *  Status codes returned from this module's methods
     *
     *  These values should match the SMPL_* values defined in
     *  `Components/simpliciti/nwk/nwk_types.h`
     */
    enum Status {
        SUCCESS,
        TIMEOUT,
        BAD_PARAM,
        NOMEM,
        NO_FRAME,
        NO_LINK,
        NO_JOIN,
        NO_CHANNEL,
        NO_PEER_UNLINK,
        TX_CCA_FAIL,        /*! can't send; chan too noisy even after multiple tires */
        NO_PAYLOAD,
        NO_AP_ADDRESS,
        NO_ACK
    };

    /*!
     *  ======== Rssi ========
     *  Radio signal strength
     */
    typedef Int8 Rssi;

    /*!
     *  ======== RxMetrics ========
     *  Receive signal metrics
     */
    struct RxMetrics {
        Rssi  rssi;
        UInt8 lqi;
    };

    /*!
     *  ======== LinkId ========
     *  Link ID
     *
     *  Valid link ID's are always non-zero.
     */
    typedef UInt8 LinkId;
    
    /*!
     *  ======== ADDR_LEN ========
     *  Number of bytes in a node's address
     */
    const Int ADDR_LEN = 4;
    
    /*!
     *  ======== Addr ========
     *  Device address
     */
    struct Addr {
        Bits8  addr[ADDR_LEN];
    };
    
    /*!
     *  ======== ADDRESS ========
     *  Log a device address
     */
    config Log.Event ADDRESS = {
        mask: Diags.ENTRY,
        msg: "device address: %x:%x:%x:%x"
    };
    
    /*!
     *  ======== CallBack ========
     *  Callback function when a message is recieved
     */
    typedef UInt8 (*CallBack)(LinkId);
    
    /*!
     *  ======== setRxOn ========
     *  Turn on radio receive
     */
    Status setRxOn();

    /*!
     *  ======== sleep ========
     *  Idle the radio to lower power consumption
     *
     *  The first method callable after a call to `sleep` should always be
     *  `{@link #awake()}`.
     *
     *  @see #awake
     */
    Status sleep();

    /*!
     *  ======== awake ========
     *  Awaken the radio from sleep
     *
     *  This entry point is a direct call to eliminate the possibility
     *  of inadvertently tracing this module using the radio; the radio
     *  must not be used until it is "awake", but calling this method
     *  with entry trace will log an event _before_ the radio is awake!
     *
     *  @see #sleep
     */
 @DirectCall
    Status awake();

    /*!
     *  ======== send ========
     *  Send a message
     */
    Status send(LinkId id, UInt8 *msg, SizeT len);
    
    /*!
     *  ======== setChannel ========
     *  Change channel to specified logical channel number
     *
     *  Channel numbers begin at 0 and grow to a build time
     *  maximum (typically 3, for a total of 4 channels).
     */
    Bool setChannel(Int chan);

    /*!
     *  ======== getMetrics ========
     *  Get radio metrics for last received frame
     */
    Status getMetrics(LinkId id, RxMetrics *metrics);
    
    /*!
     *  ======== getRssi ========
     *  Get RSSI value for the current channel
     */
@DirectCall
    Rssi getRssi();

    /*!
     *  ======== receive ========
     *  Receive message
     */
    Status receive(LinkId id, UInt8 *msg, UInt8 *len);
    
    /*!
     *  ======== start ========
     *  Initialize the radio stack
     */
    Status start(CallBack cb, Addr *addr);
    
    /*!
     *  ======== link ========
     *  Ask to link into a network
     */
    Status link(LinkId *id);
    
    /*!
     *  ======== listen ========
     *  Listen for a link request
     */
    Status listen(LinkId *id);

    /*!
     *  ======== trace ========
     *  Return true if internal trace is enabled
     */
@DirectCall
    Bool trace();
}
