/*
 *  ======== Radio.c ========
 */
#include <xdc/std.h>
#include <xdc/runtime/Diags.h>

#include "bsp.h"
#include "mrfi.h"
#include "nwk_types.h"
#include "nwk_api.h"
#include "bsp_leds.h"
#include "bsp_buttons.h"

#include "package/internal/Radio.xdc.h"

/*
 *  ======== Radio_setRxOn ========
 */
Radio_Status Radio_setRxOn(void)
{
    return ((Radio_Status)SMPL_Ioctl(IOCTL_OBJ_RADIO, IOCTL_ACT_RADIO_RXON,0));
}

/*
 *  ======== Radio_setChannel ========
 */
Bool Radio_setChannel(Int chan)
{
    freqEntry_t freq;
    Bool status = TRUE;
    if (chan < 0 || chan >= NWK_FREQ_TBL_SIZE) {
        chan = 0;
        status = FALSE;
    }

    freq.logicalChan = chan;
    if (SMPL_Ioctl(IOCTL_OBJ_FREQ, IOCTL_ACT_SET, &freq) != SMPL_SUCCESS) {
        status = FALSE;
    }

    return (status);
}

/*
 *  ======== Radio_sleep ========
 */
Radio_Status Radio_sleep(void)
{
    return ((Radio_Status)SMPL_Ioctl(IOCTL_OBJ_RADIO, IOCTL_ACT_RADIO_SLEEP, ""));
}

/*
 *  ======== Radio_awake ========
 */
Radio_Status Radio_awake(void)
{
    return ((Radio_Status)SMPL_Ioctl(IOCTL_OBJ_RADIO, IOCTL_ACT_RADIO_AWAKE, ""));
}

/*
 *  ======== Radio_send ========
 */
Radio_Status Radio_send(Radio_LinkId id, UInt8 *msg, SizeT len)
{
    return ((Radio_Status)SMPL_Send(id, msg, len));
}

/*
 *  ======== Radio_receive ========
 */
Radio_Status Radio_receive(Radio_LinkId id, UInt8 *msg, UInt8 *len)
{
    return ((Radio_Status)SMPL_Receive(id, msg, len));
}

/*
 *  ======== Radio_getMetrics ========
 */
Radio_Status Radio_getMetrics(Radio_LinkId id, Radio_RxMetrics *metrics)
{
    ioctlRadioSiginfo_t sigInfo;
    Radio_Status status;
    
    sigInfo.lid = id;
    status = (Radio_Status)SMPL_Ioctl(IOCTL_OBJ_RADIO, IOCTL_ACT_RADIO_SIGINFO,
        (void *)&sigInfo);

    metrics->rssi = sigInfo.sigInfo.rssi;
    metrics->lqi = sigInfo.sigInfo.lqi;

    return (status);
}

/*
 *  ======== Radio_start ========
 */
Radio_Status Radio_start(Radio_CallBack cb, Radio_Addr *addr)
{
    if (addr != NULL) {
        /* address must be set _before_ calling SMPL_Init() */
        SMPL_Ioctl(IOCTL_OBJ_ADDR, IOCTL_ACT_SET, addr);
    }
  
    return ((Radio_Status)SMPL_Init(cb));
}

/*
 *  ======== Radio_link ========
 */
Radio_Status Radio_link(Radio_LinkId *id)
{
    return ((Radio_Status)SMPL_Link(id));
}

/*
 *  ======== Radio_listen ========
 */
Radio_Status Radio_listen(Radio_LinkId *id)
{
    return ((Radio_Status)SMPL_LinkListen(id));
}

/*
 *  ======== Radio_trace ========
 */
Bool Radio_trace(void)
{
    return (Diags_query(Diags_USER1));
}

/*
 *  ======== Radio_getRssi ========
 */
Radio_Rssi Radio_getRssi(void)
{
    Radio_Rssi dbm;
    SMPL_Ioctl(IOCTL_OBJ_RADIO, IOCTL_ACT_RADIO_RSSI, (void *)&dbm);
    return (dbm);
}
