/*!
 *  ======== Bsp ========
 *  Minimal board support
 */
@ModuleStartup
module Bsp {

    /*!
     *  ======== led ========
     *  Return TRUE iff specified LED is on
     */
    Bool led(Int id);

    /*!
     *  ======== toggleLed ========
     *  Toggle specified LED
     */
    Void toggleLed(Int id);

    /*!
     *  ======== turnOffLed ========
     *  Turn off specified LED
     */
    Void turnOffLed(Int id);

    /*!
     *  ======== turnOnLed ========
     *  Turn on specified LED
     */
    Void turnOnLed(Int id);

    /*!
     *  ======== delay ========
     */
    Void delay(Int time);

    /*!
     *  ======== button ========
     *  Returns true iff specified button is pushed
     */
    Bool button(Int id);

    /*
     *  ======== reset ========
     *  Reset the device
     */
    Void reset();
}
