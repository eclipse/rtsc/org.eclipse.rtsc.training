/*
 *  ======== getDefs ========
 */
function getDefs()
{
    check(this);

    var mcuDef = "-D__" + String(this.$module.mcu).replace(/^.*\./, "") + "__";
    var radioDef = " -DMRFI_" + String(this.$module.radio).replace(/^.*\./,"");

    var deviceType = " -D" + String(this.deviceType).replace(/^.*\./, "");

    var rxMode = "";
    if (this.rxMode != null) {
        rxMode = " -D" + String(this.rxMode).replace(/^.*\./, "");
    }
    else {
        // TODO: is rxMode only necessary for end-devices?
//      print("warning: rxMode not defined");
    }

    var defs = mcuDef + radioDef
                   + " -DMAX_HOPS=" + this.maxHopCount
                   + " -DMAX_HOPS_FROM_AP=" + this.maxHopFromAP

    /* Undocumented SimpliciTI 1.1 macro: maximum size of Network application
     * payload. Do not change unless protocol changes are reflected in
     * different maximum network application payload size.
     */
                   + " -DMAX_NWK_PAYLOAD=9"
                   + " -DMAX_APP_PAYLOAD=" + this.maxAppPayload
                   + " -DDEFAULT_LINK_TOKEN=" + this.linkToken
                   + " -DDEFAULT_JOIN_TOKEN=" + this.joinToken
                   + " -DNUM_CONNECTIONS=" + this.numConnections
                   + " -DSIZE_INFRAME_Q=" + this.inFrameQSize
                   + " -DSIZE_OUTFRAME_Q=" + this.outFrameQSize
                   + ' -DTHIS_DEVICE_ADDRESS="' + this.deviceAddress + '"'
                   + deviceType
                   + rxMode;

    if (this.deviceType == this.$module.ACCESS_POINT) {
    /* another undocumented  macro for SimpliciTI 1.1 required by
     * Components/simpliciti/nwk_applications/nwk_join.c: Must define
     * either STARTUP_JOINCONTEXT_ON or STARTUP_JOINCONTEXT_OFF
     */
    defs += " -DSTARTUP_JOINCONTEXT_ON";
    }

    if (this.isDataHub) {
        defs += " -DAP_IS_DATA_HUB"
            + " -DNUM_STORE_AND_FWD_CLIENTS=" + this.numStoreAndFwdClients;
    }

    if (this.frequencyAgility) {
        defs += " -DFREQUENCY_AGILITY";
    }

    return (defs);
}

/*
 *  ======== getIncs ========
 */
function getIncs()
{
    check(this);

    /* location of header file directories necessary to use Simplicity */
    var DIRS = [
        "bsp",
        "bsp/mcus",
        "bsp/drivers",
        "bsp/boards/EZ430RF",
        "mrfi",
        "simpliciti/nwk",
        "simpliciti/nwk_applications"
    ];

    /* create the -I options to tell the compiler where to find includes */
    var prefix = " -I " + this.$module.rootDir + "/";
    var incs = prefix + DIRS.join(prefix);
    return (incs);
}

/*
 *  ======== check ========
 */
function check(inst)
{
    checkMod(inst.$module);

    var msg = null;
    if (inst.deviceType == null) {
        msg = "you must specify " + inst.$name + ".deviceType";
    }

    if (inst.$module.rootDir == null) {
        msg = "you must specify " + inst.$module.$name + ".rootDir";
    }

    if (inst.maxAppPayload > 50 || inst.maxAppPayload < 0) {
        msg = inst.$name
            + ".maxAppPayload must be less than 50 and non-negative";
    }
    if (msg != null) {
        throw new Error(msg);
    }
}

/*
 *  ======== checkMod ========
 */
function checkMod(mod)
{
    var msg = null;

    if (mod.rootDir == null) {
        throw new Error(mod
            + ".rootDir must be set before calling any methods of " + mod);
    }

    /* TODO: check existance of rootDir and required include sub-directories */

    if (msg != null) {
        throw new Error(msg);
    }
}

/*
 *  ======== getMakePrologue ========
 */
function getMakePrologue()
{
    checkMod(this);

    return ("# vpath for SimpliciTI sources\n"
	    + "vpath %.c " + this.rootDir + "\n\n"
	    + "# vpath for local.rf sources\n"
            + "vpath %.c " + this.$package.packageRepository + "\n\n");
}
