extern void MRFI_GpioIsr(void);

/*
 *  ======== IOPORT2_ISR ========
 */
__interrupt void IOPORT2_ISR(void)
{
    /*
     *  This ISR is easily replaced.  The new ISR must simply
     *  include the following function call.
     */
    MRFI_GpioIsr();
}

__asm("\t.global IOPORT2_ISR");
__asm("\t.sect   \".ioport2\"");
__asm("\t.align  2");
__asm("_IOPORT2_vector:\n\t.field IOPORT2_ISR, 16");
