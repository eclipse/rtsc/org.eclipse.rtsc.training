/*!
 *  ======== rf ========
 *  Wireless network support
 *
 *  This package "wraps" the SimpliciTI wireless network support with
 *  modules that simplify both the building and use of SimpliciTI.
 *
 *  The `BuildOpts` module is used to re-build the SimpliciTI stack
 *  with options that control the size and number of network buffers,
 *  for example.
 *
 *  The `Radio` module is used at runtime by applications using the
 *  Simplicity stack to communicate between devices.  By using this module, in
 *  lieu of directly calling the SimpliciTI functions, clients can easily
 *  monitor all use of the radio and avoid having to include multiple headers
 *  from various directories.
 */
package local.rf
{
    module Settings;
    module BuildOpts;
    module Radio;
    module Bsp;
    module Atomic;
}
