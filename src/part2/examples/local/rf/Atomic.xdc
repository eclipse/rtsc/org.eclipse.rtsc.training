/*!
 *  ======== Atomic ========
 *  Simple atomic operations on integral values
 */
module Atomic
{
    /*!
     *  ======== set ========
     *  Atomically set an Int and return its previous value
     */
    Int set(Int *pold, Int nval);

    /*!
     *  ======== inc ========
     *  Atomically increment an Int and return its previous value
     */
    Int inc(Int *pold);

    /*!
     *  ======== dec ========
     *  Atomically decrement an Int and return its previous value
     */
    Int dec(Int *pold);

    /*!
     *  ======== clear ========
     *  Atomically clear an Int and return its previous value
     */
    Int clear(Int *pold);

    /*!
     *  ======== set8 ========
     *  Atomically set an Int8 and return its previous value
     */
    Int8 set8(Int8 *pold, Int8 nval);

    /*!
     *  ======== inc8 ========
     *  Atomically increament an Int8 and return its previous value
     */
    Int8 inc8(Int8 *pold);

    /*!
     *  ======== dec8 ========
     *  Atomically decrement an Int8 and return its previous value
     */
    Int8 dec8(Int8 *pold);

    /*!
     *  ======== clear8 ========
     *  Atomically clear an Int8 and return its previous value
     */
    Int8 clear8(Int8 *pold);
}
