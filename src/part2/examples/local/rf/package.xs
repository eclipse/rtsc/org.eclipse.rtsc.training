/*
 *  ======== close ========
 */
function close()
{
    if (xdc.om.$name == "bld") {
        var Pkg = xdc.module("xdc.bld.PackageContents");
        /* we could set the prologue here rather than require the user
         * to set it, but this might be too much magic.
         */
//      print("make prologue:" + Pkg.makePrologue);
    }
}

/*
 *  ======== validate ========
 */
function validate()
{
    if (xdc.om.$name == "cfg" && this.Settings.type == null) {
        throw new Error("you must configure " + this.$name + ".Settings.type");
    }
}
