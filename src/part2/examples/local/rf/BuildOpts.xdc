/*!
 *  ======== BuildOpts ========
 *  Compile time options for the SimpliciTI stack
 *
 *  The `BuildOpts` module is used to re-build the SimpliciTI stack
 *  with options that control the size and number of network buffers,
 *  for example.  By using this module, in lieu of setting compiler options
 *  directly, additional checks of the selected options will be made.
 */
metaonly module BuildOpts
{
    /*!
     *  ======== Radio ========
     *  Radio hardware supported
     */
    enum Radio {
        CC1100,
        CC1101,        
        CC1110,
        CC1111,
        CC2420,
        CC2430,
        CC2431,
        CC2500,
        CC2510,
        CC2511,
        CC2520
    };

    /*!
     *  ======== Mcu ========
     *  Supported micro-controllers
     */
    enum Mcu {
        MSP430C1111,
        MSP430C111,
        MSP430C1121,
        MSP430C112,
        MSP430C1331,
        MSP430C1351,
        MSP430C312,
        MSP430C313,
        MSP430C314,
        MSP430C315,
        MSP430C323,
        MSP430C325,
        MSP430C336,
        MSP430C337,
        MSP430C412,
        MSP430C413,
        MSP430E112,
        MSP430E313,
        MSP430E315,
        MSP430E325,
        MSP430E337,
        MSP430F1101,
        MSP430F110,
        MSP430F1111,
        MSP430F1121,
        MSP430F1122,
        MSP430F112,
        MSP430F1132,
        MSP430F1222,
        MSP430F122,
        MSP430F1232,
        MSP430F123,
        MSP430F133,
        MSP430F135,
        MSP430F1471,
        MSP430F147,
        MSP430F1481,
        MSP430F148,
        MSP430F1491,
        MSP430F149,
        MSP430F155,
        MSP430F156,
        MSP430F157,
        MSP430F1610,
        MSP430F1611,
        MSP430F1612,
        MSP430F167,
        MSP430F168,
        MSP430F169,
        MSP430F2001,
        MSP430F2002,
        MSP430F2003,
        MSP430F2011,
        MSP430F2012,
        MSP430F2013,
        MSP430F2035,
        MSP430F2101,
        MSP430F2111,
        MSP430F2112,
        MSP430F2121,
        MSP430F2122,
        MSP430F2131,
        MSP430F2132,
        MSP430F2232,
        MSP430F2234,
        MSP430F2252,
        MSP430F2254,
        MSP430F2272,
        MSP430F2274,
        MSP430F2330,
        MSP430F233,
        MSP430F2350,
        MSP430F235,
        MSP430F2370,
        MSP430F2410,
        MSP430F2416,
        MSP430F2417,
        MSP430F2418,
        MSP430F2419,
        MSP430F2471,
        MSP430F247,
        MSP430F2481,
        MSP430F248,
        MSP430F2491,
        MSP430F249,
        MSP430F2616,
        MSP430F2617,
        MSP430F2618,
        MSP430F2619,
        MSP430F412,
        MSP430F413,
        MSP430F415,
        MSP430F417,
        MSP430F423,
        MSP430F4250,
        MSP430F425,
        MSP430F4260,
        MSP430F4270,
        MSP430F427,
        MSP430F4351,
        MSP430F435,
        MSP430F4361,
        MSP430F436,
        MSP430F4371,
        MSP430F437,
        MSP430F447,
        MSP430F448,
        MSP430F449,
        MSP430F4783,
        MSP430F4784,
        MSP430F4793,
        MSP430F4794,
        MSP430F5418,
        MSP430F5419,
        MSP430F5435,
        MSP430F5436,
        MSP430F5437,
        MSP430F5438,
        MSP430P112,
        MSP430P313,
        MSP430P315,
        MSP430P325,
        MSP430P337
    };

    /*!
     *  ======== DeviceType ========
     *  Type of the device
     */
    enum DeviceType {
        ACCESS_POINT,   //! Access Point (-DACCESS_POINT)
        END_DEVICE,     //! End Device (-DEND_DEVICE)
        RANGE_EXTENDER  //! Range Extender (-DRANGE_EXTENDER)
    };

    /*!
     *  ======== RxMode ========
     *  Receive mode
     *
     *  For End Devices we need to specify theie Rx mode. This field is used
     *  by the Access Point (AP) in networks containing an AP to know whether 
     *  to reserve store-and-forward support for the joining End Device.
     *
     *  For sleeping devices that do _not_ poll, use RX_ALWAYS.
     *
     *  For EDs that only send use RX_ALWAYS and not RX_NEVER. The RX_NEVER
     *  support is currently incomplete. It is inteneded to support devices
     *  that can not send as opposed to devices that can but do not send.
     */
    enum RxMode {
        RX_LISTENS,     //! -DRX_LISTENS
        RX_POLLS,       //! -DRX_POLLS
        RX_NEVER,       //! -DRX_NEVER
        RX_ALWAYS       //! -DRX_ALWAYS
    }
    
    /*!
     *  ======== radio ========
     *  Radio device name
     */
    config Radio radio = CC2500;

    /*!
     *  ======== mcu ========
     *  MCU device name
     */
    config Mcu mcu = MSP430F2274;
    
    /*!
     *  ======== rootDir ========
     *  Installation directory of the SimpliciTI software
     */
    config String rootDir;
    
    /*!
     *  ======== SRCS ========
     *  This package's sources
     */
    readonly config String SRCS[] = [
        "local/rf/Atomic.c",
        "local/rf/Bsp.c",
        "local/rf/Radio.c",
        "bsp/bsp.c",
        "mrfi/mrfi.c",
        "simpliciti/nwk/nwk_globals.c",
        "simpliciti/nwk/nwk.c",
        "simpliciti/nwk/nwk_api.c",
        "simpliciti/nwk/nwk_frame.c",
        "simpliciti/nwk/nwk_QMgmt.c",
        "simpliciti/nwk_applications/nwk_freq.c",
        "simpliciti/nwk_applications/nwk_join.c",
        "simpliciti/nwk_applications/nwk_mgmt.c",
        "simpliciti/nwk_applications/nwk_security.c",
        "simpliciti/nwk_applications/nwk_ioctl.c",
        "simpliciti/nwk_applications/nwk_link.c",
        "simpliciti/nwk_applications/nwk_ping.c"
    ];

    /*!
     *  ======== getMakePrologue ========
     *  Get the make required file prologue
     */
    String getMakePrologue();
    
instance:
    /*!
     *  ======== getDefs ========
     *  Get the compiler -D options necessary to build
     *
     *  The SimpliciTI stack is a source code product that must be compiled
     *  with a set of -D options specific to the application.  This function
     *  returns the appropriate set of -D options based on the `BuildObj`
     *  instance configuration parameters.
     */
    String getDefs();
    
    /*!
     *  ======== getIncs ========
     *  Get the compiler -I options necessary to build
     *
     *  The SimpliciTI stack is a source code product that must be compiled
     *  with a set of -I options that identify various sub-directories within
     *  the  .
     *
     *  This function returns the appropriate set of -I options based on the
     *  `BuildObj.rootDir` configuration parameter.
     */
    String getIncs();

    /*!
     *  ======== maxHopCount ========
     *  Maximum number of total hops allowed
     *
     *  Maximum number of times a frame is resent before
     *  frame dropped. Each RE and the AP decrement the hop
     *  count and resend the frame.
     */
    config Int maxHopCount = 3;     // -DMAX_HOPS=3

    /*!
     *  ======== maxHopFromAP ========
     *  Max hops away from an Access Point
     *
     *  Maximum distance from an AP. Used by poll and poll response to
     *  limit network replay traffic.
     *
     *  Keeps hop count and therefore replay storms down for sending to
     *  and from polling End Devices. Also used when joining since the End
     *  Devices can't be more than 1 hop away.
     */
    config Int maxHopFromAP = 1;    //-DMAX_HOPS_FROM_AP=1

    /*!
     *  ======== maxAppPayload ========
     *  Maximum size (in bytes) of application payload
     *
     *  Absolute maximum is based on size of Rx and Tx FIFOs
     *  (50 for CC2500/CC1100 class including SoCs and
     *  113 for CC2430).
     */
    config Int maxAppPayload = 10;  //-DMAX_APP_PAYLOAD=10

    /*!
     *  ======== linkToken ========
     *  Unique token identifying a link request
     *
     *  Obtaining link access to a network device requires this
     *  value to match on all devices. It is sent in the Link
     *  message and matched in the receiving device.
     */
    config Bits32 linkToken = 0x01020304;   //-DDEFAULT_LINK_TOKEN=0x01020304

    /*!
     *  ======== joinToken ========
     *  Unique token identifying a join request
     *
     *  Joining a network requires this value to match on all
     *  devices. It is sent in the Join message and matched in
     *  the receiving Access Point.Change this to provide some
     *  access security.
     */
    config Bits32 joinToken = 0x05060708;   //-DDEFAULT_JOIN_TOKEN=0x05060708

    /*!
     *  ======== frequencyAgility ========
     *  Enable (or disable) frequency agility
     *
     *  When defined enabled support for Frequency Agility.
     *  Otherwise only the first entry in the channel table is used.
     */
    config Bool frequencyAgility = false;   // -DFREQUENCY_AGILITY

    /*!
     *  ======== numConnections ========
     *  Number of connections supported
     *
     *  Each connection supports bi-directional communication.  Access Points
     *  and Range Extenders can set this to 0 if they do not host End Devices.
     */
    config Int numConnections = 2;  //  -DNUM_CONNECTIONS=2

    /*!
     *  ======== inFrameQSize ========
     *  Size of low level queues for sent and received frames
     *
     *  This affects RAM usage.  Access points need larger input frame queue
     *  if they are supporting store-and-forward clients because the
     *  forwarded messages are held here. Two is probably enough for an
     *  End Device.
     */
    config Int inFrameQSize = 2;    // -DSIZE_INFRAME_Q=2

    /*!
     *  ======== outFrameQSize ========
     *  Size of the output frame queue
     *
     *  The output frame queue can be small since Tx is done synchronously. If
     *  an Access Point device is also hosting an End Device that sends to a
     *  sleeping peer the output queue should be larger -- the waiting frames
     *  in this case are held here. In that case the output frame queue should
     *  be bigger. One is probably enough.
     */
    config Int outFrameQSize = 2;   // -DSIZE_OUTFRAME_Q=2
    
    /*!
     *  ======== deviceAddress ========
     *  This device's address
     *
     *  The first byte is used as a filter on the CC1100/CC2500
     *  radios so THE FIRST BYTE MUST NOT BE either 0x00 or 0xFF. Also, for
     *  these radios on End Devices the first byte should be the least
     *  significant byte so the filtering is maximally effective. Otherwise
     *  the frame has to be processed by the MCU before it is recognized as
     *  not intended for the device. APs and REs run in promiscuous mode so
     *  the filtering is not done. This setting intializes a static const array
     *  of unsigned characters of length NET_ADDR_SIZE (found in nwk_types.h).
     */
    config String deviceAddress = "{0x77, 0x56, 0x34, 0x12}";   //-DTHIS_DEVICE_ADDRESS="{0x77, 0x56, 0x34, 0x12}"

    /*!
     *  ======== deviceType ========
     *  Specify Access Point or End Device
     */
    config DeviceType deviceType;

    /*!
     *  ======== rxMode ========
     *  Receive mode
     */
    config RxMode rxMode;

    /*!
     *  ======== isDataHub ========
     *  Is this device a data hub
     *
     *  In the spcial case in which the Access Point is a data hub, the AP 
     *  will automatically listen for a link each time a new device joins
     *  the network. this is a special case scenario in which all End Device
     *  peers are the AP and every ED links to the AP. in this scenario the
     *  ED must automatically try and link after the join reply.
     */
    config Bool isDataHub = false;  // -DAP_IS_DATA_HUB

    /*!
     *  ======== numStoreAndFwdClients ========
     *  Store and forward support: number of clients
     */
    config Int numStoreAndFwdClients = 3; // -DNUM_STORE_AND_FWD_CLIENTS=3
}
