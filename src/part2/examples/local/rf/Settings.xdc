/*!
 *  ======== Settings ========
 *  Configuration time options
 */
metaonly module Settings
{
    enum DeviceType {
        ACCESS_POINT,   //! program is an Access Point
        END_DEVICE      //! program is an End Device
    };

    /*!
     *  ======== type ========
     *  Application type
     *
     *  Simplicity programs are either an Access Point or an End Device.
     *  Applications are composed of multiple programs that include a mix
     *  of both Access Points and End Devices.
     *
     *  This option selects which type of device a program will be.
     */
    config DeviceType type;
}
