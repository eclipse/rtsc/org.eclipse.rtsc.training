/*!
 *  ======== Main ========
 *  Graphical Log Event Display Tool
 *
 *  This package provides a graphical event decoder for `xdc.runtime.Log`
 *  events that are output to a serial port.
 */
metaonly module Main inherits xdc.tools.ICmd {

    override config String usage[] = [
        '[-c commPort] [-n outHist] progImage ...',
    ];

instance:

    //!MSP430 Application UART COM Port
    @CommandOption('c')
    config String commPort = "COM4";

    //!Average amount of output history to retain
    @CommandOption('n')
    config Int outHist = 200;

}

