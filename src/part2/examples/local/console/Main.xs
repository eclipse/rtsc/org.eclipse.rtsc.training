/* 'xs' command invocation only */
if (xdc.om.$name == null) {

/* ---------------- GLOBAL STATE ---------------- */

var w_display, w_shell;
var app = {};

/* ---------------- OUTPUT OBJECT ---------------- */

function Output(progName)
{
    this.progName = progName;

    this.locked = false;
    this.metaflg = false;
    this.maximized = false;
    this.style = null;
    this.text = "";

    this.frags = [];
    this.styles = [];

    this.w_comp = null;
    this.w_btnWin = null;
    this.w_txtOut = null;
}

/* ---------------- SWT ALIASES ---------------- */

var custom = xdc.jre.org.eclipse.swt.custom;
var graphics = xdc.jre.org.eclipse.swt.graphics;
var layout = xdc.jre.org.eclipse.swt.layout;
var widgets = xdc.jre.org.eclipse.swt.widgets;

var SWT = xdc.jre.org.eclipse.swt.SWT;
var GD = layout.GridData;

/* ---------------- DISPLAY RESOURCES ---------------- */

w_display = widgets.Display();

var ICO_DELETE = graphics.Image(w_display, xdc.findFile('local/console/files/delete_obj.gif'));
var ICO_INFO = graphics.Image(w_display, xdc.findFile('local/console/files/info.png'));
var ICO_LOCK = graphics.Image(w_display, xdc.findFile('local/console/files/lock.png'));
var ICO_MAXIMIZE = graphics.Image(w_display, xdc.findFile('local/console/files/single.png'));
var ICO_RESTART = graphics.Image(w_display, xdc.findFile('local/console/files/reload.png'));
var ICO_RESTORE = graphics.Image(w_display, xdc.findFile('local/console/files/tile.png'));
var ICO_SOG = graphics.Image(w_display, xdc.findFile('xdc/tools/sg/icons/sog.ico'));

var CUR_HAND = graphics.Cursor(w_display, SWT.CURSOR_HAND);

var COL_BEIGE = graphics.Color(w_display, 255, 255, 180);
var COL_BLUE = w_display.getSystemColor(SWT.COLOR_BLUE);
var COL_GREEN = w_display.getSystemColor(SWT.COLOR_DARK_GREEN);
var COL_GRAY = w_display.getSystemColor(SWT.COLOR_DARK_GRAY);

var $d = w_display.getSystemFont().getFontData()[0]; $d.setStyle(SWT.BOLD);
var FON_LABEL = graphics.Font(w_display, $d);
var FON_OUTPUT = graphics.Font(w_display, "Courier New", 8, SWT.NORMAL);

var $s = custom.StyleRange(); $s.foreground = COL_BLUE;
var STY_LOGMSG = $s;
$s = custom.StyleRange(); $s.foreground = COL_GREEN; $s.fontStyle = SWT.BOLD;
var STY_LOGPID = $s;
$s = custom.StyleRange(); $s.foreground = COL_GRAY;
var STY_LOGEVT = $s;


/* ---------------- WIDGET TREE ---------------- */

function widgetsInit()
{
    function mkToolBar(prnt, style)
    {
        var bar = widgets.ToolBar(prnt, style);
        bar.setCursor(CUR_HAND);

        return (bar);
    }

    function mkToolBut(tb, style, icon, tip, eflg)
    {
        var but = widgets.ToolItem(tb, style);

        but.setImage(icon);
        but.setToolTipText(tip);
        but.setEnabled(eflg ? true : false);

        return (but);
    }

    var DISABLED = false;
    var ENABLED = true;

    var $w$stack = [];
    var $w = null;

    function WID(w) { $w$stack.push($w); $w = w; return $w; }
    function END() { $w = $w$stack.pop(); return $w; }

    w_shell = /* Shell */
        WID(widgets.Shell(w_display, SWT.SHELL_TRIM | SWT.ON_TOP));
            $w.setLayout(layout.GridLayout());
            $w.setText('ez430 Console');
            $w.setImage(ICO_SOG);
            $w.setSize(600, 600);
            $w.setLocation(400, 0);
            $w.addShellListener(function(e, m){ appQuit(e, m); });

            WID(mkToolBar($w, SWT.HORIZONTAL | SWT.FLAT));
                $w.setLayoutData(layout.GridData(GD.HORIZONTAL_ALIGN_END));
    app.w_btnMeta = /* Button */
                WID(mkToolBut($w, SWT.CHECK, ICO_INFO, 'Show meta-data', ENABLED));
                    $w.addListener(SWT.Selection, function(e){ for each (var o in app.outputs) o.meta(); });
                END();
    app.w_btnLock = /* Button */
                WID(mkToolBut($w, SWT.CHECK, ICO_LOCK, 'Lock output', ENABLED));
                    $w.addListener(SWT.Selection, function(e){ for each (var o in app.outputs) o.lock(); });
                END();
                WID(widgets.ToolItem($w, SWT.SEPARATOR));
                END();
                WID(mkToolBut($w, SWT.PUSH, ICO_DELETE, 'Clear output', ENABLED));
                    $w.addListener(SWT.Selection, function(e){ for each (var o in app.outputs) o.clear(); });
                END();
                WID(mkToolBut($w, SWT.PUSH, ICO_RESTART, 'Reload arguments', ENABLED));
                   $w.addListener(SWT.Selection, function(e){ decoderSetup(); });
                END();
            END();

    app.w_sash = /* SashForm */
            WID(custom.SashForm($w, SWT.VERTICAL));
                $w.setLayoutData(layout.GridData(GD.FILL_BOTH));

    app.outputs = [];
    var weights = [];
    for (var i = 0; i < app.args.length; i++) {
        var output = new Output(app.args[i]);
        app.outputs.push(output);
        weights.push(1);

        output.w_comp = /* Composite */
                WID(widgets.Composite($w, SWT.BORDER));
                    $w.setLayoutData(layout.GridData(GD.FILL_BOTH));
                    $w.setLayout(layout.GridLayout(2, false));
                    /* CLabel */
                    WID(custom.CLabel($w, SWT.NULL));
                        $w.setText(app.args[i] + ' output');
                        $w.setFont(FON_LABEL);
                    END();
                    /* ToolBar */
                    WID(mkToolBar($w, SWT.HORIZONTAL | SWT.FLAT));
                        $w.setLayoutData(layout.GridData(GD.HORIZONTAL_ALIGN_END));
        output.w_btnWin = /* Button */
                        WID(mkToolBut($w, SWT.PUSH, ICO_MAXIMIZE, 'Maximize', ENABLED));
                            $w.setData(output);
                            $w.addListener(SWT.Selection, function(e){ e.widget.getData().maximize(); });
                        END();
                    END();
        output.w_txtOut = /* Text */
                    WID(custom.StyledText($w, SWT.BORDER | SWT.MULTI | SWT.READ_ONLY | SWT.V_SCROLL));
                        var $gd = layout.GridData(GD.FILL_BOTH);
                        $gd.horizontalSpan = 2;
                        $w.setLayoutData($gd);
                        $w.setBackground(COL_BEIGE);
                        $w.setFont(FON_OUTPUT);
                    END();
                END();
    }
            $w.setWeights(weights);
            END();


            /* Composite */
            WID(widgets.Composite($w, SWT.BORDER));
                $w.setLayoutData(layout.GridData(GD.FILL_HORIZONTAL));
                $w.setLayout(layout.GridLayout());
                /* CLabel */
                WID(custom.CLabel($w, SWT.NULL));
                    $w.setText(app.args[0] + ' input');
                    $w.setFont(FON_LABEL);
                END();
    app.w_txtIn = /* Text */
                WID(widgets.Text($w, SWT.BORDER | SWT.SINGLE));
                    $w.setLayoutData(layout.GridData(GD.FILL_BOTH));
                    $w.addListener(SWT.DefaultSelection, function(e){ inputSend(); });
                END();
            END();
        END();
}


/* ---------------- FUNCTIONS ---------------- */

/*
 *  ======== appQuit ========
 */
function appQuit(event, method)
{
    if (method == 'shellClosed') {
        app.runstate = 'STOPPING';
        event.doit = false;
    }
}

/*
 *  ======== decoderSetup ========
 */
function decoderSetup()
{
    app.decoders = [];
    app.evtrecs = [];

    for each (var exeFile in app.args) {

        xdc.loadPackage('xdc.rta');
        xdc.loadPackage('xdc.rov');
        xdc.loadPackage('ti.targets.omf.cof');

        var Decoder = xdc.jre.xdc.rta.Decoder;
        var Vers = xdc.jre.xdc.services.global.Vers;

        try {
            var m = Vers.getWhatString(exeFile).match(/__ASM__ = (.+)$/m);
        }
        catch (e) {
            app.cmdr.error("can't read the executable '" + exeFile + "'");
        }

        if (!m) {
            app.cmdr.error("'" + exeFile 
                           + "' contains no assembly information");
        }

        var rtaFile = m[1] + ".rta.xml";
        if (!java.io.File(rtaFile).exists()) {
            app.cmdr.error("'" + exeFile + "' contains no RTA data");
        }

        var dec = new Decoder();
        var err = String(dec.parse(rtaFile, exeFile));
        if (err) {
            app.cmdr.error("can't create decoder for '" 
                           + exeFile + "': " + err);
        }

        app.decoders.push(dec);

        app.evtrecs.push(
            java.lang.reflect.Array.newInstance(
                java.lang.Byte.TYPE, dec.getTargetEventRecSize())
        );
    }
}

/*
 *  ======== inputSend ========
 */
function inputSend()
{
    app.cmdLine = String(app.w_txtIn.getText());
    if (app.cmdLine.length > 0) {
        java.lang.Thread(uartWrite).start();
        app.w_txtIn.setText("");
    }
}

/*
 *  ======== Output.doWrite ========
 */
Output.prototype.doWrite = function()
{
    if (this.frags.length == app.inst.outHist * 2) {
        this.trim();
    }

    if (!this.locked) {
        var sr = null;
        if (this.style) {
            sr = this.style.clone();
            sr.start = this.w_txtOut.getCharCount();
            sr.length = this.text.length;
        }
        this.w_txtOut.append(this.text);
        this.frags.push(this.text);
        this.styles.push(sr);
        if (sr) {
            this.w_txtOut.setStyleRange(sr);
        }
        this.w_txtOut.setSelection(this.w_txtOut.getCharCount());
        this.w_txtOut.showSelection();
    }
    this.text = "";
    this.style = null;
};

/*
 *  ======== Output.clear ========
 */
Output.prototype.clear = function()
{
    this.w_txtOut.setText("");

    this.style = null;
    this.text = "";

    this.frags = [];
    this.styles = [];
};

/*
 *  ======== Output.lock ========
 */
Output.prototype.lock = function()
{
    this.locked = app.w_btnLock.getSelection();
    app.w_btnLock.setToolTipText(
        this.locked ? "Unlock output" : "Lock output");
};

/*
 *  ======== Output.maximize ========
 */
Output.prototype.maximize = function()
{
    this.maximized ^= true;

    app.w_sash.setMaximizedControl(this.maximized ? this.w_comp : null);

    this.w_btnWin.setImage(this.maximized ? ICO_RESTORE : ICO_MAXIMIZE);
    this.w_btnWin.setToolTipText(this.maximized ? "Restore" : "Maximize");
};


/*
 *  ======== Output.meta ========
 */
Output.prototype.meta = function()
{
    this.metaflg = app.w_btnMeta.getSelection();
    app.w_btnMeta.setToolTipText(
        this.metaflg ? "Hide meta-data" : "Show meta-data");
};

/*
 *  ======== Output.trim ========
 */
Output.prototype.trim = function()
{
    for (var i = 0; i < app.inst.outHist; i++) {
        this.frags.shift();
        this.styles.shift();
    }

    this.w_txtOut.setText("");

    for (var i = 0; i < app.inst.outHist; i++) {
        var sr = this.styles[i];
        if (sr) {
            sr.start = this.w_txtOut.getCharCount();
            sr.length = this.frags[i].length;
        }
        this.w_txtOut.append(this.frags[i]);
        if (sr) {
            this.w_txtOut.setStyleRange(sr);
        }
    }
};

/*
 *  ======== Output.write ========
 */
Output.prototype.write = function(text, style)
{
    this.text = text;
    this.style = style;

    var output = this;
    w_display.syncExec(function(){ output.doWrite(); });
};

/*
 *  ======== readerStart ========
 */
function readerStart()
{
    var buf = java.lang.reflect.Array.newInstance(java.lang.Byte.TYPE, 128);
    var idx = 0;
    var len = 0;

    var nextByte = function()
    {
        if (idx == len) {
            idx = 0;
            for (;;) {
                len = app.inStr.read(buf);
                if (app.runstate == 'RUNNING' && len > 0) {
                    break;
                }
                if (app.runstate == 'STOPPING') {
                    app.runstate = 'HALTED';
                }
            }
        }
        return len == -1 ? -1 : buf[idx++];
    };

    var scanner = function()
    {
        var decoder;    /* the current decoder */
        var record;     /* the current event buffer */
        var output;     /* the current output window */
        var eid = 0;

        const NUL = 0;
        const SOH = 1;
        const STX = 2;
        const ETX = 3;
        const EOT = 4;

        const NL = 10;

        var text = "";

        for (;;) {
            var b = nextByte();
            if (b == -1) {
                break;    /* end-of-stream */
            }

            if (b == SOH) {
                var decId = nextByte();
                decoder = app.decoders[decId];
                record = app.evtrecs[decId];
                output = app.outputs[decId];

                /* if decId is bogus skip this byte */
                if (decoder == null || record == null) {
                    continue;
                }

                for (var i = 0; i < record.length; i++) {
                    record[i] = nextByte();
                }
                var event = decoder.apply(record);
                output.write(event.getEventMsg() + "\n", STY_LOGMSG);
                if (output.metaflg) {
                    text = "\t";
                    text += " id = " + eid++;
                    text += " module = " + event.getModuleName();
                    text += " event = " + event.getEventName();
                    text += "\n";
                    output.write(text, STY_LOGEVT);
                    text = "";
                }
                continue;
            }

            /* ignore control characters */
            if (b <= 4) {
                continue;
            }

            output = app.outputs[0];
            text += String.fromCharCode(b);
            if (b == NL || text.length > 1024) {
                output.write(text, null);
                if (output.metaflg && b == NL) {
                    text = "\t";
                    text += " id = " + eid;
                    text += "\n";
                    output.write(text, STY_LOGEVT);
                }
                if (b == NL) eid++;
                text = "";
            }
        }
    };

    app.runstate = 'RUNNING';
    java.lang.Thread(scanner).start();
}

/*
 *  ======== uartWrite ========
 */
function uartWrite()
{
    for (var i = 0; i < app.cmdLine.length; i++) {
        app.outStr.write(app.cmdLine.charCodeAt(i));
    }
    app.outStr.write(10);
}

/*
 *  ======== uartOpen ========
 */
function uartOpen(uart)
{
    try {
        var SerialPort = Packages.gnu.io.SerialPort;

        var portId = Packages.gnu.io.CommPortIdentifier.getPortIdentifier(uart);
        var port = portId.open("local.console", 2000);

        port.setSerialPortParams(9600, 
                                 SerialPort.DATABITS_8, SerialPort.STOPBITS_1,
                                 SerialPort.PARITY_NONE);

        app.inStr = port.getInputStream();
        app.outStr = port.getOutputStream();
    }
    catch(e) {
        app.cmdr.error("can't open " + uart);
    }
}

/* ---------------- MAIN ---------------- */

/*
 *  ======== run ========
 */
function run(cmdr, args)
{
    app.cmdr = cmdr;
    app.args = args;
    app.inst = this;

    if (args.length == 0) {
        app.cmdr.usage();
    }

    widgetsInit();
    decoderSetup();
    uartOpen(this.commPort);
    readerStart();

    w_shell.open();
    app.w_txtIn.setFocus();

    while (app.runstate != 'HALTED') {
        try {
            if (!w_display.readAndDispatch()) {
                w_display.sleep();
            }
        }
        catch(e) {
            print(e);
        }
    }

    w_shell.close();
    w_display.dispose();

    java.lang.System.exit(0);
}

}    /* xdc.om.$name == null */
