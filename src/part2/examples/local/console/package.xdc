requires xdc.tools.sg;

/*!
 *  ======== console ========
 *  Interactive GUI xdc.runtime.Log event decoder
 *
 *  @b(Usage)
 *  @p(code)
 *      xs local.console [-c serialPortName] [-n historyLen] [progImage ...]
 *  @p
 *
 *  This package provides a graphical event decoder for xdc.runtime.Log
 *  events that are output to a serial port.
 *
 *  @a(Examples)
 *  The following example displays events from the program named
 *  `hello.x430` that are received from comport `COM3`.
 *  @p(code)
 *      xs local.console -c COM3 hello.x430
 *  @p
 *
 *  The following example displays events from two programs (named
 *  `mon_AP.x430` and `mon_ED.x430`) in two separate windows where
 *  event data is again received from comport `COM3`.
 *  @p(code)
 *      xs local.console -c COM3 mon_AP.x430 mon_ED.x430
 *  @p
 */
package local.console {
    module Main;
}

