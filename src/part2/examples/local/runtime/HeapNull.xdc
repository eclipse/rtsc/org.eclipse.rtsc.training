/* --COPYRIGHT--,ESD
 *  Copyright (c) 2008 Texas Instruments. All rights reserved. 
 *  This program and the accompanying materials are made available under the 
 *  terms of the Eclipse Public License v1.0 and Eclipse Distribution License
 *  v. 1.0 which accompanies this distribution. The Eclipse Public License is
 *  available at http://www.eclipse.org/legal/epl-v10.html and the Eclipse
 *  Distribution License is available at 
 *  http://www.eclipse.org/org/documents/edl-v10.php.
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 * --/COPYRIGHT--*/
/*
 *  ======== HeapNull.xdc ========
 */

import xdc.runtime.Assert;
import xdc.runtime.Error;

/*!
 *  ======== HeapNull ========
 *  Empty heap
 */
module HeapNull inherits xdc.runtime.IHeap {

    /*!
     *  ======== A_zeroSize ========
     *  Assert that the `{@link #size}` is non-zero on the create
     */     
    config Assert.Id A_zeroSize  =
            {msg: "HeapNull_create cannot have a zero size value"};
        
instance:

    /*!
     *  ======== create ========
     *  Create a `HeapNull` heap
     *
     *  This heap is a growth-only heap that is intended to be used by
     *  systems that never delete objects or free memory.  Objects can be
     *  created at runtime based on values determined at runtime, but
     *  objects can not be deleted.
     *
     *  @see HeapNull#Params
     */
    create();
    
    /*!
     *  ======== alloc ========
     *  Allocate a block of memory from the heap.
     *
     *  @a(Constraints)
     *  The alignment must be a power of 2.
     *
     *  @see IHeap#alloc
     */
    override Ptr alloc(SizeT size, SizeT align, Error.Block *eb);    

    /*!
     *  ======== free ========
     *  Free a block of memory back to the heap.
     *
     *  This is a growth only heap. Calling the `HeapNull_free` function
     *  will result in a `{@link #E_freeError}` error unless
     *  `{@link #freeError}` is set to `false`.
     *
     *  @see IHeap#free
     */
    override Void free(Ptr block, SizeT size);
    
    /*!
     *  ======== isBlocking ========
     *  Can this heap block the caller
     *
     *  @a(returns)
     *  `HeapNull` always returns `FALSE` since it never blocks on a
     *  resource.
     */
    override Bool isBlocking();    
}
