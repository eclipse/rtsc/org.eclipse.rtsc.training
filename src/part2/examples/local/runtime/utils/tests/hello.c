#include <xdc/runtime/System.h>
#include <local/runtime/utils/Clock.h>

#define LOOPCOUNT   10

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    int loops = LOOPCOUNT;
    volatile int j;
    Clock_TimeValue time = Clock_getTime();
    Clock_TimeValue delta;
    
    System_printf("hello world\n");

    for (;;) {
        time = Clock_getTime();
        for (j = loops; j > 0; j--);
        delta = Clock_getTime() - time;
        
        System_printf("loop count = %d, elapsed time: %d, l/t: %d\n", 
            loops, delta, loops/delta);
        loops = (loops + 10) & 0x1fff;
    }
}
