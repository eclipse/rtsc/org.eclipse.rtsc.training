import xdc.runtime.Log;
import xdc.runtime.Diags;

/*!
 *  ======== Stack ========
 *  Stack monitoring functions
 */
@ModuleStartup
module Stack
{
    /*!
     *  ======== Status ========
     *  Stack status information buffer
     */
    struct Status {
        Int unused;     /*! number of words never used (so far) */
        Int used;       /*! worst-case number of words used (so far) */
        Int curDepth;   /*! current number of words on the stack */
    }
    
    /*!
     *  ======== UNUSED ========
     *  Unused stack space event
     */
    config Log.Event UNUSED = {
        mask: Diags.USER1,
        msg: "unused stack space = %d words"
    };

    /*!
     *  ======== check ========
     *  Return non-zero is stack pointer is within allocated stack
     *
     *  If this function returns 0, the current stack pointer is pointing
     *  to a location _outside_ the caller's allocated stack.
     */
    @DirectCall
    Bool check();

    /*!
     *  ======== fill ========
     *  Fill unused stack with initial value
     *
     *  This function is called at startup and may be called at runtime
     *  to re-initialize the stack.  However, interrupts must be disabled
     *  during this process (to prevent corruption of ISR state).
     */
    Void fill();
    
    /*!
     *  ======== getUnused ========
     *  Get number of words of unused stack space
     */
    Int getUnused();

    /*!
     *  ======== getStatus ========
     *  Get number of words of unused stack space
     */
    Void getStatus(Status *stat);
}
