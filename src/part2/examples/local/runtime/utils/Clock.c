/*
 *  ======== Clock.c ========
 */
#include <xdc/runtime/Startup.h>

#include "package/internal/Clock.xdc.h"


#if 0
#include <ti/apps/msp430.h>
#else
#define CCIFG       (0x0001)    /* Capture/compare interrupt flag */
#define MC_1        (1*0x10u)   /* Timer A/B mode control: 1 - Up to CCR0 */
#define TASSEL_1    (1*0x100u)  /* Timer A/B clock source select: 1 - ACLK  */

/************************************************************
* Timer A3
************************************************************/
#define TAIV    (*(volatile UInt16 *)0x012E)
#define TACTL   (*(volatile UInt16 *)0x0160)
#define TACCTL0 (*(volatile UInt16 *)0x0162)
#define TACCTL1 (*(volatile UInt16 *)0x0164)
#define TACCTL2 (*(volatile UInt16 *)0x0166)
#define TAR     (*(volatile UInt16 *)0x0170)
#define TACCR0  (*(volatile UInt16 *)0x0172)
#define TACCR1  (*(volatile UInt16 *)0x0174)
#define TACCR2  (*(volatile UInt16 *)0x0176)

/************************************************************
* Timer B3
************************************************************/
#define TBIV    (*(volatile UInt16 *)0x011E)
#define TBCTL   (*(volatile UInt16 *)0x0180)
#define TBCCTL0 (*(volatile UInt16 *)0x0182)
#define TBCCTL1 (*(volatile UInt16 *)0x0184)
#define TBCCTL2 (*(volatile UInt16 *)0x0186)
#define TBR     (*(volatile UInt16 *)0x0190)
#define TBCCR0  (*(volatile UInt16 *)0x0192)
#define TBCCR1  (*(volatile UInt16 *)0x0194)
#define TBCCR2  (*(volatile UInt16 *)0x0196)
#endif

#define BSP_TIMER_CLK_MHZ   8
#define PERIOD_1SEC         12000u

static Int ticks = 0;

/*
 *  ======== Clock_Module_startup ========
 */
Int Clock_Module_startup(Int State)
{
    /* 
     * We use Timer_B since BSP uses Timer_A
     */
//    TBCCR0 = PERIOD_1SEC;       /* ~1 second */
    TBCCR0 = 0xffff;
    TBCTL = TASSEL_1 + MC_1;    /* ACLK, upmode */
    TBR = 0;                    /* initial count */
    
    return (Startup_DONE);
}

/*
 *  ======== Clock_getTime ========
 */
Clock_TimeValue Clock_getTime()
{
    return (((UInt16)ticks * PERIOD_1SEC) + TBR);
}

/*
 *  ======== Clock_tick ========
 */
Void Clock_tick(void)
{
    ticks++;
}

/*
 *  ======== Clock_delay ========
 */
Void Clock_delay(UInt16 usec)
{
    TBR = 0; /* initial count */
    TBCCR0 = BSP_TIMER_CLK_MHZ * usec; /* compare count. (delay in ticks) */
    
    /* Start the timer in UP mode */
    TBCTL |= MC_1;
    
    /* Loop till compare interrupt flag is set */
    while(!(TBCCTL0 & CCIFG));
    
    /* Stop the timer */
    TBCTL &= ~(MC_1);
    
    /* Clear the interrupt flag */
    TBCCTL0 &= ~CCIFG;
}
