/*
 *  ======== Clock.xdc ========
 */

/*!
 *  ======== Clock ========
 *  Basic "wall clock" functions
 */
@ModuleStartup

module Clock {
    /*! 
     *  ======== TimeValue ========
     *  Time value type
     */
    typedef UInt16 TimeValue;
 
    /*!
     *  ======== getTime ========
     *  Get the current time
     */
    TimeValue getTime();

    /*!
     *  ======== tick ========
     *  Advance time by one "tick"
     *
     *  This function is called by the application to advance time
     *  by one timer period
     */
    Void tick();
}
