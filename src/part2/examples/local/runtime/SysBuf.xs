/*
 *  ======== SysBuf.xs ========
 */

var Memory = null;
 
/*
 *  ======== module$static$init ========
 */
function module$static$init(obj, params)
{
    /* set size and placement of SysBuf's output buffer, outbuf */
    obj.outbuf.length = params.bufSize;
    Memory.staticPlace(obj.outbuf, 0, params.sectionName);

    /* clear the output buffer */
    for (var i = 0; i < obj.outbuf.length; i++) {
        obj.outbuf[i] = 0;
    }

    /* define a convenient alias for the buffer */
    if (this.bufName != null) {
        Program.global[this.bufName] = obj.outbuf;
    }

    /* initialize output index state */
    obj.outidx = 0;
}

/*
 *  ======== module$use ========
 */
function module$use(obj, params)
{
    Memory = xdc.module('xdc.runtime.Memory');
}
