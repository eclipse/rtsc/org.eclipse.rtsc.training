/* --COPYRIGHT--,ESD
 *  Copyright (c) 2008 Texas Instruments. All rights reserved. 
 *  This program and the accompanying materials are made available under the 
 *  terms of the Eclipse Public License v1.0 and Eclipse Distribution License
 *  v. 1.0 which accompanies this distribution. The Eclipse Public License is
 *  available at http://www.eclipse.org/legal/epl-v10.html and the Eclipse
 *  Distribution License is available at 
 *  http://www.eclipse.org/org/documents/edl-v10.php.
 *
 *  Contributors:
 *      Texas Instruments - initial implementation
 * --/COPYRIGHT--*/
/*
 *  ======== LoggerCallback.c ========
 */
#include <xdc/std.h>

#include <xdc/runtime/Log.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/System.h>

#include "package/internal/LoggerCallback.xdc.h"

/*
 *  ======== Instance_init =========
 */
Void LoggerCallback_Instance_init(LoggerCallback_Object *obj,
    const LoggerCallback_Params *prms)
{
}

/*
 *  ======== write4 =========
 */
Void LoggerCallback_write4(LoggerCallback_Object *obj, Types_Event evt,
    IArg a1, IArg a2, IArg a3, IArg a4)
{
    Log_EventRec evrec;
    
    LoggerCallback_TimestampProxy_get64(&evrec.tstamp);

    evrec.serial = 0;
    evrec.evt = evt;
    evrec.arg[0] = a1;
    evrec.arg[1] = a2;
    evrec.arg[2] = a3;
    evrec.arg[3] = a4;

    if (LoggerCallback_callbackFxn != NULL) {
        LoggerCallback_callbackFxn(&evrec);
    }
}

/*
 *  ======== write8 =========
 */
Void LoggerCallback_write8(LoggerCallback_Object *obj, Types_Event evt,
    IArg a1, IArg a2, IArg a3, IArg a4, IArg a5, IArg a6, IArg a7, IArg a8)
{
    System_abort("only 4 arguments are supported\n");
}
