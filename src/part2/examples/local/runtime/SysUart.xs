/*
 *  ======== SysUart.xs ========
 */

/*
 *  ======== module$static$init ========
 */
function module$static$init(obj, params)
{
    /* initialize module's internal state */
    obj.lineIdx = 0;
    if (params.lineSize > 0) {
        params.lineBuf.length = params.lineSize + 1;
    }
}

/*
 *  ======== module$use ========
 */
function module$use()
{
    xdc.useModule("xdc.runtime.Startup");
    
    /* need to initialize the UART's Rx interrupt vector */
    Program.sectMap[".usci_rx"] = {loadSegment: "USCI_RX"};
    Program.sectMap[".reset"] = {loadSegment: "RESET"};
}
