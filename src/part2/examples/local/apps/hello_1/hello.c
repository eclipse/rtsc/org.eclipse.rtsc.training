#include <xdc/runtime/Log.h>
#include <xdc/runtime/Diags.h>

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    Log_print0(Diags_ENTRY, "hello world");
    
    return (0);
}
