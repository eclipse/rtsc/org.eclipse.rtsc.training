import xdc.runtime.Diags;
import xdc.runtime.Log;

/*!
 *  ======== Msgs ========
 *  Simple Log Events
 */

module Msgs {
    config Log.Event HELLO = {
        mask: Diags.ENTRY,
        msg: "hello world"
    };

    config Log.Event GOODBYE = {
        mask: Diags.ENTRY,
        msg: "goodbye cruel world, status: %d"
    };
}
