#include <xdc/runtime/Log.h>

#include "Msgs.h"

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    /* say hello */
    Log_write0(Msgs_HELLO);

    /* and goodbye */
    Log_write1(Msgs_GOODBYE, 0);
    
    return (0);
}
