#include <msp430x22x4.h>
#include "vlo_rand.h"

unsigned int randInt;
unsigned int bitCounter;

//monobit variables
int numberOfOnes = 0;
int numberOfZeros = 0;

// poker test variables
float pokerBuckets[16];  // m=4, k=5,000
float pokerResult;

// runs test variables
int lastBit = 0x0f;
int currentRun = 1;
int runLengthsZero[6];
int runLengthsOne[6];
int failLongTest = 0;

void monobit(unsigned int);
void poker(unsigned int);
float computePoker(void);
void run( unsigned int );

void main(void)
{
  volatile int delay;
  WDTCTL = WDTPW + WDTHOLD;                 // Stop Watchdog Timer
  for( bitCounter = 1250; bitCounter != 0; bitCounter--)
  {
    randInt = TI_getRandomIntegerFromVLO();
    monobit( randInt );
    run( randInt );
    poker( randInt );
  }
  pokerResult = computePoker();
   P1DIR = 0x01;
  _NOP();                                   // SET BREAKPOINT HERE
  while( 1 )
  {
   P1OUT ^= 0x01;
  }
}

void run( unsigned int rand )
{
  int i;
  for( i = 0; i < 16; i++ )
  {
    if( currentRun == 26 )
    {
      failLongTest = 1;
    }
    if( lastBit == 0x0f )
    {
      lastBit = rand & 0x01;
    }
    else if ( lastBit == (rand & 0x01) )
    {
      currentRun++;
    }
    else if ( lastBit != (rand & 0x01) )
    {
      if( lastBit )
      {
        if( currentRun >=6 )
        {
          runLengthsOne[5]++;
        }
        else
        {
          runLengthsOne[currentRun-1]++;
        }
      }
      else
      {
        if( currentRun >=6 )
        {
          runLengthsZero[5]++;
        }
        else
        {
          runLengthsZero[currentRun-1]++;
        }
      }
      currentRun = 1;
    } // if bit change
    lastBit = (rand & 0x01);
    rand = rand >> 1;
  } // for
}

float computePoker( void )
{
  float returnValue = 0;
  int i;
  for( i = 0; i < 16; i++ )
  {
    returnValue += pokerBuckets[i]*pokerBuckets[i];
  }
  returnValue = returnValue * 0.0032;
  returnValue -= 5000.00;
  return returnValue;
}

void poker( unsigned int rand )
{
  int topTop = (rand & 0xF000)>>12;
  int topBot = (rand & 0x0F00)>>8;
  int botTop = (rand & 0x00F0)>>4;
  int botBot = (rand & 0x000F);
  pokerBuckets[topTop]++;
  pokerBuckets[topBot]++;
  pokerBuckets[botTop]++;
  pokerBuckets[botBot]++;
}

void monobit( unsigned int rand )
{
  int i;
  for( i = 16; i != 0; i-- )
  {
    if( rand & 0x01 )
    {
      numberOfOnes++;
    }
    else
    {
      numberOfZeros++;
    }
    rand = rand>>1;
  } // for all bits
}

