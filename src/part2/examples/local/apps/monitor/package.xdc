/*!
 *  ======== monitor ========
 *  Wireless temperature sensor application
 *
 *  This package builds two executables:
 *  @p(dlist)
 *      - `mod_AP`
 *          a wireless "access point" that accumulates sensor data from
 *          "end devices" and displays the data to via a UART.
 *      - `mod_ED`
 *          an "end device" that monitors tempature and battery strength
 *          and wirelessly transmits this to the "access point".
 *  @p
 */
package local.apps.monitor
{
}
