/*
 *  ======== getLibs ========
 */
function getLibs(prog)
{
    var Settings = xdc.module("local.rf.Settings");

    var lib = "lib/ed";
    if (Settings.type == Settings.ACCESS_POINT) {
        lib = "lib/ap";
    }

    return (lib + ".a" + prog.build.target.suffix);
}
