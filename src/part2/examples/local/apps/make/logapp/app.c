#include <xdc/runtime/Log.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Diags.h>

#include <local/apps/hello_2/Msgs.h>

/*
 *  ======== main ========
 */
Int main(Int argc, String argv[])
{
    Log_write0(Msgs_HELLO);

    Log_print0(Diags_USER1, "hello world, again\n");

    System_exit(0);
    return (0);
}
