/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
requires txn.catalog.c430;
requires xdc.platform [1,0,1];

/*!
 *  ======== txn.platforms.ez430_rf2500 ========
 *  Platform package for the eZ430-RF2500 platform.
 *
 *  This package implements the interfaces (xdc.platform.IPlatform)
 *  necessary to build and run executables on the eZ430-RF2500 platform.
 */
package txn.platforms.ez430_rf2500 [1,0,0,1] {
    module Platform;
}
