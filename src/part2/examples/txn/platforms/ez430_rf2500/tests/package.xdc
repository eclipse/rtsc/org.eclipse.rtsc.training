/* --COPYRIGHT--,EPL
 *  Copyright (c) 2008 Texas Instruments and others.
 *  All rights reserved. This program and the accompanying materials
 *  are made available under the terms of the Eclipse Public License v1.0
 *  which accompanies this distribution, and is available at
 *  http://www.eclipse.org/legal/epl-v10.html
 * 
 *  Contributors:
 *      Texas Instruments - initial implementation
 * 
 * --/COPYRIGHT--*/
requires txn.platforms.ez430_rf2500;

/*!
 *  ======== txn.platforms.ez430_rf2500.tests ========
 *  Test package for the ez430_rf2500 platform.
 */
package txn.platforms.ez430_rf2500.tests [1,0,0,0] {
}
