/*!
 *  ======== txn.catalog.c430 ========
 *  Package of devices for the MSP430 family processors.
 *
 *  Each module in this package implements the xdc.platform.ICpuDataSheet
 *  interface. This interface is used by platforms (modules that implement
 *  xdc.platform.IPlatform) to obtain the memory map supported by each CPU.
 */
package txn.catalog.c430 [1,0,0,0] {
    module MSP430;
    module MSP430F223x, MSP430F225x, MSP430F227x;
    interface IMSP430x22xx;
}
