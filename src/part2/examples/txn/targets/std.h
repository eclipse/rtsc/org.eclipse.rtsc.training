/*
 *  ======== txn/targets/std.h ========
 *  Standard types for supported TI compilers
 */

#ifndef txn_targets_STD_
#define txn_targets_STD_

/*  Define target-specific "portable" macros
 *
 *  The build command-line defines xdc_target_name__ to be the value
 *  of the target's name config parameter.  We use this to include the
 *  target-specific definitions for the required target-independent
 *  xdc_target* macros.
 *
 *  xdc__local_include is a macro that expands into a #include of 
 *  "<name>.h", where <name> is the argument to the macro. This macro
 *  is defined in xdc/std.h.
 */
#ifdef xdc_target_name__
#include xdc__local_include(xdc_target_name__)
#endif

/* "inherit" (i.e., include) all ti.targets standard types */
#include <ti/targets/std.h>

#endif /* txn_targets_STD_ */
