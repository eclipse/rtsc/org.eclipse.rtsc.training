/*
 *  ======== ITarget.asmName ========
 *  Given a C symbol name return its assembly language name
 */
function asmName(cname)
{
    return (cname); /* TI MSP430 code gen does not mangle C symbols */
}
