requires ti.targets;

/*!
 *  ======== txn.targets ========
 *  MSP320 targets based on the TI codegen tools.
 *
 *  This package illustrates how one can create custom targets *without*
 *  making any changes to the xdc tool set.  This means that the developer is
 *  never dependent on "external" developers for updates to the xdc tools in
 *  order to build libraries with respect to special targets.
 *
 *  This package contains the following modules:
 *
 *      MSP430    this module represents a little endian Arm9 ti_arm9_abi
 *                target.  Because this module inherits from the
 *                ti.targets.ITarget interface, there is no need to implement
 *                any of the xdc.bld.ITarget functions (the implementations
 *                in ti.targets.ITarget are sufficient).
 *
 *  This package contains the following interfaces:
 *
 *      IMSP430 - this interface is used by the modules described above.
 *                Its sole purpose is to encapsulate commonality between
 *                multiple targets defined in this package.
 */
package txn.targets [1, 0, 0]
{
    module MSP430;
    interface IMSP430;
}
