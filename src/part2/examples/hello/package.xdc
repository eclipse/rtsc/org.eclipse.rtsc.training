/*!
 *  ======== hello ========
 *  System_printf based "hello world"
 *
 *  This package simply builds a `{@link xdc.runtime.System#printf System_printf}` 
 *  based "hello world" executable.
 */
package hello
{
}
