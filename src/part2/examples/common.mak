#
#  This makefile is only required for:
#      local/apps/make/* - the makefile-based examples need the location
#                          of the compiler and the XDCtools
#      ../makefile	 - top-level makefile that builds all tutorial
#			   examples
#

#
# Modify the following definitions as appropriate:
#     CGTOOLS - must point to a 3.1 version (or later) of TI's MSP430 compiler
#     XDCROOT - must point to a 3.15 version (or later) of XDCtools
#
CGTOOLS = "C:/Program Files/Texas Instruments/CC Essentials v3.1/tools/compiler/MSP430"
XDCROOT = $(HOME)/xdctools_3_15_00_33

#
# If necessary modify HOSTOS to be
#	o Windows for Windows hosts,
#	o output of uname for unix hosts
#
HOSTOS := $(shell uname)
ifeq (,$(HOSTOS))
    HOSTOS := Windows
    $(info setting HOSTOS = $(HOSTOS))
endif
HOSTOS := $(patsubst Windows%,Windows,$(HOSTOS))

#
#  Check XDCROOT definition
#
ifeq (,$(wildcard $(XDCROOT)/packages))
$(error XDCROOT (= '$(XDCROOT)') does not point to a valid installation of XDCtools)
endif
