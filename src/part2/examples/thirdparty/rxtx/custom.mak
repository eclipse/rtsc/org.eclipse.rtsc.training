# Get RXTX zip file name
DOWNLOADS = $(PKGROOT)/../../../imports/downloads
RXTX_ZIP := $(firstword $(wildcard rxtx*.zip $(DOWNLOADS)/rxtx*.zip))
RXTX     := $(patsubst %.zip,%,$(notdir $(RXTX_ZIP)))

ifeq (,$(RXTX))
$(error you must download a copy of the RXTX binary distribution to $(CURDIR))
endif

RXTX_DLL = $(RXTX)/Windows/i368-mingw32/rxtxSerial.dll
RXTX_JAR = $(RXTX)/RXTXcomm.jar

all: .install

.install:$(RXTX_DLL) $(RXTX_JAR)
	@$(MSG) "installing $(RXTX) into $(XDCROOT)/jre ..."
	$(RM) $(XDCROOT)/jre/bin/$(RXTX_DLL)
	$(CP) $(RXTX_DLL) $(XDCROOT)/jre/bin
	$(RM) $(XDCROOT)/jre/lib/ext/$(RXTX_JAR)
	$(CP) $(RXTX_JAR) $(XDCROOT)/jre/lib/ext
	$(RM) $@; $(TOUCH) $@

$(RXTX_JAR): $(RXTX_ZIP)
	@$(MSG) "extracting $@ from $< ..."
	$(UNZIP) -q -o $< $@
	$(TOUCH) $@

$(RXTX_DLL): $(RXTX_ZIP)
	@$(MSG) "extracting $@ from $< ..."
	$(UNZIP) -q -o $< $@
	$(TOUCH) $@

clean::
	$(RMDIR) $(RXTX)
	# remove non-EPL IP _only_ from the current package
	$(RM)    $(RXTX).zip
