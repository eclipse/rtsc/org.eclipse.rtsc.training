/*!
 *  ======== thirdparty.rxtx ========
 *  Install LGPL v 2.1 licenced RXTX support into XDCtools JRE
 *
 *  This package exists to install Windows Java COMM port support into
 *  the JRE supplied with XDCtools.  SUN's JRE does not support Windows
 *  COM ports but the RXTX project (http://www.rxtx.org/) provides the
 *  necessary support.
 *
 *  To add Windows COM port support simply copy the binary download from
 *  the RXTX project (e.g., rxtx-2.1-7-bins-r2.zip) into this package's
 *  base directory and rebuild.
 *  @p(code)
 *      cp rxtx-2.1-7-bins-r2.zip .../src/thirdparty/rxtx
 *      xdc -P .../src/thirdparty/rxtx
 *  @p
 */
package thirdparty.rxtx
{
}
