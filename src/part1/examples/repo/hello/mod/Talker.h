/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

/*
 * ======== GENERATED SECTIONS ========
 *     
 *     PROLOGUE
 *     INCLUDES
 *     
 *     INTERNAL DEFINITIONS
 *     MODULE-WIDE CONFIGS
 *     FUNCTION DECLARATIONS
 *     SYSTEM FUNCTIONS
 *     C++ CLIENT WRAPPER [experimental]
 *     
 *     EPILOGUE
 *     STATE STRUCTURES
 *     PREFIX ALIASES
 *     C++ SUPPLIER WRAPPER [experimental]
 */


/*
 * ======== PROLOGUE ========
 */

#ifndef hello_mod_Talker__include
#define hello_mod_Talker__include

#ifndef __nested__
#define __nested__
#define hello_mod_Talker__top__
#endif

#ifdef __cplusplus
#define __extern extern "C"
#else
#define __extern extern
#endif

#define hello_mod_Talker___VERS 150


/*
 * ======== INCLUDES ========
 */

#include <xdc/std.h>

#include <xdc/runtime/xdc.h>
#include <xdc/runtime/Types.h>
#include <hello/mod/package/package.defs.h>

#include <xdc/runtime/IModule.h>


/*
 * ======== AUXILIARY DEFINITIONS ========
 */


/*
 * ======== INTERNAL DEFINITIONS ========
 */


/*
 * ======== MODULE-WIDE CONFIGS ========
 */

/* Module__diagsEnabled */
typedef xdc_Bits32 CT__hello_mod_Talker_Module__diagsEnabled;
__extern __FAR__ const CT__hello_mod_Talker_Module__diagsEnabled hello_mod_Talker_Module__diagsEnabled__C;

/* Module__diagsIncluded */
typedef xdc_Bits32 CT__hello_mod_Talker_Module__diagsIncluded;
__extern __FAR__ const CT__hello_mod_Talker_Module__diagsIncluded hello_mod_Talker_Module__diagsIncluded__C;

/* Module__diagsMask */
typedef xdc_Bits16* CT__hello_mod_Talker_Module__diagsMask;
__extern __FAR__ const CT__hello_mod_Talker_Module__diagsMask hello_mod_Talker_Module__diagsMask__C;

/* Module__gateObj */
typedef xdc_Ptr CT__hello_mod_Talker_Module__gateObj;
__extern __FAR__ const CT__hello_mod_Talker_Module__gateObj hello_mod_Talker_Module__gateObj__C;

/* Module__gatePrms */
typedef xdc_Ptr CT__hello_mod_Talker_Module__gatePrms;
__extern __FAR__ const CT__hello_mod_Talker_Module__gatePrms hello_mod_Talker_Module__gatePrms__C;

/* Module__id */
typedef xdc_runtime_Types_ModuleId CT__hello_mod_Talker_Module__id;
__extern __FAR__ const CT__hello_mod_Talker_Module__id hello_mod_Talker_Module__id__C;

/* Module__loggerDefined */
typedef xdc_Bool CT__hello_mod_Talker_Module__loggerDefined;
__extern __FAR__ const CT__hello_mod_Talker_Module__loggerDefined hello_mod_Talker_Module__loggerDefined__C;

/* Module__loggerObj */
typedef xdc_Ptr CT__hello_mod_Talker_Module__loggerObj;
__extern __FAR__ const CT__hello_mod_Talker_Module__loggerObj hello_mod_Talker_Module__loggerObj__C;

/* Module__loggerFxn4 */
typedef xdc_runtime_Types_LoggerFxn4 CT__hello_mod_Talker_Module__loggerFxn4;
__extern __FAR__ const CT__hello_mod_Talker_Module__loggerFxn4 hello_mod_Talker_Module__loggerFxn4__C;

/* Module__loggerFxn8 */
typedef xdc_runtime_Types_LoggerFxn8 CT__hello_mod_Talker_Module__loggerFxn8;
__extern __FAR__ const CT__hello_mod_Talker_Module__loggerFxn8 hello_mod_Talker_Module__loggerFxn8__C;

/* Module__startupDoneFxn */
typedef xdc_Bool (*CT__hello_mod_Talker_Module__startupDoneFxn)(void);
__extern __FAR__ const CT__hello_mod_Talker_Module__startupDoneFxn hello_mod_Talker_Module__startupDoneFxn__C;

/* Object__count */
typedef xdc_Int CT__hello_mod_Talker_Object__count;
__extern __FAR__ const CT__hello_mod_Talker_Object__count hello_mod_Talker_Object__count__C;

/* Object__heap */
typedef xdc_runtime_IHeap_Handle CT__hello_mod_Talker_Object__heap;
__extern __FAR__ const CT__hello_mod_Talker_Object__heap hello_mod_Talker_Object__heap__C;

/* Object__sizeof */
typedef xdc_SizeT CT__hello_mod_Talker_Object__sizeof;
__extern __FAR__ const CT__hello_mod_Talker_Object__sizeof hello_mod_Talker_Object__sizeof__C;

/* Object__table */
typedef xdc_Ptr CT__hello_mod_Talker_Object__table;
__extern __FAR__ const CT__hello_mod_Talker_Object__table hello_mod_Talker_Object__table__C;

/* text */
#define hello_mod_Talker_text (hello_mod_Talker_text__C)
typedef xdc_String CT__hello_mod_Talker_text;
__extern __FAR__ const CT__hello_mod_Talker_text hello_mod_Talker_text__C;

/* count */
#define hello_mod_Talker_count (hello_mod_Talker_count__C)
typedef xdc_Int CT__hello_mod_Talker_count;
__extern __FAR__ const CT__hello_mod_Talker_count hello_mod_Talker_count__C;


/*
 * ======== FUNCTION DECLARATIONS ========
 */

/* Module_startup */
#define hello_mod_Talker_Module_startup( state ) -1

/* Handle__label__S */
xdc__CODESECT(hello_mod_Talker_Handle__label__S, "hello_mod_Talker_Handle__label")
__extern xdc_runtime_Types_Label* hello_mod_Talker_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab );

/* Module__startupDone__S */
xdc__CODESECT(hello_mod_Talker_Module__startupDone__S, "hello_mod_Talker_Module__startupDone")
__extern xdc_Bool hello_mod_Talker_Module__startupDone__S( void );

/* Object__create__S */
xdc__CODESECT(hello_mod_Talker_Object__create__S, "hello_mod_Talker_Object__create")
__extern xdc_Ptr hello_mod_Talker_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb );

/* Object__delete__S */
xdc__CODESECT(hello_mod_Talker_Object__delete__S, "hello_mod_Talker_Object__delete")
__extern xdc_Void hello_mod_Talker_Object__delete__S( xdc_Ptr instp );

/* Object__destruct__S */
xdc__CODESECT(hello_mod_Talker_Object__destruct__S, "hello_mod_Talker_Object__destruct")
__extern xdc_Void hello_mod_Talker_Object__destruct__S( xdc_Ptr objp );

/* Object__get__S */
xdc__CODESECT(hello_mod_Talker_Object__get__S, "hello_mod_Talker_Object__get")
__extern xdc_Ptr hello_mod_Talker_Object__get__S( xdc_Ptr oarr, xdc_Int i );

/* Object__first__S */
xdc__CODESECT(hello_mod_Talker_Object__first__S, "hello_mod_Talker_Object__first")
__extern xdc_Ptr hello_mod_Talker_Object__first__S( void );

/* Object__next__S */
xdc__CODESECT(hello_mod_Talker_Object__next__S, "hello_mod_Talker_Object__next")
__extern xdc_Ptr hello_mod_Talker_Object__next__S( xdc_Ptr obj );

/* Params__init__S */
xdc__CODESECT(hello_mod_Talker_Params__init__S, "hello_mod_Talker_Params__init")
__extern xdc_Void hello_mod_Talker_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz );

/* Proxy__abstract__S */
xdc__CODESECT(hello_mod_Talker_Proxy__abstract__S, "hello_mod_Talker_Proxy__abstract")
__extern xdc_Bool hello_mod_Talker_Proxy__abstract__S( void );

/* Proxy__delegate__S */
xdc__CODESECT(hello_mod_Talker_Proxy__delegate__S, "hello_mod_Talker_Proxy__delegate")
__extern xdc_Ptr hello_mod_Talker_Proxy__delegate__S( void );

/* print__E */
#define hello_mod_Talker_print hello_mod_Talker_print__E
xdc__CODESECT(hello_mod_Talker_print__E, "hello_mod_Talker_print")
__extern xdc_Void hello_mod_Talker_print__E( void );
xdc__CODESECT(hello_mod_Talker_print__F, "hello_mod_Talker_print")
__extern xdc_Void hello_mod_Talker_print__F( void );
__extern xdc_Void hello_mod_Talker_print__R( void );


/*
 * ======== SYSTEM FUNCTIONS ========
 */

/* Module_startupDone */
#define hello_mod_Talker_Module_startupDone() hello_mod_Talker_Module__startupDone__S()

/* Object_heap */
#define hello_mod_Talker_Object_heap() hello_mod_Talker_Object__heap__C

/* Module_heap */
#define hello_mod_Talker_Module_heap() hello_mod_Talker_Object__heap__C

/* Module_id */
static inline CT__hello_mod_Talker_Module__id hello_mod_Talker_Module_id( void ) 
{
    return hello_mod_Talker_Module__id__C;
}

/* Module_hasMask */
static inline xdc_Bool hello_mod_Talker_Module_hasMask( void ) 
{
    return hello_mod_Talker_Module__diagsMask__C != NULL;
}

/* Module_getMask */
static inline xdc_Bits16 hello_mod_Talker_Module_getMask( void ) 
{
    return hello_mod_Talker_Module__diagsMask__C != NULL ? *hello_mod_Talker_Module__diagsMask__C : 0;
}

/* Module_setMask */
static inline xdc_Void hello_mod_Talker_Module_setMask( xdc_Bits16 mask ) 
{
    if (hello_mod_Talker_Module__diagsMask__C != NULL) *hello_mod_Talker_Module__diagsMask__C = mask;
}


/*
 * ======== C++ CLIENT WRAPPER [experimental] ========
 */

#if defined(__cplusplus)
namespace hello_mod { namespace Talker {

/* Talker::text */
static const CT__hello_mod_Talker_text& text = hello_mod_Talker_text__C;

/* Talker::count */
static const CT__hello_mod_Talker_count& count = hello_mod_Talker_count__C;

/* Talker::print */
static inline xdc_Void print(  )
{
    hello_mod_Talker_print();
}

}}
#endif /* __cplusplus */


/*
 * ======== EPILOGUE ========
 */

#ifdef hello_mod_Talker__top__
#undef __nested__
#endif

#endif /* hello_mod_Talker__include */


/*
 * ======== STATE STRUCTURES ========
 */

#if defined(__config__) || (!defined(__nested__) && defined(hello_mod_Talker__internalaccess))

#ifndef hello_mod_Talker__include_state
#define hello_mod_Talker__include_state

/* C++ wrapper */
#if defined(__cplusplus)
namespace hello_mod { namespace Talker {


}}
#endif /* __cplusplus */

#endif /* hello_mod_Talker__include_state */

#endif


/*
 * ======== PREFIX ALIASES ========
 */

#if !defined(__nested__) && !defined(hello_mod_Talker__nolocalnames)

/* module prefix */
#define Talker_text hello_mod_Talker_text
#define Talker_count hello_mod_Talker_count
#define Talker_print hello_mod_Talker_print
#define Talker_Module_name hello_mod_Talker_Module_name
#define Talker_Module_id hello_mod_Talker_Module_id
#define Talker_Module_startup hello_mod_Talker_Module_startup
#define Talker_Module_startupDone hello_mod_Talker_Module_startupDone
#define Talker_Module_hasMask hello_mod_Talker_Module_hasMask
#define Talker_Module_getMask hello_mod_Talker_Module_getMask
#define Talker_Module_setMask hello_mod_Talker_Module_setMask
#define Talker_Object_heap hello_mod_Talker_Object_heap
#define Talker_Module_heap hello_mod_Talker_Module_heap

/* C++ wrapper */
#if defined(__cplusplus) && !defined(hello_mod_Talker__INTERNAL__)
namespace Talker = hello_mod::Talker;
#endif

#endif


/*
 * ======== C++ SUPPLIER WRAPPER [experimental] ========
 */

#if defined(__cplusplus) && defined(hello_mod_Talker__INTERNAL__)
namespace Talker {
    using hello_mod::Talker::text;
    using hello_mod::Talker::count;

/* Talker::print */
inline xdc_Void print(  );
#define DEFINE__print\
    xdc_Void hello_mod_Talker_print( void )\
    {\
        Talker::print();\
    }\

}
#endif /* __cplusplus */
