/*
 *  ======== hello/mod/Talker.c ========
 */

#include <xdc/runtime/System.h>
#include "package/internal/Talker.xdc.h"
 
Void Talker_print()
{
    Int i;
    for (i = 0; i < Talker_count; i++) {
        System_printf("%s\n", Talker_text);
    }
}
