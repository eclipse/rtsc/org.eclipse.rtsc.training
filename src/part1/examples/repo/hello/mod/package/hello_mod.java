/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */
import java.util.*;
import org.mozilla.javascript.*;
import xdc.services.intern.xsr.*;
import xdc.services.spec.*;

public class hello_mod
{
    static final String VERS = "@(#) xdc-t32\n";

    static final Proto.Elm $$T_Bool = Proto.Elm.newBool();
    static final Proto.Elm $$T_Num = Proto.Elm.newNum();
    static final Proto.Elm $$T_Str = Proto.Elm.newStr();
    static final Proto.Elm $$T_Obj = Proto.Elm.newObj();

    static final Proto.Fxn $$T_Met = new Proto.Fxn(null, null, 0, -1, false);
    static final Proto.Map $$T_Map = new Proto.Map($$T_Obj);
    static final Proto.Arr $$T_Vec = new Proto.Arr($$T_Obj);

    static final XScriptO $$DEFAULT = Value.DEFAULT;
    static final Object $$UNDEF = Undefined.instance;

    static final Proto.Obj $$Package = (Proto.Obj)Global.get("$$Package");
    static final Proto.Obj $$Module = (Proto.Obj)Global.get("$$Module");
    static final Proto.Obj $$Instance = (Proto.Obj)Global.get("$$Instance");
    static final Proto.Obj $$Params = (Proto.Obj)Global.get("$$Params");

    static final Object $$objFldGet = Global.get("$$objFldGet");
    static final Object $$objFldSet = Global.get("$$objFldSet");
    static final Object $$proxyGet = Global.get("$$proxyGet");
    static final Object $$proxySet = Global.get("$$proxySet");
    static final Object $$delegGet = Global.get("$$delegGet");
    static final Object $$delegSet = Global.get("$$delegSet");

    Scriptable xdcO;
    Session ses;
    Value.Obj om;

    boolean isROV;
    boolean isCFG;

    Proto.Obj pkgP;
    Value.Obj pkgV;

    ArrayList<Object> imports = new ArrayList<Object>();
    ArrayList<Object> loggables = new ArrayList<Object>();
    ArrayList<Object> mcfgs = new ArrayList<Object>();
    ArrayList<Object> proxies = new ArrayList<Object>();
    ArrayList<Object> sizes = new ArrayList<Object>();
    ArrayList<Object> tdefs = new ArrayList<Object>();

    void $$IMPORTS()
    {
        Global.callFxn("loadPackage", xdcO, "xdc");
        Global.callFxn("loadPackage", xdcO, "xdc.corevers");
        Global.callFxn("loadPackage", xdcO, "xdc.runtime");
    }

    void $$OBJECTS()
    {
        pkgP = (Proto.Obj)om.bind("hello.mod.Package", new Proto.Obj());
        pkgV = (Value.Obj)om.bind("hello.mod", new Value.Obj("hello.mod", pkgP));
    }

    void Talker$$OBJECTS()
    {
        Proto.Obj po, spo;
        Value.Obj vo;

        po = (Proto.Obj)om.bind("hello.mod.Talker.Module", new Proto.Obj());
        vo = (Value.Obj)om.bind("hello.mod.Talker", new Value.Obj("hello.mod.Talker", po));
        pkgV.bind("Talker", vo);
        // decls 
    }

    void Talker$$CONSTS()
    {
        // module Talker
        om.bind("hello.mod.Talker.print", new Extern("hello_mod_Talker_print__E", "xdc_Void(*)(xdc_Void)", true, false, "hello.mod.Talker.print"));
    }

    void Talker$$CREATES()
    {
        Proto.Fxn fxn;
        StringBuilder sb;

    }

    void Talker$$FUNCTIONS()
    {
        Proto.Fxn fxn;

    }

    void Talker$$SIZES()
    {
        Proto.Str so;
        Object fxn;

    }

    void Talker$$TYPES()
    {
        Scriptable cap;
        Proto.Obj po;
        Proto.Str ps;
        Proto.Typedef pt;
        Object fxn;

        cap = (Scriptable)Global.callFxn("loadCapsule", xdcO, "hello/mod/Talker.xs");
        om.bind("hello.mod.Talker$$capsule", cap);
        po = (Proto.Obj)om.find("hello.mod.Talker.Module");
        po.init("hello.mod.Talker.Module", om.find("xdc.runtime.IModule.Module"));
                po.addFld("$hostonly", $$T_Num, 0, "r");
        if (isCFG) {
                        po.addFld("text", $$T_Str, "Hello World", "w");
                        po.addFld("count", Proto.Elm.newCNum("(xdc_Int)"), 1L, "w");
        }//isCFG
                fxn = Global.get(cap, "module$use");
                if (fxn != null) om.bind("hello.mod.Talker$$module$use", true);
                if (fxn != null) po.addFxn("module$use", $$T_Met, fxn);
                fxn = Global.get(cap, "module$meta$init");
                if (fxn != null) om.bind("hello.mod.Talker$$module$meta$init", true);
                if (fxn != null) po.addFxn("module$meta$init", $$T_Met, fxn);
                fxn = Global.get(cap, "module$static$init");
                if (fxn != null) om.bind("hello.mod.Talker$$module$static$init", true);
                if (fxn != null) po.addFxn("module$static$init", $$T_Met, fxn);
                fxn = Global.get(cap, "module$validate");
                if (fxn != null) om.bind("hello.mod.Talker$$module$validate", true);
                if (fxn != null) po.addFxn("module$validate", $$T_Met, fxn);
    }

    void Talker$$ROV()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("hello.mod.Talker");
    }

    void $$SINGLETONS()
    {
        pkgP.init("hello.mod.Package", (Proto.Obj)om.find("xdc.IPackage.Module"));
        pkgP.bind("$capsule", $$UNDEF);
        pkgV.init2(pkgP, "hello.mod", Value.DEFAULT, false);
        pkgV.bind("$name", "hello.mod");
        pkgV.bind("$category", "Package");
        pkgV.bind("$$qn", "hello.mod.");
        pkgV.bind("$vers", Global.newArray());
        Value.Map atmap = (Value.Map)pkgV.getv("$attr");
        atmap.seal("length");
        imports.clear();
        pkgV.bind("$imports", imports);
        StringBuilder sb = new StringBuilder();
        sb.append("var pkg = xdc.om['hello.mod'];\n");
        sb.append("if (pkg.$vers.length >= 3) {\n");
            sb.append("pkg.$vers.push(Packages.xdc.services.global.Vers.getDate(xdc.csd() + '/..'));\n");
        sb.append("}\n");
        sb.append("pkg.build.libraries = [\n");
            sb.append("'lib/hello.mod.a86GW',\n");
            sb.append("'lib/hello.mod.a64P',\n");
        sb.append("];\n");
        sb.append("pkg.build.libDesc = [\n");
            sb.append("['lib/hello.mod.a86GW', {target: 'gnu.targets.Mingw'}],\n");
            sb.append("['lib/hello.mod.a64P', {target: 'ti.targets.C64P'}],\n");
        sb.append("];\n");
        sb.append("if('suffix' in xdc.om['xdc.IPackage$$LibDesc']) {\n");
            sb.append("pkg.build.libDesc['lib/hello.mod.a86GW'].suffix = '86GW';\n");
            sb.append("pkg.build.libDesc['lib/hello.mod.a64P'].suffix = '64P';\n");
        sb.append("}\n");
        Global.eval(sb.toString());
    }

    void Talker$$SINGLETONS()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("hello.mod.Talker");
        po = (Proto.Obj)om.find("hello.mod.Talker.Module");
        vo.init2(po, "hello.mod.Talker", $$DEFAULT, false);
        vo.bind("Module", po);
        vo.bind("$category", "Module");
        vo.bind("$capsule", om.find("hello.mod.Talker$$capsule"));
        vo.bind("$package", om.find("hello.mod"));
        tdefs.clear();
        proxies.clear();
        mcfgs.clear();
        mcfgs.add("Module__diagsEnabled");
        mcfgs.add("Module__diagsIncluded");
        mcfgs.add("Module__diagsMask");
        mcfgs.add("Module__gateObj");
        mcfgs.add("Module__gatePrms");
        mcfgs.add("Module__id");
        mcfgs.add("Module__loggerDefined");
        mcfgs.add("Module__loggerObj");
        mcfgs.add("Module__loggerFxn4");
        mcfgs.add("Module__loggerFxn8");
        mcfgs.add("Module__startupDoneFxn");
        mcfgs.add("Object__count");
        mcfgs.add("Object__heap");
        mcfgs.add("Object__sizeof");
        mcfgs.add("Object__table");
        mcfgs.add("text");
        mcfgs.add("count");
        vo.bind("$$tdefs", Global.newArray(tdefs.toArray()));
        vo.bind("$$proxies", Global.newArray(proxies.toArray()));
        vo.bind("$$mcfgs", Global.newArray(mcfgs.toArray()));
        ((Value.Arr)pkgV.getv("$modules")).add(vo);
        ((Value.Arr)om.find("$modules")).add(vo);
        vo.bind("$$instflag", 0);
        vo.bind("$$iobjflag", 0);
        vo.bind("$$sizeflag", 1);
        vo.bind("$$dlgflag", 0);
        vo.bind("$$iflag", 0);
        vo.bind("$$romcfgs", "|");
        if (isCFG) {
            Proto.Str ps = (Proto.Str)vo.find("Module_State");
            if (ps != null) vo.bind("$object", ps.newInstance());
            vo.bind("$$meta_iobj", 1);
        }//isCFG
        vo.bind("print", om.find("hello.mod.Talker.print"));
        vo.bind("$$fxntab", Global.newArray("hello_mod_Talker_Handle__label__E", "hello_mod_Talker_Module__startupDone__E", "hello_mod_Talker_Object__create__E", "hello_mod_Talker_Object__delete__E", "hello_mod_Talker_Object__destruct__E", "hello_mod_Talker_Object__get__E", "hello_mod_Talker_Object__first__E", "hello_mod_Talker_Object__next__E", "hello_mod_Talker_Params__init__E", "hello_mod_Talker_Proxy__abstract__E", "hello_mod_Talker_Proxy__delegate__E", "hello_mod_Talker_print__E"));
        vo.bind("$$logEvtCfgs", Global.newArray());
        vo.bind("$$errorDescCfgs", Global.newArray());
        vo.bind("$$assertDescCfgs", Global.newArray());
        Value.Map atmap = (Value.Map)vo.getv("$attr");
        atmap.seal("length");
        vo.bind("MODULE_STARTUP$", 0);
        vo.bind("PROXY$", 0);
        loggables.clear();
        loggables.add(Global.newObject("name", "print", "entry", "", "exit", ""));
        vo.bind("$$loggables", loggables.toArray());
        pkgV.bind("Talker", vo);
        ((Value.Arr)pkgV.getv("$unitNames")).add("Talker");
    }

    void $$INITIALIZATION()
    {
        Value.Obj vo;

        if (isCFG) {
        }//isCFG
        Global.callFxn("module$meta$init", (Scriptable)om.find("hello.mod.Talker"));
        Global.callFxn("init", pkgV);
        ((Value.Obj)om.getv("hello.mod.Talker")).bless();
        ((Value.Arr)om.find("$packages")).add(pkgV);
    }

    public void exec( Scriptable xdcO, Session ses )
    {
        this.xdcO = xdcO;
        this.ses = ses;
        om = (Value.Obj)xdcO.get("om", null);

        Object o = om.geto("$name");
        String s = o instanceof String ? (String)o : null;
        isCFG = s != null && s.equals("cfg");
        isROV = s != null && s.equals("rov");

        $$IMPORTS();
        $$OBJECTS();
        Talker$$OBJECTS();
        Talker$$CONSTS();
        Talker$$CREATES();
        Talker$$FUNCTIONS();
        Talker$$SIZES();
        Talker$$TYPES();
        if (isROV) {
            Talker$$ROV();
        }//isROV
        $$SINGLETONS();
        Talker$$SINGLETONS();
        $$INITIALIZATION();
    }
}
