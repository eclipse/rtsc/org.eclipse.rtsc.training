/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

#define __nested__
#define __config__

#include <xdc/std.h>

/*
 * ======== GENERATED SECTIONS ========
 *     
 *     MODULE INCLUDES
 *     
 *     <module-name> INTERNALS
 *     <module-name> INHERITS
 *     <module-name> VTABLE
 *     <module-name> PATCH TABLE
 *     <module-name> DECLARATIONS
 *     <module-name> OBJECT OFFSETS
 *     <module-name> TEMPLATES
 *     <module-name> INITIALIZERS
 *     <module-name> FUNCTION STUBS
 *     <module-name> PROXY BODY
 *     <module-name> OBJECT DESCRIPTOR
 *     <module-name> SYSTEM FUNCTIONS
 *     <module-name> PRAGMAS
 *     
 *     INITIALIZATION ENTRY POINT
 *     PROGRAM GLOBALS
 *     CLINK DIRECTIVES
 */


/*
 * ======== MODULE INCLUDES ========
 */

#include <hello/mod/Talker.h>
#include <xdc/runtime/Assert.h>
#include <xdc/runtime/Core.h>
#include <xdc/runtime/Defaults.h>
#include <xdc/runtime/Diags.h>
#include <xdc/runtime/Error.h>
#include <xdc/runtime/Gate.h>
#include <xdc/runtime/GateNull.h>
#include <xdc/runtime/HeapStd.h>
#include <xdc/runtime/Log.h>
#include <xdc/runtime/Main.h>
#include <xdc/runtime/Memory.h>
#include <xdc/runtime/Startup.h>
#include <xdc/runtime/SysMin.h>
#include <xdc/runtime/System.h>
#include <xdc/runtime/Text.h>


/*
 * ======== hello.mod.Talker INTERNALS ========
 */

/* Module__ */
typedef struct hello_mod_Talker_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} hello_mod_Talker_Module__;

/* Module__root__V */
extern hello_mod_Talker_Module__ hello_mod_Talker_Module__root__V;


/*
 * ======== xdc.runtime.Assert INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Assert_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Assert_Module__;

/* Module__root__V */
extern xdc_runtime_Assert_Module__ xdc_runtime_Assert_Module__root__V;


/*
 * ======== xdc.runtime.Core INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Core_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Core_Module__;

/* Module__root__V */
extern xdc_runtime_Core_Module__ xdc_runtime_Core_Module__root__V;


/*
 * ======== xdc.runtime.Defaults INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Defaults_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Defaults_Module__;

/* Module__root__V */
extern xdc_runtime_Defaults_Module__ xdc_runtime_Defaults_Module__root__V;


/*
 * ======== xdc.runtime.Diags INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Diags_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Diags_Module__;

/* Module__root__V */
extern xdc_runtime_Diags_Module__ xdc_runtime_Diags_Module__root__V;


/*
 * ======== xdc.runtime.Error INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Error_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Error_Module__;

/* Module__root__V */
extern xdc_runtime_Error_Module__ xdc_runtime_Error_Module__root__V;


/*
 * ======== xdc.runtime.Gate INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Gate_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Gate_Module__;

/* Module__root__V */
extern xdc_runtime_Gate_Module__ xdc_runtime_Gate_Module__root__V;


/*
 * ======== xdc.runtime.GateNull INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_GateNull_Module__ {
    xdc_runtime_Types_ModHdr hdr;
} xdc_runtime_GateNull_Module__;

/* Module__root__V */
extern xdc_runtime_GateNull_Module__ xdc_runtime_GateNull_Module__root__V;

/* Object__ */
typedef struct xdc_runtime_GateNull_Object__ {
    const xdc_runtime_GateNull_Fxns__* __fxns;
} xdc_runtime_GateNull_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_GateNull_Object__ obj;
} xdc_runtime_GateNull_Object2__;

/* __ParamsPtr */
#ifdef xdc_runtime_GateNull___VERS
    #define xdc_runtime_GateNull___ParamsPtr xdc_UChar*
#else
    #define xdc_runtime_GateNull___ParamsPtr xdc_Ptr
#endif


/*
 * ======== xdc.runtime.HeapStd INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_HeapStd_Module__ {
    xdc_runtime_Types_ModHdr hdr;
} xdc_runtime_HeapStd_Module__;

/* Module__root__V */
extern xdc_runtime_HeapStd_Module__ xdc_runtime_HeapStd_Module__root__V;

/* Object__ */
typedef struct xdc_runtime_HeapStd_Object__ {
    const xdc_runtime_HeapStd_Fxns__* __fxns;
    xdc_runtime_Memory_Size remainSize;
    xdc_runtime_Memory_Size startSize;
} xdc_runtime_HeapStd_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_HeapStd_Object__ obj;
} xdc_runtime_HeapStd_Object2__;

/* __ParamsPtr */
#ifdef xdc_runtime_HeapStd___VERS
    #define xdc_runtime_HeapStd___ParamsPtr xdc_UChar*
#else
    #define xdc_runtime_HeapStd___ParamsPtr xdc_Ptr
#endif


/*
 * ======== xdc.runtime.Log INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Log_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Log_Module__;

/* Module__root__V */
extern xdc_runtime_Log_Module__ xdc_runtime_Log_Module__root__V;


/*
 * ======== xdc.runtime.Main INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Main_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Main_Module__;

/* Module__root__V */
extern xdc_runtime_Main_Module__ xdc_runtime_Main_Module__root__V;


/*
 * ======== xdc.runtime.Main_Module_GateProxy INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Main_Module_GateProxy_Module__ {
    xdc_runtime_Types_ModHdr hdr;
} xdc_runtime_Main_Module_GateProxy_Module__;

/* Module__root__V */
extern xdc_runtime_Main_Module_GateProxy_Module__ xdc_runtime_Main_Module_GateProxy_Module__root__V;

/* <-- xdc_runtime_GateNull_Object */

/* Object */
typedef xdc_runtime_GateNull_Object__ xdc_runtime_Main_Module_GateProxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_Main_Module_GateProxy_Object__ obj;
} xdc_runtime_Main_Module_GateProxy_Object2__;

/* __ParamsPtr */
#ifdef xdc_runtime_Main_Module_GateProxy___VERS
    #define xdc_runtime_Main_Module_GateProxy___ParamsPtr xdc_UChar*
#else
    #define xdc_runtime_Main_Module_GateProxy___ParamsPtr xdc_Ptr
#endif


/*
 * ======== xdc.runtime.Memory INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Memory_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Memory_Module__;

/* Module__root__V */
extern xdc_runtime_Memory_Module__ xdc_runtime_Memory_Module__root__V;


/*
 * ======== xdc.runtime.Memory_HeapProxy INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Memory_HeapProxy_Module__ {
    xdc_runtime_Types_ModHdr hdr;
} xdc_runtime_Memory_HeapProxy_Module__;

/* Module__root__V */
extern xdc_runtime_Memory_HeapProxy_Module__ xdc_runtime_Memory_HeapProxy_Module__root__V;

/* <-- xdc_runtime_HeapStd_Object */

/* Object */
typedef xdc_runtime_HeapStd_Object__ xdc_runtime_Memory_HeapProxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_Memory_HeapProxy_Object__ obj;
} xdc_runtime_Memory_HeapProxy_Object2__;

/* __ParamsPtr */
#ifdef xdc_runtime_Memory_HeapProxy___VERS
    #define xdc_runtime_Memory_HeapProxy___ParamsPtr xdc_UChar*
#else
    #define xdc_runtime_Memory_HeapProxy___ParamsPtr xdc_Ptr
#endif


/*
 * ======== xdc.runtime.Startup INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Startup_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Startup_Module__;

/* Module__root__V */
extern xdc_runtime_Startup_Module__ xdc_runtime_Startup_Module__root__V;


/*
 * ======== xdc.runtime.SysMin INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_SysMin_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_SysMin_Module__;

/* Module__root__V */
extern xdc_runtime_SysMin_Module__ xdc_runtime_SysMin_Module__root__V;


/*
 * ======== xdc.runtime.System INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_System_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_System_Module__;

/* Module__root__V */
extern xdc_runtime_System_Module__ xdc_runtime_System_Module__root__V;


/*
 * ======== xdc.runtime.System_Module_GateProxy INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_System_Module_GateProxy_Module__ {
    xdc_runtime_Types_ModHdr hdr;
} xdc_runtime_System_Module_GateProxy_Module__;

/* Module__root__V */
extern xdc_runtime_System_Module_GateProxy_Module__ xdc_runtime_System_Module_GateProxy_Module__root__V;

/* <-- xdc_runtime_GateNull_Object */

/* Object */
typedef xdc_runtime_GateNull_Object__ xdc_runtime_System_Module_GateProxy_Object__;

/* Object2__ */
typedef struct {
    xdc_runtime_Types_InstHdr hdr;
    xdc_runtime_System_Module_GateProxy_Object__ obj;
} xdc_runtime_System_Module_GateProxy_Object2__;

/* __ParamsPtr */
#ifdef xdc_runtime_System_Module_GateProxy___VERS
    #define xdc_runtime_System_Module_GateProxy___ParamsPtr xdc_UChar*
#else
    #define xdc_runtime_System_Module_GateProxy___ParamsPtr xdc_Ptr
#endif


/*
 * ======== xdc.runtime.System_SupportProxy INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_System_SupportProxy_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_System_SupportProxy_Module__;

/* Module__root__V */
extern xdc_runtime_System_SupportProxy_Module__ xdc_runtime_System_SupportProxy_Module__root__V;


/*
 * ======== xdc.runtime.Text INTERNALS ========
 */

/* Module__ */
typedef struct xdc_runtime_Text_Module__ {
    xdc_runtime_Types_ModHdrS hdr;
} xdc_runtime_Text_Module__;

/* Module__root__V */
extern xdc_runtime_Text_Module__ xdc_runtime_Text_Module__root__V;


/*
 * ======== xdc.runtime.GateNull INHERITS ========
 */

__FAR__ const xdc_runtime_Types_Base xdc_runtime_IModule_Interface__BASE__C = {0};
__FAR__ const xdc_runtime_Types_Base xdc_runtime_IGateProvider_Interface__BASE__C = {(void*)&xdc_runtime_IModule_Interface__BASE__C};


/*
 * ======== xdc.runtime.HeapStd INHERITS ========
 */

__FAR__ const xdc_runtime_Types_Base xdc_runtime_IHeap_Interface__BASE__C = {(void*)&xdc_runtime_IModule_Interface__BASE__C};


/*
 * ======== xdc.runtime.SysMin INHERITS ========
 */

__FAR__ const xdc_runtime_Types_Base xdc_runtime_ISystemSupport_Interface__BASE__C = {(void*)&xdc_runtime_IModule_Interface__BASE__C};


/*
 * ======== xdc.runtime.GateNull VTABLE ========
 */

/* Module__FXNS__C */
const xdc_runtime_GateNull_Fxns__ xdc_runtime_GateNull_Module__FXNS__C = {
    (void*)&xdc_runtime_IGateProvider_Interface__BASE__C, /* base__ */
    &xdc_runtime_GateNull_Module__FXNS__C.__sfxns, /* __sysp */
    xdc_runtime_GateNull_query__E,
    xdc_runtime_GateNull_enter__E,
    xdc_runtime_GateNull_leave__E,
    {
        xdc_runtime_GateNull_Object__create__S,
        xdc_runtime_GateNull_Object__delete__S,
        xdc_runtime_GateNull_Handle__label__S,
        0x8008, /* __mid */
    } /* __sfxns */
};


/*
 * ======== xdc.runtime.HeapStd VTABLE ========
 */

/* Module__FXNS__C */
const xdc_runtime_HeapStd_Fxns__ xdc_runtime_HeapStd_Module__FXNS__C = {
    (void*)&xdc_runtime_IHeap_Interface__BASE__C, /* base__ */
    &xdc_runtime_HeapStd_Module__FXNS__C.__sfxns, /* __sysp */
    xdc_runtime_HeapStd_alloc__E,
    xdc_runtime_HeapStd_free__E,
    xdc_runtime_HeapStd_isBlocking__E,
    xdc_runtime_HeapStd_getStats__E,
    {
        xdc_runtime_HeapStd_Object__create__S,
        xdc_runtime_HeapStd_Object__delete__S,
        xdc_runtime_HeapStd_Handle__label__S,
        0x800c, /* __mid */
    } /* __sfxns */
};


/*
 * ======== xdc.runtime.SysMin VTABLE ========
 */

/* Module__FXNS__C */
const xdc_runtime_SysMin_Fxns__ xdc_runtime_SysMin_Module__FXNS__C = {
    (void*)&xdc_runtime_ISystemSupport_Interface__BASE__C, /* base__ */
    &xdc_runtime_SysMin_Module__FXNS__C.__sfxns, /* __sysp */
    xdc_runtime_SysMin_abort__E,
    xdc_runtime_SysMin_exit__E,
    xdc_runtime_SysMin_flush__E,
    xdc_runtime_SysMin_putch__E,
    xdc_runtime_SysMin_ready__E,
    {
        NULL, /* __create */
        NULL, /* __delete */
        NULL, /* __label */
        0x800f, /* __mid */
    } /* __sfxns */
};


/*
 * ======== hello.mod.Talker DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Assert DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Core DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Defaults DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Diags DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Error DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_Error_Module_State__ {
    xdc_UInt16 count;
} xdc_runtime_Error_Module_State__;

/* Module__state__V */
xdc_runtime_Error_Module_State__ xdc_runtime_Error_Module__state__V;


/*
 * ======== xdc.runtime.Gate DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.GateNull DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.HeapStd DECLARATIONS ========
 */

/* Object__table__V */
xdc_runtime_HeapStd_Object__ xdc_runtime_HeapStd_Object__table__V[1];

/* Module_State__ */
typedef struct xdc_runtime_HeapStd_Module_State__ {
    xdc_runtime_Memory_Size remainRTSSize;
} xdc_runtime_HeapStd_Module_State__;

/* Module__state__V */
xdc_runtime_HeapStd_Module_State__ xdc_runtime_HeapStd_Module__state__V;


/*
 * ======== xdc.runtime.Log DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Main DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Main_Module_GateProxy DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Memory DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_Memory_Module_State__ {
    xdc_SizeT maxDefaultTypeAlign;
} xdc_runtime_Memory_Module_State__;

/* Module__state__V */
xdc_runtime_Memory_Module_State__ xdc_runtime_Memory_Module__state__V;


/*
 * ======== xdc.runtime.Memory_HeapProxy DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Startup DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_Startup_Module_State__ {
    xdc_Int* stateTab;
    xdc_Bool execFlag;
    xdc_Bool rtsDoneFlag;
} xdc_runtime_Startup_Module_State__;

/* Module__state__V */
xdc_runtime_Startup_Module_State__ xdc_runtime_Startup_Module__state__V;

/* --> xdc_runtime_System_Module_startup__E */
extern xdc_Int xdc_runtime_System_Module_startup__E(xdc_Int);

/* --> xdc_runtime_SysMin_Module_startup__E */
extern xdc_Int xdc_runtime_SysMin_Module_startup__E(xdc_Int);

/* --> xdc_runtime_Startup_sfxnTab__A */
const __T1_xdc_runtime_Startup_sfxnTab xdc_runtime_Startup_sfxnTab__A[2];

/* --> xdc_runtime_Startup_sfxnRts__A */
const __T1_xdc_runtime_Startup_sfxnRts xdc_runtime_Startup_sfxnRts__A[2];


/*
 * ======== xdc.runtime.SysMin DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_SysMin_Module_State__ {
    __TA_xdc_runtime_SysMin_Module_State__outbuf outbuf;
    xdc_UInt outidx;
    xdc_Bool wrapped;
} xdc_runtime_SysMin_Module_State__;

/* --> xdc_runtime_SysMin_Module_State_0_outbuf__A */
__T1_xdc_runtime_SysMin_Module_State__outbuf xdc_runtime_SysMin_Module_State_0_outbuf__A[1024];

/* Module__state__V */
xdc_runtime_SysMin_Module_State__ xdc_runtime_SysMin_Module__state__V;

/* --> xdc_runtime_SysMin_output__I */
extern xdc_Void xdc_runtime_SysMin_output__I(xdc_Char*,xdc_UInt);


/*
 * ======== xdc.runtime.System DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_System_Module_State__ {
    __TA_xdc_runtime_System_Module_State__atexitHandlers atexitHandlers;
    xdc_Int numAtexitHandlers;
    xdc_Int exitStatus;
} xdc_runtime_System_Module_State__;

/* --> xdc_runtime_System_Module_State_0_atexitHandlers__A */
__T1_xdc_runtime_System_Module_State__atexitHandlers xdc_runtime_System_Module_State_0_atexitHandlers__A[8];

/* Module__state__V */
xdc_runtime_System_Module_State__ xdc_runtime_System_Module__state__V;

/* --> xdc_runtime_System_printfExtend__I */
extern xdc_Int xdc_runtime_System_printfExtend__I(xdc_Char**,xdc_Char**,xdc_VaList,xdc_runtime_System_ParseData*);


/*
 * ======== xdc.runtime.System_Module_GateProxy DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.System_SupportProxy DECLARATIONS ========
 */


/*
 * ======== xdc.runtime.Text DECLARATIONS ========
 */

/* Module_State__ */
typedef struct xdc_runtime_Text_Module_State__ {
    xdc_Ptr charBase;
    xdc_Ptr nodeBase;
} xdc_runtime_Text_Module_State__;

/* Module__state__V */
xdc_runtime_Text_Module_State__ xdc_runtime_Text_Module__state__V;

/* --> xdc_runtime_Text_charTab__A */
const __T1_xdc_runtime_Text_charTab xdc_runtime_Text_charTab__A[680];

/* --> xdc_runtime_Text_nodeTab__A */
const __T1_xdc_runtime_Text_nodeTab xdc_runtime_Text_nodeTab__A[19];


/*
 * ======== xdc.cfg.Program TEMPLATE ========
 */

/*
 *  ======== __ASM__ ========
 *  Define this program's package-path relative assembly directory
 */
__FAR__ char *__ASM__ = "@(#)__ASM__ = D:/work/tutorial/repo/hello/mod/package/cfg/prog_x86GW";

/*
 *  ======== __ISA__ ========
 *  Define the ISA of this executable.  This symbol is used by platform
 *  specific "exec" commands that support more than one ISA; e.g., gdb
 */
__FAR__ char *__ISA__ = "@(#)__ISA__ = x86";

/*
 *  ======== __PLAT__ ========
 *  Define the name of the platform that can run this executable.  This
 *  symbol is used by platform independent "exec" commands
 */
__FAR__ char *__PLAT__ = "@(#)__PLAT__ = host.platforms.PC";

/*
 *  ======== __TARG__ ========
 *  Define the name of the target used to build this executable.
 */
__FAR__ char *__TARG__ = "@(#)__TARG__ = gnu.targets.Mingw";


/*
 * ======== xdc.runtime.Diags TEMPLATE ========
 */



/*
 * ======== xdc.runtime.Startup TEMPLATE ========
 */


xdc_Bool xdc_runtime_System_Module__startupDone__F(void) {
    return (&xdc_runtime_Startup_Module__state__V)->stateTab == 0 || (&xdc_runtime_Startup_Module__state__V)->stateTab[0] < 0;
}
xdc_Bool xdc_runtime_SysMin_Module__startupDone__F(void) {
    return (&xdc_runtime_Startup_Module__state__V)->stateTab == 0 || (&xdc_runtime_Startup_Module__state__V)->stateTab[1] < 0;
}


xdc_Void xdc_runtime_Startup_exec__I(void)
{
    xdc_Int state[3];
    xdc_runtime_Startup_startModsFxn__C(state, 2);
}

#ifdef __ti__
#pragma FUNC_EXT_CALLED(xdc_runtime_Startup_reset__I);
#endif

/*
 *  ======== xdc_runtime_Startup_reset__I ========
 *  This function is called by bootstrap initialization code as early as
 *  possible in the startup process.  This function simply calls any
 *  configured null.
 */
xdc_Void xdc_runtime_Startup_reset__I(void)
{
}

/*
 * ======== xdc.runtime.System TEMPLATE ========
 */


#include <xdc/std.h>
#include <limits.h>
#include <xdc/runtime/Types.h>
#include <xdc/runtime/Text.h>
#include <xdc/runtime/Assert.h>

/*
 *  ======== System_printfExtend__I ========
 *  This function processes optional extended formats of printf.
 */
Int xdc_runtime_System_printfExtend__I(Char **pbuf, Char **pfmt, 
    VaList va, xdc_runtime_System_ParseData *parse)
{
    Char    *fmt = *pfmt;
    Int     res;
    Char    c;
    Bool    found = FALSE;
    
    res = 0;

    c = *fmt++;
    *pfmt = *pfmt + 1;

    
    if (c == '$') {
        c = *fmt++;
        *pfmt = *pfmt + 1;
        if (c == 'L') {
            xdc_runtime_Types_Label *lab = parse->aFlag ? 
                (xdc_runtime_Types_Label *)iargToPtr(va_arg(va, IArg)) :
                (xdc_runtime_Types_Label *)va_arg(va, void *);
            res += xdc_runtime_Text_putLab(lab, pbuf, parse->precis);
            parse->len = 0;
            found = TRUE;
        }
    }


    if (found == FALSE) {
         /* other character (like %) copy to output */
         *(parse->ptr) = c;
         parse->len = 1;
    }

    return (res);
}



/*
 * ======== xdc.runtime.SysMin TEMPLATE ========
 */


#ifdef __ti__
extern int HOSTwrite(int, char *, unsigned);
#else
#include <stdio.h>
#endif

/*
 *  ======== SysMin_output__I ========
 *  HOSTWrite only writes a max of N chars at a time. The amount it writes
 *  is returned. This function loops until the entire buffer is written.
 *  Being a static function allows it to conditionally compile out.
 */
Void xdc_runtime_SysMin_output__I(Char *buf, UInt size)
{
#ifdef __ti__
    Int printCount;
    
    while (size != 0) {
        printCount = HOSTwrite(1, buf, size);
        if ((printCount <= 0) || (printCount > size)) {
            break;  /* ensure we never get stuck in an infinite loop */
        }
        size -= printCount;
        buf = buf + printCount;
    }    
#else
    fwrite(buf, 1, size, stdout);
#endif
}

/*
 * ======== xdc.runtime.Text TEMPLATE ========
 */


/*
 *  ======== xdc_runtime_Text_visitRope__I ========
 *  This function is indirectly called within Text.c through
 *  the visitRopeFxn configuration parameter of xdc.runtime.Text.
 */
Void xdc_runtime_Text_visitRope__I(xdc_runtime_Text_RopeId rope,
    xdc_Fxn visFxn, xdc_Ptr visState)
{
    xdc_String stack[3];
    xdc_runtime_Text_visitRope2__I(rope, visFxn, visState, stack);
}


/*
 * ======== hello.mod.Talker INITIALIZERS ========
 */

/* Module__root__V */
hello_mod_Talker_Module__ hello_mod_Talker_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__hello_mod_Talker_Module__diagsEnabled hello_mod_Talker_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__hello_mod_Talker_Module__diagsIncluded hello_mod_Talker_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__hello_mod_Talker_Module__diagsMask hello_mod_Talker_Module__diagsMask__C = ((CT__hello_mod_Talker_Module__diagsMask)((void*)&hello_mod_Talker_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__hello_mod_Talker_Module__gateObj hello_mod_Talker_Module__gateObj__C = ((CT__hello_mod_Talker_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__hello_mod_Talker_Module__gatePrms hello_mod_Talker_Module__gatePrms__C = ((CT__hello_mod_Talker_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__hello_mod_Talker_Module__id hello_mod_Talker_Module__id__C = (xdc_Bits16)0x8012;

/* Module__loggerDefined__C */
__FAR__ const CT__hello_mod_Talker_Module__loggerDefined hello_mod_Talker_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__hello_mod_Talker_Module__loggerObj hello_mod_Talker_Module__loggerObj__C = ((CT__hello_mod_Talker_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__hello_mod_Talker_Module__loggerFxn4 hello_mod_Talker_Module__loggerFxn4__C = ((CT__hello_mod_Talker_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__hello_mod_Talker_Module__loggerFxn8 hello_mod_Talker_Module__loggerFxn8__C = ((CT__hello_mod_Talker_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__hello_mod_Talker_Module__startupDoneFxn hello_mod_Talker_Module__startupDoneFxn__C = ((CT__hello_mod_Talker_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__hello_mod_Talker_Object__count hello_mod_Talker_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__hello_mod_Talker_Object__heap hello_mod_Talker_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__hello_mod_Talker_Object__sizeof hello_mod_Talker_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__hello_mod_Talker_Object__table hello_mod_Talker_Object__table__C = 0;

/* text__C */
__FAR__ const CT__hello_mod_Talker_text hello_mod_Talker_text__C = "Goodnight Moon";

/* count__C */
__FAR__ const CT__hello_mod_Talker_count hello_mod_Talker_count__C = (xdc_Int)0x3;


/*
 * ======== xdc.runtime.Assert INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Assert_Module__ xdc_runtime_Assert_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Assert_Module__diagsEnabled xdc_runtime_Assert_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Assert_Module__diagsIncluded xdc_runtime_Assert_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Assert_Module__diagsMask xdc_runtime_Assert_Module__diagsMask__C = ((CT__xdc_runtime_Assert_Module__diagsMask)((void*)&xdc_runtime_Assert_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Assert_Module__gateObj xdc_runtime_Assert_Module__gateObj__C = ((CT__xdc_runtime_Assert_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Assert_Module__gatePrms xdc_runtime_Assert_Module__gatePrms__C = ((CT__xdc_runtime_Assert_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Assert_Module__id xdc_runtime_Assert_Module__id__C = (xdc_Bits16)0x8002;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Assert_Module__loggerDefined xdc_runtime_Assert_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Assert_Module__loggerObj xdc_runtime_Assert_Module__loggerObj__C = ((CT__xdc_runtime_Assert_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Assert_Module__loggerFxn4 xdc_runtime_Assert_Module__loggerFxn4__C = ((CT__xdc_runtime_Assert_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Assert_Module__loggerFxn8 xdc_runtime_Assert_Module__loggerFxn8__C = ((CT__xdc_runtime_Assert_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Assert_Module__startupDoneFxn xdc_runtime_Assert_Module__startupDoneFxn__C = ((CT__xdc_runtime_Assert_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Assert_Object__count xdc_runtime_Assert_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Assert_Object__heap xdc_runtime_Assert_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Assert_Object__sizeof xdc_runtime_Assert_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Assert_Object__table xdc_runtime_Assert_Object__table__C = 0;

/* E_assertFailed__C */
__FAR__ const CT__xdc_runtime_Assert_E_assertFailed xdc_runtime_Assert_E_assertFailed__C = (((xdc_runtime_Error_Id)309) << 16 | 0);


/*
 * ======== xdc.runtime.Core INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Core_Module__ xdc_runtime_Core_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Core_Module__diagsEnabled xdc_runtime_Core_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Core_Module__diagsIncluded xdc_runtime_Core_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Core_Module__diagsMask xdc_runtime_Core_Module__diagsMask__C = ((CT__xdc_runtime_Core_Module__diagsMask)((void*)&xdc_runtime_Core_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Core_Module__gateObj xdc_runtime_Core_Module__gateObj__C = ((CT__xdc_runtime_Core_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Core_Module__gatePrms xdc_runtime_Core_Module__gatePrms__C = ((CT__xdc_runtime_Core_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Core_Module__id xdc_runtime_Core_Module__id__C = (xdc_Bits16)0x8003;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Core_Module__loggerDefined xdc_runtime_Core_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Core_Module__loggerObj xdc_runtime_Core_Module__loggerObj__C = ((CT__xdc_runtime_Core_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Core_Module__loggerFxn4 xdc_runtime_Core_Module__loggerFxn4__C = ((CT__xdc_runtime_Core_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Core_Module__loggerFxn8 xdc_runtime_Core_Module__loggerFxn8__C = ((CT__xdc_runtime_Core_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Core_Module__startupDoneFxn xdc_runtime_Core_Module__startupDoneFxn__C = ((CT__xdc_runtime_Core_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Core_Object__count xdc_runtime_Core_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Core_Object__heap xdc_runtime_Core_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Core_Object__sizeof xdc_runtime_Core_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Core_Object__table xdc_runtime_Core_Object__table__C = 0;

/* A_initializedParams__C */
__FAR__ const CT__xdc_runtime_Core_A_initializedParams xdc_runtime_Core_A_initializedParams__C = (((xdc_runtime_Assert_Id)1) << 16 | 16);


/*
 * ======== xdc.runtime.Defaults INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Defaults_Module__ xdc_runtime_Defaults_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__diagsEnabled xdc_runtime_Defaults_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__diagsIncluded xdc_runtime_Defaults_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__diagsMask xdc_runtime_Defaults_Module__diagsMask__C = ((CT__xdc_runtime_Defaults_Module__diagsMask)((void*)&xdc_runtime_Defaults_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__gateObj xdc_runtime_Defaults_Module__gateObj__C = ((CT__xdc_runtime_Defaults_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__gatePrms xdc_runtime_Defaults_Module__gatePrms__C = ((CT__xdc_runtime_Defaults_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__id xdc_runtime_Defaults_Module__id__C = (xdc_Bits16)0x8004;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerDefined xdc_runtime_Defaults_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerObj xdc_runtime_Defaults_Module__loggerObj__C = ((CT__xdc_runtime_Defaults_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerFxn4 xdc_runtime_Defaults_Module__loggerFxn4__C = ((CT__xdc_runtime_Defaults_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__loggerFxn8 xdc_runtime_Defaults_Module__loggerFxn8__C = ((CT__xdc_runtime_Defaults_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Defaults_Module__startupDoneFxn xdc_runtime_Defaults_Module__startupDoneFxn__C = ((CT__xdc_runtime_Defaults_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Defaults_Object__count xdc_runtime_Defaults_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Defaults_Object__heap xdc_runtime_Defaults_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Defaults_Object__sizeof xdc_runtime_Defaults_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Defaults_Object__table xdc_runtime_Defaults_Object__table__C = 0;


/*
 * ======== xdc.runtime.Diags INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Diags_Module__ xdc_runtime_Diags_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Diags_Module__diagsEnabled xdc_runtime_Diags_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Diags_Module__diagsIncluded xdc_runtime_Diags_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Diags_Module__diagsMask xdc_runtime_Diags_Module__diagsMask__C = ((CT__xdc_runtime_Diags_Module__diagsMask)((void*)&xdc_runtime_Diags_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Diags_Module__gateObj xdc_runtime_Diags_Module__gateObj__C = ((CT__xdc_runtime_Diags_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Diags_Module__gatePrms xdc_runtime_Diags_Module__gatePrms__C = ((CT__xdc_runtime_Diags_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Diags_Module__id xdc_runtime_Diags_Module__id__C = (xdc_Bits16)0x8005;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Diags_Module__loggerDefined xdc_runtime_Diags_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Diags_Module__loggerObj xdc_runtime_Diags_Module__loggerObj__C = ((CT__xdc_runtime_Diags_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Diags_Module__loggerFxn4 xdc_runtime_Diags_Module__loggerFxn4__C = ((CT__xdc_runtime_Diags_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Diags_Module__loggerFxn8 xdc_runtime_Diags_Module__loggerFxn8__C = ((CT__xdc_runtime_Diags_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Diags_Module__startupDoneFxn xdc_runtime_Diags_Module__startupDoneFxn__C = ((CT__xdc_runtime_Diags_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Diags_Object__count xdc_runtime_Diags_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Diags_Object__heap xdc_runtime_Diags_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Diags_Object__sizeof xdc_runtime_Diags_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Diags_Object__table xdc_runtime_Diags_Object__table__C = 0;

/* setMaskEnabled__C */
__FAR__ const CT__xdc_runtime_Diags_setMaskEnabled xdc_runtime_Diags_setMaskEnabled__C = 0;

/* dictBase__C */
__FAR__ const CT__xdc_runtime_Diags_dictBase xdc_runtime_Diags_dictBase__C = ((CT__xdc_runtime_Diags_dictBase)0);


/*
 * ======== xdc.runtime.Error INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Error_Module__ xdc_runtime_Error_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__state__V */
xdc_runtime_Error_Module_State__ xdc_runtime_Error_Module__state__V = {
    (xdc_UInt16)0x0,  /* count */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Error_Module__diagsEnabled xdc_runtime_Error_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Error_Module__diagsIncluded xdc_runtime_Error_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Error_Module__diagsMask xdc_runtime_Error_Module__diagsMask__C = ((CT__xdc_runtime_Error_Module__diagsMask)((void*)&xdc_runtime_Error_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Error_Module__gateObj xdc_runtime_Error_Module__gateObj__C = ((CT__xdc_runtime_Error_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Error_Module__gatePrms xdc_runtime_Error_Module__gatePrms__C = ((CT__xdc_runtime_Error_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Error_Module__id xdc_runtime_Error_Module__id__C = (xdc_Bits16)0x8006;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Error_Module__loggerDefined xdc_runtime_Error_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Error_Module__loggerObj xdc_runtime_Error_Module__loggerObj__C = ((CT__xdc_runtime_Error_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Error_Module__loggerFxn4 xdc_runtime_Error_Module__loggerFxn4__C = ((CT__xdc_runtime_Error_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Error_Module__loggerFxn8 xdc_runtime_Error_Module__loggerFxn8__C = ((CT__xdc_runtime_Error_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Error_Module__startupDoneFxn xdc_runtime_Error_Module__startupDoneFxn__C = ((CT__xdc_runtime_Error_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Error_Object__count xdc_runtime_Error_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Error_Object__heap xdc_runtime_Error_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Error_Object__sizeof xdc_runtime_Error_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Error_Object__table xdc_runtime_Error_Object__table__C = 0;

/* E_generic__C */
__FAR__ const CT__xdc_runtime_Error_E_generic xdc_runtime_Error_E_generic__C = (((xdc_runtime_Error_Id)331) << 16 | 0);

/* E_memory__C */
__FAR__ const CT__xdc_runtime_Error_E_memory xdc_runtime_Error_E_memory__C = (((xdc_runtime_Error_Id)349) << 16 | 0);

/* policy__C */
__FAR__ const CT__xdc_runtime_Error_policy xdc_runtime_Error_policy__C = xdc_runtime_Error_UNWIND;

/* raiseHook__C */
__FAR__ const CT__xdc_runtime_Error_raiseHook xdc_runtime_Error_raiseHook__C = ((CT__xdc_runtime_Error_raiseHook)((xdc_Fxn)xdc_runtime_Error_print__E));

/* maxDepth__C */
__FAR__ const CT__xdc_runtime_Error_maxDepth xdc_runtime_Error_maxDepth__C = (xdc_UInt16)0x10;


/*
 * ======== xdc.runtime.Gate INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Gate_Module__ xdc_runtime_Gate_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Gate_Module__diagsEnabled xdc_runtime_Gate_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Gate_Module__diagsIncluded xdc_runtime_Gate_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Gate_Module__diagsMask xdc_runtime_Gate_Module__diagsMask__C = ((CT__xdc_runtime_Gate_Module__diagsMask)((void*)&xdc_runtime_Gate_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Gate_Module__gateObj xdc_runtime_Gate_Module__gateObj__C = ((CT__xdc_runtime_Gate_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Gate_Module__gatePrms xdc_runtime_Gate_Module__gatePrms__C = ((CT__xdc_runtime_Gate_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Gate_Module__id xdc_runtime_Gate_Module__id__C = (xdc_Bits16)0x8007;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Gate_Module__loggerDefined xdc_runtime_Gate_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Gate_Module__loggerObj xdc_runtime_Gate_Module__loggerObj__C = ((CT__xdc_runtime_Gate_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Gate_Module__loggerFxn4 xdc_runtime_Gate_Module__loggerFxn4__C = ((CT__xdc_runtime_Gate_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Gate_Module__loggerFxn8 xdc_runtime_Gate_Module__loggerFxn8__C = ((CT__xdc_runtime_Gate_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Gate_Module__startupDoneFxn xdc_runtime_Gate_Module__startupDoneFxn__C = ((CT__xdc_runtime_Gate_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Gate_Object__count xdc_runtime_Gate_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Gate_Object__heap xdc_runtime_Gate_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Gate_Object__sizeof xdc_runtime_Gate_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Gate_Object__table xdc_runtime_Gate_Object__table__C = 0;


/*
 * ======== xdc.runtime.GateNull INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc xdc_runtime_GateNull_Object__DESC__C;

/* Object__PARAMS__C */
__FAR__ const xdc_runtime_GateNull_Params xdc_runtime_GateNull_Object__PARAMS__C = {
    sizeof (xdc_runtime_GateNull_Params), /* __size */
    0, /* __self */
    0, /* __fxns */
    (xdc_runtime_IInstance_Params*)&xdc_runtime_GateNull_Object__PARAMS__C.__iprms, /* instance */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
xdc_runtime_GateNull_Module__ xdc_runtime_GateNull_Module__root__V = {{
    {&xdc_runtime_GateNull_Module__root__V.hdr.link,  /* link.next */
    &xdc_runtime_GateNull_Module__root__V.hdr.link},  /* link.prev */
    (UChar*)0,  /* instArrBeg */
    (UChar*)(0+-1),  /* instArrEnd */
    sizeof (xdc_runtime_GateNull_Object__),  /* instSize */
    0,  /* cur */
    0,  /* diagsMask */
}};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__diagsEnabled xdc_runtime_GateNull_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__diagsIncluded xdc_runtime_GateNull_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__diagsMask xdc_runtime_GateNull_Module__diagsMask__C = ((CT__xdc_runtime_GateNull_Module__diagsMask)((void*)&xdc_runtime_GateNull_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__gateObj xdc_runtime_GateNull_Module__gateObj__C = ((CT__xdc_runtime_GateNull_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__gatePrms xdc_runtime_GateNull_Module__gatePrms__C = ((CT__xdc_runtime_GateNull_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__id xdc_runtime_GateNull_Module__id__C = (xdc_Bits16)0x8008;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__loggerDefined xdc_runtime_GateNull_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__loggerObj xdc_runtime_GateNull_Module__loggerObj__C = ((CT__xdc_runtime_GateNull_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__loggerFxn4 xdc_runtime_GateNull_Module__loggerFxn4__C = ((CT__xdc_runtime_GateNull_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__loggerFxn8 xdc_runtime_GateNull_Module__loggerFxn8__C = ((CT__xdc_runtime_GateNull_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_GateNull_Module__startupDoneFxn xdc_runtime_GateNull_Module__startupDoneFxn__C = ((CT__xdc_runtime_GateNull_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_GateNull_Object__count xdc_runtime_GateNull_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_GateNull_Object__heap xdc_runtime_GateNull_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_GateNull_Object__sizeof xdc_runtime_GateNull_Object__sizeof__C = sizeof(xdc_runtime_GateNull_Object__);

/* Object__table__C */
__FAR__ const CT__xdc_runtime_GateNull_Object__table xdc_runtime_GateNull_Object__table__C = 0;


/*
 * ======== xdc.runtime.HeapStd INITIALIZERS ========
 */

/* Object__DESC__C */
__FAR__ const xdc_runtime_Core_ObjDesc xdc_runtime_HeapStd_Object__DESC__C;

/* Object__PARAMS__C */
__FAR__ const xdc_runtime_HeapStd_Params xdc_runtime_HeapStd_Object__PARAMS__C = {
    sizeof (xdc_runtime_HeapStd_Params), /* __size */
    0, /* __self */
    0, /* __fxns */
    (xdc_runtime_IInstance_Params*)&xdc_runtime_HeapStd_Object__PARAMS__C.__iprms, /* instance */
    ((xdc_UArg)((void*)0x0)),  /* size */
    {
        sizeof (xdc_runtime_IInstance_Params), /* __size */
        0,  /* name */
    },  /* instance */
};

/* Module__root__V */
xdc_runtime_HeapStd_Module__ xdc_runtime_HeapStd_Module__root__V = {{
    {&xdc_runtime_HeapStd_Module__root__V.hdr.link,  /* link.next */
    &xdc_runtime_HeapStd_Module__root__V.hdr.link},  /* link.prev */
    (UChar*)xdc_runtime_HeapStd_Object__table__V,  /* instArrBeg */
    (UChar*)(xdc_runtime_HeapStd_Object__table__V+0),  /* instArrEnd */
    sizeof (xdc_runtime_HeapStd_Object__),  /* instSize */
    0,  /* cur */
    0,  /* diagsMask */
}};

/* Object__table__V */
xdc_runtime_HeapStd_Object__ xdc_runtime_HeapStd_Object__table__V[1] = {
    {/* instance#0 */
        &xdc_runtime_HeapStd_Module__FXNS__C,
        ((xdc_UArg)((void*)0x1000)),  /* remainSize */
        ((xdc_UArg)((void*)0x1000)),  /* startSize */
    },
};

/* Module__state__V */
xdc_runtime_HeapStd_Module_State__ xdc_runtime_HeapStd_Module__state__V = {
    ((xdc_UArg)((void*)0x0)),  /* remainRTSSize */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__diagsEnabled xdc_runtime_HeapStd_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__diagsIncluded xdc_runtime_HeapStd_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__diagsMask xdc_runtime_HeapStd_Module__diagsMask__C = ((CT__xdc_runtime_HeapStd_Module__diagsMask)((void*)&xdc_runtime_HeapStd_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__gateObj xdc_runtime_HeapStd_Module__gateObj__C = ((CT__xdc_runtime_HeapStd_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__gatePrms xdc_runtime_HeapStd_Module__gatePrms__C = ((CT__xdc_runtime_HeapStd_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__id xdc_runtime_HeapStd_Module__id__C = (xdc_Bits16)0x800c;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__loggerDefined xdc_runtime_HeapStd_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__loggerObj xdc_runtime_HeapStd_Module__loggerObj__C = ((CT__xdc_runtime_HeapStd_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__loggerFxn4 xdc_runtime_HeapStd_Module__loggerFxn4__C = ((CT__xdc_runtime_HeapStd_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__loggerFxn8 xdc_runtime_HeapStd_Module__loggerFxn8__C = ((CT__xdc_runtime_HeapStd_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_HeapStd_Module__startupDoneFxn xdc_runtime_HeapStd_Module__startupDoneFxn__C = ((CT__xdc_runtime_HeapStd_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_HeapStd_Object__count xdc_runtime_HeapStd_Object__count__C = 1;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_HeapStd_Object__heap xdc_runtime_HeapStd_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_HeapStd_Object__sizeof xdc_runtime_HeapStd_Object__sizeof__C = sizeof(xdc_runtime_HeapStd_Object__);

/* Object__table__C */
__FAR__ const CT__xdc_runtime_HeapStd_Object__table xdc_runtime_HeapStd_Object__table__C = xdc_runtime_HeapStd_Object__table__V;

/* E_noRTSMemory__C */
__FAR__ const CT__xdc_runtime_HeapStd_E_noRTSMemory xdc_runtime_HeapStd_E_noRTSMemory__C = (((xdc_runtime_Error_Id)421) << 16 | 0);

/* A_zeroSize__C */
__FAR__ const CT__xdc_runtime_HeapStd_A_zeroSize xdc_runtime_HeapStd_A_zeroSize__C = (((xdc_runtime_Assert_Id)95) << 16 | 16);

/* A_invalidTotalFreeSize__C */
__FAR__ const CT__xdc_runtime_HeapStd_A_invalidTotalFreeSize xdc_runtime_HeapStd_A_invalidTotalFreeSize__C = (((xdc_runtime_Assert_Id)140) << 16 | 16);

/* A_invalidAlignment__C */
__FAR__ const CT__xdc_runtime_HeapStd_A_invalidAlignment xdc_runtime_HeapStd_A_invalidAlignment__C = (((xdc_runtime_Assert_Id)201) << 16 | 16);


/*
 * ======== xdc.runtime.Log INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Log_Module__ xdc_runtime_Log_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Log_Module__diagsEnabled xdc_runtime_Log_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Log_Module__diagsIncluded xdc_runtime_Log_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Log_Module__diagsMask xdc_runtime_Log_Module__diagsMask__C = ((CT__xdc_runtime_Log_Module__diagsMask)((void*)&xdc_runtime_Log_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Log_Module__gateObj xdc_runtime_Log_Module__gateObj__C = ((CT__xdc_runtime_Log_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Log_Module__gatePrms xdc_runtime_Log_Module__gatePrms__C = ((CT__xdc_runtime_Log_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Log_Module__id xdc_runtime_Log_Module__id__C = (xdc_Bits16)0x8009;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Log_Module__loggerDefined xdc_runtime_Log_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Log_Module__loggerObj xdc_runtime_Log_Module__loggerObj__C = ((CT__xdc_runtime_Log_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Log_Module__loggerFxn4 xdc_runtime_Log_Module__loggerFxn4__C = ((CT__xdc_runtime_Log_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Log_Module__loggerFxn8 xdc_runtime_Log_Module__loggerFxn8__C = ((CT__xdc_runtime_Log_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Log_Module__startupDoneFxn xdc_runtime_Log_Module__startupDoneFxn__C = ((CT__xdc_runtime_Log_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Log_Object__count xdc_runtime_Log_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Log_Object__heap xdc_runtime_Log_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Log_Object__sizeof xdc_runtime_Log_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Log_Object__table xdc_runtime_Log_Object__table__C = 0;

/* L_construct__C */
__FAR__ const CT__xdc_runtime_Log_L_construct xdc_runtime_Log_L_construct__C = (((xdc_runtime_Log_Event)468) << 16 | 4);

/* L_create__C */
__FAR__ const CT__xdc_runtime_Log_L_create xdc_runtime_Log_L_create__C = (((xdc_runtime_Log_Event)492) << 16 | 4);

/* L_destruct__C */
__FAR__ const CT__xdc_runtime_Log_L_destruct xdc_runtime_Log_L_destruct__C = (((xdc_runtime_Log_Event)513) << 16 | 4);

/* L_delete__C */
__FAR__ const CT__xdc_runtime_Log_L_delete xdc_runtime_Log_L_delete__C = (((xdc_runtime_Log_Event)532) << 16 | 4);


/*
 * ======== xdc.runtime.Main INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Main_Module__ xdc_runtime_Main_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Main_Module__diagsEnabled xdc_runtime_Main_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Main_Module__diagsIncluded xdc_runtime_Main_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Main_Module__diagsMask xdc_runtime_Main_Module__diagsMask__C = ((CT__xdc_runtime_Main_Module__diagsMask)((void*)&xdc_runtime_Main_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Main_Module__gateObj xdc_runtime_Main_Module__gateObj__C = ((CT__xdc_runtime_Main_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Main_Module__gatePrms xdc_runtime_Main_Module__gatePrms__C = ((CT__xdc_runtime_Main_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Main_Module__id xdc_runtime_Main_Module__id__C = (xdc_Bits16)0x800a;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Main_Module__loggerDefined xdc_runtime_Main_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Main_Module__loggerObj xdc_runtime_Main_Module__loggerObj__C = ((CT__xdc_runtime_Main_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Main_Module__loggerFxn4 xdc_runtime_Main_Module__loggerFxn4__C = ((CT__xdc_runtime_Main_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Main_Module__loggerFxn8 xdc_runtime_Main_Module__loggerFxn8__C = ((CT__xdc_runtime_Main_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Main_Module__startupDoneFxn xdc_runtime_Main_Module__startupDoneFxn__C = ((CT__xdc_runtime_Main_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Main_Object__count xdc_runtime_Main_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Main_Object__heap xdc_runtime_Main_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Main_Object__sizeof xdc_runtime_Main_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Main_Object__table xdc_runtime_Main_Object__table__C = 0;


/*
 * ======== xdc.runtime.Main_Module_GateProxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.Memory INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Memory_Module__ xdc_runtime_Memory_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__state__V */
xdc_runtime_Memory_Module_State__ xdc_runtime_Memory_Module__state__V = {
    (xdc_SizeT)0x4,  /* maxDefaultTypeAlign */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Memory_Module__diagsEnabled xdc_runtime_Memory_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Memory_Module__diagsIncluded xdc_runtime_Memory_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Memory_Module__diagsMask xdc_runtime_Memory_Module__diagsMask__C = ((CT__xdc_runtime_Memory_Module__diagsMask)((void*)&xdc_runtime_Memory_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Memory_Module__gateObj xdc_runtime_Memory_Module__gateObj__C = ((CT__xdc_runtime_Memory_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Memory_Module__gatePrms xdc_runtime_Memory_Module__gatePrms__C = ((CT__xdc_runtime_Memory_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Memory_Module__id xdc_runtime_Memory_Module__id__C = (xdc_Bits16)0x800b;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Memory_Module__loggerDefined xdc_runtime_Memory_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Memory_Module__loggerObj xdc_runtime_Memory_Module__loggerObj__C = ((CT__xdc_runtime_Memory_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Memory_Module__loggerFxn4 xdc_runtime_Memory_Module__loggerFxn4__C = ((CT__xdc_runtime_Memory_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Memory_Module__loggerFxn8 xdc_runtime_Memory_Module__loggerFxn8__C = ((CT__xdc_runtime_Memory_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Memory_Module__startupDoneFxn xdc_runtime_Memory_Module__startupDoneFxn__C = ((CT__xdc_runtime_Memory_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Memory_Object__count xdc_runtime_Memory_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Memory_Object__heap xdc_runtime_Memory_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Memory_Object__sizeof xdc_runtime_Memory_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Memory_Object__table xdc_runtime_Memory_Object__table__C = 0;

/* defaultHeapInstance__C */
__FAR__ const CT__xdc_runtime_Memory_defaultHeapInstance xdc_runtime_Memory_defaultHeapInstance__C = (xdc_runtime_IHeap_Handle)&xdc_runtime_HeapStd_Object__table__V[0];


/*
 * ======== xdc.runtime.Memory_HeapProxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.Startup INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Startup_Module__ xdc_runtime_Startup_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__state__V */
xdc_runtime_Startup_Module_State__ xdc_runtime_Startup_Module__state__V = {
    ((xdc_Int*)0),  /* stateTab */
    0,  /* execFlag */
    0,  /* rtsDoneFlag */
};

/* --> xdc_runtime_Startup_sfxnTab__A */
const __T1_xdc_runtime_Startup_sfxnTab xdc_runtime_Startup_sfxnTab__A[2] = {
    ((xdc_Int(*)(xdc_Int))((xdc_Fxn)xdc_runtime_System_Module_startup__E)),  /* [0] */
    ((xdc_Int(*)(xdc_Int))((xdc_Fxn)xdc_runtime_SysMin_Module_startup__E)),  /* [1] */
};

/* --> xdc_runtime_Startup_sfxnRts__A */
const __T1_xdc_runtime_Startup_sfxnRts xdc_runtime_Startup_sfxnRts__A[2] = {
    1,  /* [0] */
    1,  /* [1] */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Startup_Module__diagsEnabled xdc_runtime_Startup_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Startup_Module__diagsIncluded xdc_runtime_Startup_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Startup_Module__diagsMask xdc_runtime_Startup_Module__diagsMask__C = ((CT__xdc_runtime_Startup_Module__diagsMask)((void*)&xdc_runtime_Startup_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Startup_Module__gateObj xdc_runtime_Startup_Module__gateObj__C = ((CT__xdc_runtime_Startup_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Startup_Module__gatePrms xdc_runtime_Startup_Module__gatePrms__C = ((CT__xdc_runtime_Startup_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Startup_Module__id xdc_runtime_Startup_Module__id__C = (xdc_Bits16)0x800d;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Startup_Module__loggerDefined xdc_runtime_Startup_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Startup_Module__loggerObj xdc_runtime_Startup_Module__loggerObj__C = ((CT__xdc_runtime_Startup_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Startup_Module__loggerFxn4 xdc_runtime_Startup_Module__loggerFxn4__C = ((CT__xdc_runtime_Startup_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Startup_Module__loggerFxn8 xdc_runtime_Startup_Module__loggerFxn8__C = ((CT__xdc_runtime_Startup_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Startup_Module__startupDoneFxn xdc_runtime_Startup_Module__startupDoneFxn__C = ((CT__xdc_runtime_Startup_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Startup_Object__count xdc_runtime_Startup_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Startup_Object__heap xdc_runtime_Startup_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Startup_Object__sizeof xdc_runtime_Startup_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Startup_Object__table xdc_runtime_Startup_Object__table__C = 0;

/* maxPasses__C */
__FAR__ const CT__xdc_runtime_Startup_maxPasses xdc_runtime_Startup_maxPasses__C = (xdc_Int)0x20;

/* firstFxns__C */
__FAR__ const CT__xdc_runtime_Startup_firstFxns xdc_runtime_Startup_firstFxns__C = {0, 0};

/* lastFxns__C */
__FAR__ const CT__xdc_runtime_Startup_lastFxns xdc_runtime_Startup_lastFxns__C = {0, 0};

/* startModsFxn__C */
__FAR__ const CT__xdc_runtime_Startup_startModsFxn xdc_runtime_Startup_startModsFxn__C = ((CT__xdc_runtime_Startup_startModsFxn)((xdc_Fxn)xdc_runtime_Startup_startMods__I));

/* execImpl__C */
__FAR__ const CT__xdc_runtime_Startup_execImpl xdc_runtime_Startup_execImpl__C = ((CT__xdc_runtime_Startup_execImpl)((xdc_Fxn)xdc_runtime_Startup_exec__I));

/* sfxnTab__C */
__FAR__ const CT__xdc_runtime_Startup_sfxnTab xdc_runtime_Startup_sfxnTab__C = ((CT__xdc_runtime_Startup_sfxnTab)xdc_runtime_Startup_sfxnTab__A);

/* sfxnRts__C */
__FAR__ const CT__xdc_runtime_Startup_sfxnRts xdc_runtime_Startup_sfxnRts__C = ((CT__xdc_runtime_Startup_sfxnRts)xdc_runtime_Startup_sfxnRts__A);


/*
 * ======== xdc.runtime.SysMin INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_SysMin_Module__ xdc_runtime_SysMin_Module__root__V = {
    {0},  /* diagsMask */
};

/* --> xdc_runtime_SysMin_Module_State_0_outbuf__A */
__T1_xdc_runtime_SysMin_Module_State__outbuf xdc_runtime_SysMin_Module_State_0_outbuf__A[1024];

/* Module__state__V */
xdc_runtime_SysMin_Module_State__ xdc_runtime_SysMin_Module__state__V = {
    xdc_runtime_SysMin_Module_State_0_outbuf__A,  /* outbuf */
    (xdc_UInt)0x0,  /* outidx */
    0,  /* wrapped */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__diagsEnabled xdc_runtime_SysMin_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__diagsIncluded xdc_runtime_SysMin_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__diagsMask xdc_runtime_SysMin_Module__diagsMask__C = ((CT__xdc_runtime_SysMin_Module__diagsMask)((void*)&xdc_runtime_SysMin_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__gateObj xdc_runtime_SysMin_Module__gateObj__C = ((CT__xdc_runtime_SysMin_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__gatePrms xdc_runtime_SysMin_Module__gatePrms__C = ((CT__xdc_runtime_SysMin_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__id xdc_runtime_SysMin_Module__id__C = (xdc_Bits16)0x800f;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerDefined xdc_runtime_SysMin_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerObj xdc_runtime_SysMin_Module__loggerObj__C = ((CT__xdc_runtime_SysMin_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerFxn4 xdc_runtime_SysMin_Module__loggerFxn4__C = ((CT__xdc_runtime_SysMin_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__loggerFxn8 xdc_runtime_SysMin_Module__loggerFxn8__C = ((CT__xdc_runtime_SysMin_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_SysMin_Module__startupDoneFxn xdc_runtime_SysMin_Module__startupDoneFxn__C = ((CT__xdc_runtime_SysMin_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_SysMin_Object__count xdc_runtime_SysMin_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_SysMin_Object__heap xdc_runtime_SysMin_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_SysMin_Object__sizeof xdc_runtime_SysMin_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_SysMin_Object__table xdc_runtime_SysMin_Object__table__C = 0;

/* bufSize__C */
__FAR__ const CT__xdc_runtime_SysMin_bufSize xdc_runtime_SysMin_bufSize__C = (xdc_SizeT)0x400;

/* flushAtExit__C */
__FAR__ const CT__xdc_runtime_SysMin_flushAtExit xdc_runtime_SysMin_flushAtExit__C = 1;

/* outputFxn__C */
__FAR__ const CT__xdc_runtime_SysMin_outputFxn xdc_runtime_SysMin_outputFxn__C = ((CT__xdc_runtime_SysMin_outputFxn)0);

/* outputFunc__C */
__FAR__ const CT__xdc_runtime_SysMin_outputFunc xdc_runtime_SysMin_outputFunc__C = ((CT__xdc_runtime_SysMin_outputFunc)((xdc_Fxn)xdc_runtime_SysMin_output__I));


/*
 * ======== xdc.runtime.System INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_System_Module__ xdc_runtime_System_Module__root__V = {
    {0},  /* diagsMask */
};

/* --> xdc_runtime_System_Module_State_0_atexitHandlers__A */
__T1_xdc_runtime_System_Module_State__atexitHandlers xdc_runtime_System_Module_State_0_atexitHandlers__A[8] = {
    ((xdc_Void(*)(xdc_Int))0),  /* [0] */
    ((xdc_Void(*)(xdc_Int))0),  /* [1] */
    ((xdc_Void(*)(xdc_Int))0),  /* [2] */
    ((xdc_Void(*)(xdc_Int))0),  /* [3] */
    ((xdc_Void(*)(xdc_Int))0),  /* [4] */
    ((xdc_Void(*)(xdc_Int))0),  /* [5] */
    ((xdc_Void(*)(xdc_Int))0),  /* [6] */
    ((xdc_Void(*)(xdc_Int))0),  /* [7] */
};

/* Module__state__V */
xdc_runtime_System_Module_State__ xdc_runtime_System_Module__state__V = {
    xdc_runtime_System_Module_State_0_atexitHandlers__A,  /* atexitHandlers */
    (xdc_Int)0x0,  /* numAtexitHandlers */
    (xdc_Int)0xcafe,  /* exitStatus */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_System_Module__diagsEnabled xdc_runtime_System_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_System_Module__diagsIncluded xdc_runtime_System_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_System_Module__diagsMask xdc_runtime_System_Module__diagsMask__C = ((CT__xdc_runtime_System_Module__diagsMask)((void*)&xdc_runtime_System_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_System_Module__gateObj xdc_runtime_System_Module__gateObj__C = ((CT__xdc_runtime_System_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_System_Module__gatePrms xdc_runtime_System_Module__gatePrms__C = ((CT__xdc_runtime_System_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_System_Module__id xdc_runtime_System_Module__id__C = (xdc_Bits16)0x800e;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_System_Module__loggerDefined xdc_runtime_System_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_System_Module__loggerObj xdc_runtime_System_Module__loggerObj__C = ((CT__xdc_runtime_System_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_System_Module__loggerFxn4 xdc_runtime_System_Module__loggerFxn4__C = ((CT__xdc_runtime_System_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_System_Module__loggerFxn8 xdc_runtime_System_Module__loggerFxn8__C = ((CT__xdc_runtime_System_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_System_Module__startupDoneFxn xdc_runtime_System_Module__startupDoneFxn__C = ((CT__xdc_runtime_System_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_System_Object__count xdc_runtime_System_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_System_Object__heap xdc_runtime_System_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_System_Object__sizeof xdc_runtime_System_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_System_Object__table xdc_runtime_System_Object__table__C = 0;

/* A_cannotFitIntoArg__C */
__FAR__ const CT__xdc_runtime_System_A_cannotFitIntoArg xdc_runtime_System_A_cannotFitIntoArg__C = (((xdc_runtime_Assert_Id)261) << 16 | 16);

/* maxAtexitHandlers__C */
__FAR__ const CT__xdc_runtime_System_maxAtexitHandlers xdc_runtime_System_maxAtexitHandlers__C = (xdc_Int)0x8;

/* extendFxn__C */
__FAR__ const CT__xdc_runtime_System_extendFxn xdc_runtime_System_extendFxn__C = ((CT__xdc_runtime_System_extendFxn)((xdc_Fxn)xdc_runtime_System_printfExtend__I));


/*
 * ======== xdc.runtime.System_Module_GateProxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.System_SupportProxy INITIALIZERS ========
 */


/*
 * ======== xdc.runtime.Text INITIALIZERS ========
 */

/* Module__root__V */
xdc_runtime_Text_Module__ xdc_runtime_Text_Module__root__V = {
    {0},  /* diagsMask */
};

/* Module__state__V */
xdc_runtime_Text_Module_State__ xdc_runtime_Text_Module__state__V = {
    ((xdc_Ptr)((void*)&xdc_runtime_Text_charTab__A[0])),  /* charBase */
    ((xdc_Ptr)((void*)&xdc_runtime_Text_nodeTab__A[0])),  /* nodeBase */
};

/* --> xdc_runtime_Text_charTab__A */
const __T1_xdc_runtime_Text_charTab xdc_runtime_Text_charTab__A[680] = {
    (xdc_Char)0x0,  /* [0] */
    (xdc_Char)0x41,  /* [1] */
    (xdc_Char)0x5f,  /* [2] */
    (xdc_Char)0x69,  /* [3] */
    (xdc_Char)0x6e,  /* [4] */
    (xdc_Char)0x69,  /* [5] */
    (xdc_Char)0x74,  /* [6] */
    (xdc_Char)0x69,  /* [7] */
    (xdc_Char)0x61,  /* [8] */
    (xdc_Char)0x6c,  /* [9] */
    (xdc_Char)0x69,  /* [10] */
    (xdc_Char)0x7a,  /* [11] */
    (xdc_Char)0x65,  /* [12] */
    (xdc_Char)0x64,  /* [13] */
    (xdc_Char)0x50,  /* [14] */
    (xdc_Char)0x61,  /* [15] */
    (xdc_Char)0x72,  /* [16] */
    (xdc_Char)0x61,  /* [17] */
    (xdc_Char)0x6d,  /* [18] */
    (xdc_Char)0x73,  /* [19] */
    (xdc_Char)0x3a,  /* [20] */
    (xdc_Char)0x20,  /* [21] */
    (xdc_Char)0x75,  /* [22] */
    (xdc_Char)0x6e,  /* [23] */
    (xdc_Char)0x69,  /* [24] */
    (xdc_Char)0x6e,  /* [25] */
    (xdc_Char)0x69,  /* [26] */
    (xdc_Char)0x74,  /* [27] */
    (xdc_Char)0x69,  /* [28] */
    (xdc_Char)0x61,  /* [29] */
    (xdc_Char)0x6c,  /* [30] */
    (xdc_Char)0x69,  /* [31] */
    (xdc_Char)0x7a,  /* [32] */
    (xdc_Char)0x65,  /* [33] */
    (xdc_Char)0x64,  /* [34] */
    (xdc_Char)0x20,  /* [35] */
    (xdc_Char)0x50,  /* [36] */
    (xdc_Char)0x61,  /* [37] */
    (xdc_Char)0x72,  /* [38] */
    (xdc_Char)0x61,  /* [39] */
    (xdc_Char)0x6d,  /* [40] */
    (xdc_Char)0x73,  /* [41] */
    (xdc_Char)0x20,  /* [42] */
    (xdc_Char)0x73,  /* [43] */
    (xdc_Char)0x74,  /* [44] */
    (xdc_Char)0x72,  /* [45] */
    (xdc_Char)0x75,  /* [46] */
    (xdc_Char)0x63,  /* [47] */
    (xdc_Char)0x74,  /* [48] */
    (xdc_Char)0x0,  /* [49] */
    (xdc_Char)0x48,  /* [50] */
    (xdc_Char)0x65,  /* [51] */
    (xdc_Char)0x61,  /* [52] */
    (xdc_Char)0x70,  /* [53] */
    (xdc_Char)0x4d,  /* [54] */
    (xdc_Char)0x69,  /* [55] */
    (xdc_Char)0x6e,  /* [56] */
    (xdc_Char)0x5f,  /* [57] */
    (xdc_Char)0x63,  /* [58] */
    (xdc_Char)0x72,  /* [59] */
    (xdc_Char)0x65,  /* [60] */
    (xdc_Char)0x61,  /* [61] */
    (xdc_Char)0x74,  /* [62] */
    (xdc_Char)0x65,  /* [63] */
    (xdc_Char)0x20,  /* [64] */
    (xdc_Char)0x63,  /* [65] */
    (xdc_Char)0x61,  /* [66] */
    (xdc_Char)0x6e,  /* [67] */
    (xdc_Char)0x6e,  /* [68] */
    (xdc_Char)0x6f,  /* [69] */
    (xdc_Char)0x74,  /* [70] */
    (xdc_Char)0x20,  /* [71] */
    (xdc_Char)0x68,  /* [72] */
    (xdc_Char)0x61,  /* [73] */
    (xdc_Char)0x76,  /* [74] */
    (xdc_Char)0x65,  /* [75] */
    (xdc_Char)0x20,  /* [76] */
    (xdc_Char)0x61,  /* [77] */
    (xdc_Char)0x20,  /* [78] */
    (xdc_Char)0x7a,  /* [79] */
    (xdc_Char)0x65,  /* [80] */
    (xdc_Char)0x72,  /* [81] */
    (xdc_Char)0x6f,  /* [82] */
    (xdc_Char)0x20,  /* [83] */
    (xdc_Char)0x73,  /* [84] */
    (xdc_Char)0x69,  /* [85] */
    (xdc_Char)0x7a,  /* [86] */
    (xdc_Char)0x65,  /* [87] */
    (xdc_Char)0x20,  /* [88] */
    (xdc_Char)0x76,  /* [89] */
    (xdc_Char)0x61,  /* [90] */
    (xdc_Char)0x6c,  /* [91] */
    (xdc_Char)0x75,  /* [92] */
    (xdc_Char)0x65,  /* [93] */
    (xdc_Char)0x0,  /* [94] */
    (xdc_Char)0x48,  /* [95] */
    (xdc_Char)0x65,  /* [96] */
    (xdc_Char)0x61,  /* [97] */
    (xdc_Char)0x70,  /* [98] */
    (xdc_Char)0x53,  /* [99] */
    (xdc_Char)0x74,  /* [100] */
    (xdc_Char)0x64,  /* [101] */
    (xdc_Char)0x5f,  /* [102] */
    (xdc_Char)0x63,  /* [103] */
    (xdc_Char)0x72,  /* [104] */
    (xdc_Char)0x65,  /* [105] */
    (xdc_Char)0x61,  /* [106] */
    (xdc_Char)0x74,  /* [107] */
    (xdc_Char)0x65,  /* [108] */
    (xdc_Char)0x20,  /* [109] */
    (xdc_Char)0x63,  /* [110] */
    (xdc_Char)0x61,  /* [111] */
    (xdc_Char)0x6e,  /* [112] */
    (xdc_Char)0x6e,  /* [113] */
    (xdc_Char)0x6f,  /* [114] */
    (xdc_Char)0x74,  /* [115] */
    (xdc_Char)0x20,  /* [116] */
    (xdc_Char)0x68,  /* [117] */
    (xdc_Char)0x61,  /* [118] */
    (xdc_Char)0x76,  /* [119] */
    (xdc_Char)0x65,  /* [120] */
    (xdc_Char)0x20,  /* [121] */
    (xdc_Char)0x61,  /* [122] */
    (xdc_Char)0x20,  /* [123] */
    (xdc_Char)0x7a,  /* [124] */
    (xdc_Char)0x65,  /* [125] */
    (xdc_Char)0x72,  /* [126] */
    (xdc_Char)0x6f,  /* [127] */
    (xdc_Char)0x20,  /* [128] */
    (xdc_Char)0x73,  /* [129] */
    (xdc_Char)0x69,  /* [130] */
    (xdc_Char)0x7a,  /* [131] */
    (xdc_Char)0x65,  /* [132] */
    (xdc_Char)0x20,  /* [133] */
    (xdc_Char)0x76,  /* [134] */
    (xdc_Char)0x61,  /* [135] */
    (xdc_Char)0x6c,  /* [136] */
    (xdc_Char)0x75,  /* [137] */
    (xdc_Char)0x65,  /* [138] */
    (xdc_Char)0x0,  /* [139] */
    (xdc_Char)0x48,  /* [140] */
    (xdc_Char)0x65,  /* [141] */
    (xdc_Char)0x61,  /* [142] */
    (xdc_Char)0x70,  /* [143] */
    (xdc_Char)0x53,  /* [144] */
    (xdc_Char)0x74,  /* [145] */
    (xdc_Char)0x64,  /* [146] */
    (xdc_Char)0x20,  /* [147] */
    (xdc_Char)0x69,  /* [148] */
    (xdc_Char)0x6e,  /* [149] */
    (xdc_Char)0x73,  /* [150] */
    (xdc_Char)0x74,  /* [151] */
    (xdc_Char)0x61,  /* [152] */
    (xdc_Char)0x6e,  /* [153] */
    (xdc_Char)0x63,  /* [154] */
    (xdc_Char)0x65,  /* [155] */
    (xdc_Char)0x20,  /* [156] */
    (xdc_Char)0x74,  /* [157] */
    (xdc_Char)0x6f,  /* [158] */
    (xdc_Char)0x74,  /* [159] */
    (xdc_Char)0x61,  /* [160] */
    (xdc_Char)0x6c,  /* [161] */
    (xdc_Char)0x46,  /* [162] */
    (xdc_Char)0x72,  /* [163] */
    (xdc_Char)0x65,  /* [164] */
    (xdc_Char)0x65,  /* [165] */
    (xdc_Char)0x53,  /* [166] */
    (xdc_Char)0x69,  /* [167] */
    (xdc_Char)0x7a,  /* [168] */
    (xdc_Char)0x65,  /* [169] */
    (xdc_Char)0x20,  /* [170] */
    (xdc_Char)0x69,  /* [171] */
    (xdc_Char)0x73,  /* [172] */
    (xdc_Char)0x20,  /* [173] */
    (xdc_Char)0x67,  /* [174] */
    (xdc_Char)0x72,  /* [175] */
    (xdc_Char)0x65,  /* [176] */
    (xdc_Char)0x61,  /* [177] */
    (xdc_Char)0x74,  /* [178] */
    (xdc_Char)0x65,  /* [179] */
    (xdc_Char)0x72,  /* [180] */
    (xdc_Char)0x20,  /* [181] */
    (xdc_Char)0x74,  /* [182] */
    (xdc_Char)0x68,  /* [183] */
    (xdc_Char)0x61,  /* [184] */
    (xdc_Char)0x6e,  /* [185] */
    (xdc_Char)0x20,  /* [186] */
    (xdc_Char)0x73,  /* [187] */
    (xdc_Char)0x74,  /* [188] */
    (xdc_Char)0x61,  /* [189] */
    (xdc_Char)0x72,  /* [190] */
    (xdc_Char)0x74,  /* [191] */
    (xdc_Char)0x69,  /* [192] */
    (xdc_Char)0x6e,  /* [193] */
    (xdc_Char)0x67,  /* [194] */
    (xdc_Char)0x20,  /* [195] */
    (xdc_Char)0x73,  /* [196] */
    (xdc_Char)0x69,  /* [197] */
    (xdc_Char)0x7a,  /* [198] */
    (xdc_Char)0x65,  /* [199] */
    (xdc_Char)0x0,  /* [200] */
    (xdc_Char)0x48,  /* [201] */
    (xdc_Char)0x65,  /* [202] */
    (xdc_Char)0x61,  /* [203] */
    (xdc_Char)0x70,  /* [204] */
    (xdc_Char)0x53,  /* [205] */
    (xdc_Char)0x74,  /* [206] */
    (xdc_Char)0x64,  /* [207] */
    (xdc_Char)0x5f,  /* [208] */
    (xdc_Char)0x61,  /* [209] */
    (xdc_Char)0x6c,  /* [210] */
    (xdc_Char)0x6c,  /* [211] */
    (xdc_Char)0x6f,  /* [212] */
    (xdc_Char)0x63,  /* [213] */
    (xdc_Char)0x20,  /* [214] */
    (xdc_Char)0x2d,  /* [215] */
    (xdc_Char)0x20,  /* [216] */
    (xdc_Char)0x72,  /* [217] */
    (xdc_Char)0x65,  /* [218] */
    (xdc_Char)0x71,  /* [219] */
    (xdc_Char)0x75,  /* [220] */
    (xdc_Char)0x65,  /* [221] */
    (xdc_Char)0x73,  /* [222] */
    (xdc_Char)0x74,  /* [223] */
    (xdc_Char)0x65,  /* [224] */
    (xdc_Char)0x64,  /* [225] */
    (xdc_Char)0x20,  /* [226] */
    (xdc_Char)0x61,  /* [227] */
    (xdc_Char)0x6c,  /* [228] */
    (xdc_Char)0x69,  /* [229] */
    (xdc_Char)0x67,  /* [230] */
    (xdc_Char)0x6e,  /* [231] */
    (xdc_Char)0x6d,  /* [232] */
    (xdc_Char)0x65,  /* [233] */
    (xdc_Char)0x6e,  /* [234] */
    (xdc_Char)0x74,  /* [235] */
    (xdc_Char)0x20,  /* [236] */
    (xdc_Char)0x69,  /* [237] */
    (xdc_Char)0x73,  /* [238] */
    (xdc_Char)0x20,  /* [239] */
    (xdc_Char)0x67,  /* [240] */
    (xdc_Char)0x72,  /* [241] */
    (xdc_Char)0x65,  /* [242] */
    (xdc_Char)0x61,  /* [243] */
    (xdc_Char)0x74,  /* [244] */
    (xdc_Char)0x65,  /* [245] */
    (xdc_Char)0x72,  /* [246] */
    (xdc_Char)0x20,  /* [247] */
    (xdc_Char)0x74,  /* [248] */
    (xdc_Char)0x68,  /* [249] */
    (xdc_Char)0x61,  /* [250] */
    (xdc_Char)0x6e,  /* [251] */
    (xdc_Char)0x20,  /* [252] */
    (xdc_Char)0x61,  /* [253] */
    (xdc_Char)0x6c,  /* [254] */
    (xdc_Char)0x6c,  /* [255] */
    (xdc_Char)0x6f,  /* [256] */
    (xdc_Char)0x77,  /* [257] */
    (xdc_Char)0x65,  /* [258] */
    (xdc_Char)0x64,  /* [259] */
    (xdc_Char)0x0,  /* [260] */
    (xdc_Char)0x41,  /* [261] */
    (xdc_Char)0x5f,  /* [262] */
    (xdc_Char)0x63,  /* [263] */
    (xdc_Char)0x61,  /* [264] */
    (xdc_Char)0x6e,  /* [265] */
    (xdc_Char)0x6e,  /* [266] */
    (xdc_Char)0x6f,  /* [267] */
    (xdc_Char)0x74,  /* [268] */
    (xdc_Char)0x46,  /* [269] */
    (xdc_Char)0x69,  /* [270] */
    (xdc_Char)0x74,  /* [271] */
    (xdc_Char)0x49,  /* [272] */
    (xdc_Char)0x6e,  /* [273] */
    (xdc_Char)0x74,  /* [274] */
    (xdc_Char)0x6f,  /* [275] */
    (xdc_Char)0x41,  /* [276] */
    (xdc_Char)0x72,  /* [277] */
    (xdc_Char)0x67,  /* [278] */
    (xdc_Char)0x3a,  /* [279] */
    (xdc_Char)0x20,  /* [280] */
    (xdc_Char)0x73,  /* [281] */
    (xdc_Char)0x69,  /* [282] */
    (xdc_Char)0x7a,  /* [283] */
    (xdc_Char)0x65,  /* [284] */
    (xdc_Char)0x6f,  /* [285] */
    (xdc_Char)0x66,  /* [286] */
    (xdc_Char)0x28,  /* [287] */
    (xdc_Char)0x46,  /* [288] */
    (xdc_Char)0x6c,  /* [289] */
    (xdc_Char)0x6f,  /* [290] */
    (xdc_Char)0x61,  /* [291] */
    (xdc_Char)0x74,  /* [292] */
    (xdc_Char)0x29,  /* [293] */
    (xdc_Char)0x20,  /* [294] */
    (xdc_Char)0x3e,  /* [295] */
    (xdc_Char)0x20,  /* [296] */
    (xdc_Char)0x73,  /* [297] */
    (xdc_Char)0x69,  /* [298] */
    (xdc_Char)0x7a,  /* [299] */
    (xdc_Char)0x65,  /* [300] */
    (xdc_Char)0x6f,  /* [301] */
    (xdc_Char)0x66,  /* [302] */
    (xdc_Char)0x28,  /* [303] */
    (xdc_Char)0x41,  /* [304] */
    (xdc_Char)0x72,  /* [305] */
    (xdc_Char)0x67,  /* [306] */
    (xdc_Char)0x29,  /* [307] */
    (xdc_Char)0x0,  /* [308] */
    (xdc_Char)0x61,  /* [309] */
    (xdc_Char)0x73,  /* [310] */
    (xdc_Char)0x73,  /* [311] */
    (xdc_Char)0x65,  /* [312] */
    (xdc_Char)0x72,  /* [313] */
    (xdc_Char)0x74,  /* [314] */
    (xdc_Char)0x69,  /* [315] */
    (xdc_Char)0x6f,  /* [316] */
    (xdc_Char)0x6e,  /* [317] */
    (xdc_Char)0x20,  /* [318] */
    (xdc_Char)0x66,  /* [319] */
    (xdc_Char)0x61,  /* [320] */
    (xdc_Char)0x69,  /* [321] */
    (xdc_Char)0x6c,  /* [322] */
    (xdc_Char)0x75,  /* [323] */
    (xdc_Char)0x72,  /* [324] */
    (xdc_Char)0x65,  /* [325] */
    (xdc_Char)0x25,  /* [326] */
    (xdc_Char)0x73,  /* [327] */
    (xdc_Char)0x25,  /* [328] */
    (xdc_Char)0x73,  /* [329] */
    (xdc_Char)0x0,  /* [330] */
    (xdc_Char)0x67,  /* [331] */
    (xdc_Char)0x65,  /* [332] */
    (xdc_Char)0x6e,  /* [333] */
    (xdc_Char)0x65,  /* [334] */
    (xdc_Char)0x72,  /* [335] */
    (xdc_Char)0x69,  /* [336] */
    (xdc_Char)0x63,  /* [337] */
    (xdc_Char)0x20,  /* [338] */
    (xdc_Char)0x65,  /* [339] */
    (xdc_Char)0x72,  /* [340] */
    (xdc_Char)0x72,  /* [341] */
    (xdc_Char)0x6f,  /* [342] */
    (xdc_Char)0x72,  /* [343] */
    (xdc_Char)0x3a,  /* [344] */
    (xdc_Char)0x20,  /* [345] */
    (xdc_Char)0x25,  /* [346] */
    (xdc_Char)0x73,  /* [347] */
    (xdc_Char)0x0,  /* [348] */
    (xdc_Char)0x6f,  /* [349] */
    (xdc_Char)0x75,  /* [350] */
    (xdc_Char)0x74,  /* [351] */
    (xdc_Char)0x20,  /* [352] */
    (xdc_Char)0x6f,  /* [353] */
    (xdc_Char)0x66,  /* [354] */
    (xdc_Char)0x20,  /* [355] */
    (xdc_Char)0x6d,  /* [356] */
    (xdc_Char)0x65,  /* [357] */
    (xdc_Char)0x6d,  /* [358] */
    (xdc_Char)0x6f,  /* [359] */
    (xdc_Char)0x72,  /* [360] */
    (xdc_Char)0x79,  /* [361] */
    (xdc_Char)0x3a,  /* [362] */
    (xdc_Char)0x20,  /* [363] */
    (xdc_Char)0x68,  /* [364] */
    (xdc_Char)0x65,  /* [365] */
    (xdc_Char)0x61,  /* [366] */
    (xdc_Char)0x70,  /* [367] */
    (xdc_Char)0x3d,  /* [368] */
    (xdc_Char)0x30,  /* [369] */
    (xdc_Char)0x78,  /* [370] */
    (xdc_Char)0x25,  /* [371] */
    (xdc_Char)0x78,  /* [372] */
    (xdc_Char)0x2c,  /* [373] */
    (xdc_Char)0x20,  /* [374] */
    (xdc_Char)0x73,  /* [375] */
    (xdc_Char)0x69,  /* [376] */
    (xdc_Char)0x7a,  /* [377] */
    (xdc_Char)0x65,  /* [378] */
    (xdc_Char)0x3d,  /* [379] */
    (xdc_Char)0x25,  /* [380] */
    (xdc_Char)0x75,  /* [381] */
    (xdc_Char)0x0,  /* [382] */
    (xdc_Char)0x66,  /* [383] */
    (xdc_Char)0x72,  /* [384] */
    (xdc_Char)0x65,  /* [385] */
    (xdc_Char)0x65,  /* [386] */
    (xdc_Char)0x28,  /* [387] */
    (xdc_Char)0x29,  /* [388] */
    (xdc_Char)0x20,  /* [389] */
    (xdc_Char)0x69,  /* [390] */
    (xdc_Char)0x6e,  /* [391] */
    (xdc_Char)0x76,  /* [392] */
    (xdc_Char)0x61,  /* [393] */
    (xdc_Char)0x6c,  /* [394] */
    (xdc_Char)0x69,  /* [395] */
    (xdc_Char)0x64,  /* [396] */
    (xdc_Char)0x20,  /* [397] */
    (xdc_Char)0x69,  /* [398] */
    (xdc_Char)0x6e,  /* [399] */
    (xdc_Char)0x20,  /* [400] */
    (xdc_Char)0x67,  /* [401] */
    (xdc_Char)0x72,  /* [402] */
    (xdc_Char)0x6f,  /* [403] */
    (xdc_Char)0x77,  /* [404] */
    (xdc_Char)0x74,  /* [405] */
    (xdc_Char)0x68,  /* [406] */
    (xdc_Char)0x2d,  /* [407] */
    (xdc_Char)0x6f,  /* [408] */
    (xdc_Char)0x6e,  /* [409] */
    (xdc_Char)0x6c,  /* [410] */
    (xdc_Char)0x79,  /* [411] */
    (xdc_Char)0x20,  /* [412] */
    (xdc_Char)0x48,  /* [413] */
    (xdc_Char)0x65,  /* [414] */
    (xdc_Char)0x61,  /* [415] */
    (xdc_Char)0x70,  /* [416] */
    (xdc_Char)0x4d,  /* [417] */
    (xdc_Char)0x69,  /* [418] */
    (xdc_Char)0x6e,  /* [419] */
    (xdc_Char)0x0,  /* [420] */
    (xdc_Char)0x54,  /* [421] */
    (xdc_Char)0x68,  /* [422] */
    (xdc_Char)0x65,  /* [423] */
    (xdc_Char)0x20,  /* [424] */
    (xdc_Char)0x52,  /* [425] */
    (xdc_Char)0x54,  /* [426] */
    (xdc_Char)0x53,  /* [427] */
    (xdc_Char)0x20,  /* [428] */
    (xdc_Char)0x68,  /* [429] */
    (xdc_Char)0x65,  /* [430] */
    (xdc_Char)0x61,  /* [431] */
    (xdc_Char)0x70,  /* [432] */
    (xdc_Char)0x20,  /* [433] */
    (xdc_Char)0x69,  /* [434] */
    (xdc_Char)0x73,  /* [435] */
    (xdc_Char)0x20,  /* [436] */
    (xdc_Char)0x75,  /* [437] */
    (xdc_Char)0x73,  /* [438] */
    (xdc_Char)0x65,  /* [439] */
    (xdc_Char)0x64,  /* [440] */
    (xdc_Char)0x20,  /* [441] */
    (xdc_Char)0x75,  /* [442] */
    (xdc_Char)0x70,  /* [443] */
    (xdc_Char)0x2e,  /* [444] */
    (xdc_Char)0x20,  /* [445] */
    (xdc_Char)0x45,  /* [446] */
    (xdc_Char)0x78,  /* [447] */
    (xdc_Char)0x61,  /* [448] */
    (xdc_Char)0x6d,  /* [449] */
    (xdc_Char)0x69,  /* [450] */
    (xdc_Char)0x6e,  /* [451] */
    (xdc_Char)0x65,  /* [452] */
    (xdc_Char)0x20,  /* [453] */
    (xdc_Char)0x50,  /* [454] */
    (xdc_Char)0x72,  /* [455] */
    (xdc_Char)0x6f,  /* [456] */
    (xdc_Char)0x67,  /* [457] */
    (xdc_Char)0x72,  /* [458] */
    (xdc_Char)0x61,  /* [459] */
    (xdc_Char)0x6d,  /* [460] */
    (xdc_Char)0x2e,  /* [461] */
    (xdc_Char)0x68,  /* [462] */
    (xdc_Char)0x65,  /* [463] */
    (xdc_Char)0x61,  /* [464] */
    (xdc_Char)0x70,  /* [465] */
    (xdc_Char)0x2e,  /* [466] */
    (xdc_Char)0x0,  /* [467] */
    (xdc_Char)0x3c,  /* [468] */
    (xdc_Char)0x2d,  /* [469] */
    (xdc_Char)0x2d,  /* [470] */
    (xdc_Char)0x20,  /* [471] */
    (xdc_Char)0x63,  /* [472] */
    (xdc_Char)0x6f,  /* [473] */
    (xdc_Char)0x6e,  /* [474] */
    (xdc_Char)0x73,  /* [475] */
    (xdc_Char)0x74,  /* [476] */
    (xdc_Char)0x72,  /* [477] */
    (xdc_Char)0x75,  /* [478] */
    (xdc_Char)0x63,  /* [479] */
    (xdc_Char)0x74,  /* [480] */
    (xdc_Char)0x3a,  /* [481] */
    (xdc_Char)0x20,  /* [482] */
    (xdc_Char)0x25,  /* [483] */
    (xdc_Char)0x70,  /* [484] */
    (xdc_Char)0x28,  /* [485] */
    (xdc_Char)0x27,  /* [486] */
    (xdc_Char)0x25,  /* [487] */
    (xdc_Char)0x73,  /* [488] */
    (xdc_Char)0x27,  /* [489] */
    (xdc_Char)0x29,  /* [490] */
    (xdc_Char)0x0,  /* [491] */
    (xdc_Char)0x3c,  /* [492] */
    (xdc_Char)0x2d,  /* [493] */
    (xdc_Char)0x2d,  /* [494] */
    (xdc_Char)0x20,  /* [495] */
    (xdc_Char)0x63,  /* [496] */
    (xdc_Char)0x72,  /* [497] */
    (xdc_Char)0x65,  /* [498] */
    (xdc_Char)0x61,  /* [499] */
    (xdc_Char)0x74,  /* [500] */
    (xdc_Char)0x65,  /* [501] */
    (xdc_Char)0x3a,  /* [502] */
    (xdc_Char)0x20,  /* [503] */
    (xdc_Char)0x25,  /* [504] */
    (xdc_Char)0x70,  /* [505] */
    (xdc_Char)0x28,  /* [506] */
    (xdc_Char)0x27,  /* [507] */
    (xdc_Char)0x25,  /* [508] */
    (xdc_Char)0x73,  /* [509] */
    (xdc_Char)0x27,  /* [510] */
    (xdc_Char)0x29,  /* [511] */
    (xdc_Char)0x0,  /* [512] */
    (xdc_Char)0x2d,  /* [513] */
    (xdc_Char)0x2d,  /* [514] */
    (xdc_Char)0x3e,  /* [515] */
    (xdc_Char)0x20,  /* [516] */
    (xdc_Char)0x64,  /* [517] */
    (xdc_Char)0x65,  /* [518] */
    (xdc_Char)0x73,  /* [519] */
    (xdc_Char)0x74,  /* [520] */
    (xdc_Char)0x72,  /* [521] */
    (xdc_Char)0x75,  /* [522] */
    (xdc_Char)0x63,  /* [523] */
    (xdc_Char)0x74,  /* [524] */
    (xdc_Char)0x3a,  /* [525] */
    (xdc_Char)0x20,  /* [526] */
    (xdc_Char)0x28,  /* [527] */
    (xdc_Char)0x25,  /* [528] */
    (xdc_Char)0x70,  /* [529] */
    (xdc_Char)0x29,  /* [530] */
    (xdc_Char)0x0,  /* [531] */
    (xdc_Char)0x2d,  /* [532] */
    (xdc_Char)0x2d,  /* [533] */
    (xdc_Char)0x3e,  /* [534] */
    (xdc_Char)0x20,  /* [535] */
    (xdc_Char)0x64,  /* [536] */
    (xdc_Char)0x65,  /* [537] */
    (xdc_Char)0x6c,  /* [538] */
    (xdc_Char)0x65,  /* [539] */
    (xdc_Char)0x74,  /* [540] */
    (xdc_Char)0x65,  /* [541] */
    (xdc_Char)0x3a,  /* [542] */
    (xdc_Char)0x20,  /* [543] */
    (xdc_Char)0x28,  /* [544] */
    (xdc_Char)0x25,  /* [545] */
    (xdc_Char)0x70,  /* [546] */
    (xdc_Char)0x29,  /* [547] */
    (xdc_Char)0x0,  /* [548] */
    (xdc_Char)0x78,  /* [549] */
    (xdc_Char)0x64,  /* [550] */
    (xdc_Char)0x63,  /* [551] */
    (xdc_Char)0x2e,  /* [552] */
    (xdc_Char)0x0,  /* [553] */
    (xdc_Char)0x72,  /* [554] */
    (xdc_Char)0x75,  /* [555] */
    (xdc_Char)0x6e,  /* [556] */
    (xdc_Char)0x74,  /* [557] */
    (xdc_Char)0x69,  /* [558] */
    (xdc_Char)0x6d,  /* [559] */
    (xdc_Char)0x65,  /* [560] */
    (xdc_Char)0x2e,  /* [561] */
    (xdc_Char)0x0,  /* [562] */
    (xdc_Char)0x41,  /* [563] */
    (xdc_Char)0x73,  /* [564] */
    (xdc_Char)0x73,  /* [565] */
    (xdc_Char)0x65,  /* [566] */
    (xdc_Char)0x72,  /* [567] */
    (xdc_Char)0x74,  /* [568] */
    (xdc_Char)0x0,  /* [569] */
    (xdc_Char)0x43,  /* [570] */
    (xdc_Char)0x6f,  /* [571] */
    (xdc_Char)0x72,  /* [572] */
    (xdc_Char)0x65,  /* [573] */
    (xdc_Char)0x0,  /* [574] */
    (xdc_Char)0x44,  /* [575] */
    (xdc_Char)0x65,  /* [576] */
    (xdc_Char)0x66,  /* [577] */
    (xdc_Char)0x61,  /* [578] */
    (xdc_Char)0x75,  /* [579] */
    (xdc_Char)0x6c,  /* [580] */
    (xdc_Char)0x74,  /* [581] */
    (xdc_Char)0x73,  /* [582] */
    (xdc_Char)0x0,  /* [583] */
    (xdc_Char)0x44,  /* [584] */
    (xdc_Char)0x69,  /* [585] */
    (xdc_Char)0x61,  /* [586] */
    (xdc_Char)0x67,  /* [587] */
    (xdc_Char)0x73,  /* [588] */
    (xdc_Char)0x0,  /* [589] */
    (xdc_Char)0x45,  /* [590] */
    (xdc_Char)0x72,  /* [591] */
    (xdc_Char)0x72,  /* [592] */
    (xdc_Char)0x6f,  /* [593] */
    (xdc_Char)0x72,  /* [594] */
    (xdc_Char)0x0,  /* [595] */
    (xdc_Char)0x47,  /* [596] */
    (xdc_Char)0x61,  /* [597] */
    (xdc_Char)0x74,  /* [598] */
    (xdc_Char)0x65,  /* [599] */
    (xdc_Char)0x0,  /* [600] */
    (xdc_Char)0x47,  /* [601] */
    (xdc_Char)0x61,  /* [602] */
    (xdc_Char)0x74,  /* [603] */
    (xdc_Char)0x65,  /* [604] */
    (xdc_Char)0x4e,  /* [605] */
    (xdc_Char)0x75,  /* [606] */
    (xdc_Char)0x6c,  /* [607] */
    (xdc_Char)0x6c,  /* [608] */
    (xdc_Char)0x0,  /* [609] */
    (xdc_Char)0x4c,  /* [610] */
    (xdc_Char)0x6f,  /* [611] */
    (xdc_Char)0x67,  /* [612] */
    (xdc_Char)0x0,  /* [613] */
    (xdc_Char)0x4d,  /* [614] */
    (xdc_Char)0x61,  /* [615] */
    (xdc_Char)0x69,  /* [616] */
    (xdc_Char)0x6e,  /* [617] */
    (xdc_Char)0x0,  /* [618] */
    (xdc_Char)0x4d,  /* [619] */
    (xdc_Char)0x65,  /* [620] */
    (xdc_Char)0x6d,  /* [621] */
    (xdc_Char)0x6f,  /* [622] */
    (xdc_Char)0x72,  /* [623] */
    (xdc_Char)0x79,  /* [624] */
    (xdc_Char)0x0,  /* [625] */
    (xdc_Char)0x48,  /* [626] */
    (xdc_Char)0x65,  /* [627] */
    (xdc_Char)0x61,  /* [628] */
    (xdc_Char)0x70,  /* [629] */
    (xdc_Char)0x53,  /* [630] */
    (xdc_Char)0x74,  /* [631] */
    (xdc_Char)0x64,  /* [632] */
    (xdc_Char)0x0,  /* [633] */
    (xdc_Char)0x53,  /* [634] */
    (xdc_Char)0x74,  /* [635] */
    (xdc_Char)0x61,  /* [636] */
    (xdc_Char)0x72,  /* [637] */
    (xdc_Char)0x74,  /* [638] */
    (xdc_Char)0x75,  /* [639] */
    (xdc_Char)0x70,  /* [640] */
    (xdc_Char)0x0,  /* [641] */
    (xdc_Char)0x53,  /* [642] */
    (xdc_Char)0x79,  /* [643] */
    (xdc_Char)0x73,  /* [644] */
    (xdc_Char)0x74,  /* [645] */
    (xdc_Char)0x65,  /* [646] */
    (xdc_Char)0x6d,  /* [647] */
    (xdc_Char)0x0,  /* [648] */
    (xdc_Char)0x53,  /* [649] */
    (xdc_Char)0x79,  /* [650] */
    (xdc_Char)0x73,  /* [651] */
    (xdc_Char)0x4d,  /* [652] */
    (xdc_Char)0x69,  /* [653] */
    (xdc_Char)0x6e,  /* [654] */
    (xdc_Char)0x0,  /* [655] */
    (xdc_Char)0x54,  /* [656] */
    (xdc_Char)0x65,  /* [657] */
    (xdc_Char)0x78,  /* [658] */
    (xdc_Char)0x74,  /* [659] */
    (xdc_Char)0x0,  /* [660] */
    (xdc_Char)0x68,  /* [661] */
    (xdc_Char)0x65,  /* [662] */
    (xdc_Char)0x6c,  /* [663] */
    (xdc_Char)0x6c,  /* [664] */
    (xdc_Char)0x6f,  /* [665] */
    (xdc_Char)0x2e,  /* [666] */
    (xdc_Char)0x0,  /* [667] */
    (xdc_Char)0x6d,  /* [668] */
    (xdc_Char)0x6f,  /* [669] */
    (xdc_Char)0x64,  /* [670] */
    (xdc_Char)0x2e,  /* [671] */
    (xdc_Char)0x0,  /* [672] */
    (xdc_Char)0x54,  /* [673] */
    (xdc_Char)0x61,  /* [674] */
    (xdc_Char)0x6c,  /* [675] */
    (xdc_Char)0x6b,  /* [676] */
    (xdc_Char)0x65,  /* [677] */
    (xdc_Char)0x72,  /* [678] */
    (xdc_Char)0x0,  /* [679] */
};

/* --> xdc_runtime_Text_nodeTab__A */
const __T1_xdc_runtime_Text_nodeTab xdc_runtime_Text_nodeTab__A[19] = {
    {
        (xdc_Bits16)0x0,  /* left */
        (xdc_Bits16)0x0,  /* right */
    },  /* [0] */
    {
        (xdc_Bits16)0x225,  /* left */
        (xdc_Bits16)0x22a,  /* right */
    },  /* [1] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x233,  /* right */
    },  /* [2] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x23a,  /* right */
    },  /* [3] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x23f,  /* right */
    },  /* [4] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x248,  /* right */
    },  /* [5] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x24e,  /* right */
    },  /* [6] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x254,  /* right */
    },  /* [7] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x259,  /* right */
    },  /* [8] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x262,  /* right */
    },  /* [9] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x266,  /* right */
    },  /* [10] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x26b,  /* right */
    },  /* [11] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x272,  /* right */
    },  /* [12] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x27a,  /* right */
    },  /* [13] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x282,  /* right */
    },  /* [14] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x289,  /* right */
    },  /* [15] */
    {
        (xdc_Bits16)0x8001,  /* left */
        (xdc_Bits16)0x290,  /* right */
    },  /* [16] */
    {
        (xdc_Bits16)0x295,  /* left */
        (xdc_Bits16)0x29c,  /* right */
    },  /* [17] */
    {
        (xdc_Bits16)0x8011,  /* left */
        (xdc_Bits16)0x2a1,  /* right */
    },  /* [18] */
};

/* Module__diagsEnabled__C */
__FAR__ const CT__xdc_runtime_Text_Module__diagsEnabled xdc_runtime_Text_Module__diagsEnabled__C = (xdc_Bits32)0x10;

/* Module__diagsIncluded__C */
__FAR__ const CT__xdc_runtime_Text_Module__diagsIncluded xdc_runtime_Text_Module__diagsIncluded__C = (xdc_Bits32)0x10;

/* Module__diagsMask__C */
__FAR__ const CT__xdc_runtime_Text_Module__diagsMask xdc_runtime_Text_Module__diagsMask__C = ((CT__xdc_runtime_Text_Module__diagsMask)((void*)&xdc_runtime_Text_Module__root__V.hdr.diagsMask));

/* Module__gateObj__C */
__FAR__ const CT__xdc_runtime_Text_Module__gateObj xdc_runtime_Text_Module__gateObj__C = ((CT__xdc_runtime_Text_Module__gateObj)0);

/* Module__gatePrms__C */
__FAR__ const CT__xdc_runtime_Text_Module__gatePrms xdc_runtime_Text_Module__gatePrms__C = ((CT__xdc_runtime_Text_Module__gatePrms)0);

/* Module__id__C */
__FAR__ const CT__xdc_runtime_Text_Module__id xdc_runtime_Text_Module__id__C = (xdc_Bits16)0x8010;

/* Module__loggerDefined__C */
__FAR__ const CT__xdc_runtime_Text_Module__loggerDefined xdc_runtime_Text_Module__loggerDefined__C = 0;

/* Module__loggerObj__C */
__FAR__ const CT__xdc_runtime_Text_Module__loggerObj xdc_runtime_Text_Module__loggerObj__C = ((CT__xdc_runtime_Text_Module__loggerObj)0);

/* Module__loggerFxn4__C */
__FAR__ const CT__xdc_runtime_Text_Module__loggerFxn4 xdc_runtime_Text_Module__loggerFxn4__C = ((CT__xdc_runtime_Text_Module__loggerFxn4)0);

/* Module__loggerFxn8__C */
__FAR__ const CT__xdc_runtime_Text_Module__loggerFxn8 xdc_runtime_Text_Module__loggerFxn8__C = ((CT__xdc_runtime_Text_Module__loggerFxn8)0);

/* Module__startupDoneFxn__C */
__FAR__ const CT__xdc_runtime_Text_Module__startupDoneFxn xdc_runtime_Text_Module__startupDoneFxn__C = ((CT__xdc_runtime_Text_Module__startupDoneFxn)0);

/* Object__count__C */
__FAR__ const CT__xdc_runtime_Text_Object__count xdc_runtime_Text_Object__count__C = 0;

/* Object__heap__C */
__FAR__ const CT__xdc_runtime_Text_Object__heap xdc_runtime_Text_Object__heap__C = 0;

/* Object__sizeof__C */
__FAR__ const CT__xdc_runtime_Text_Object__sizeof xdc_runtime_Text_Object__sizeof__C = 0;

/* Object__table__C */
__FAR__ const CT__xdc_runtime_Text_Object__table xdc_runtime_Text_Object__table__C = 0;

/* nameUnknown__C */
__FAR__ const CT__xdc_runtime_Text_nameUnknown xdc_runtime_Text_nameUnknown__C = "{unknown-instance-name}";

/* nameEmpty__C */
__FAR__ const CT__xdc_runtime_Text_nameEmpty xdc_runtime_Text_nameEmpty__C = "{empty-instance-name}";

/* nameStatic__C */
__FAR__ const CT__xdc_runtime_Text_nameStatic xdc_runtime_Text_nameStatic__C = "{static-instance-name}";

/* isLoaded__C */
__FAR__ const CT__xdc_runtime_Text_isLoaded xdc_runtime_Text_isLoaded__C = 1;

/* charTab__C */
__FAR__ const CT__xdc_runtime_Text_charTab xdc_runtime_Text_charTab__C = ((CT__xdc_runtime_Text_charTab)xdc_runtime_Text_charTab__A);

/* nodeTab__C */
__FAR__ const CT__xdc_runtime_Text_nodeTab xdc_runtime_Text_nodeTab__C = ((CT__xdc_runtime_Text_nodeTab)xdc_runtime_Text_nodeTab__A);

/* charCnt__C */
__FAR__ const CT__xdc_runtime_Text_charCnt xdc_runtime_Text_charCnt__C = (xdc_Int16)0x2a8;

/* nodeCnt__C */
__FAR__ const CT__xdc_runtime_Text_nodeCnt xdc_runtime_Text_nodeCnt__C = (xdc_Int16)0x13;

/* visitRopeFxn__C */
__FAR__ const CT__xdc_runtime_Text_visitRopeFxn xdc_runtime_Text_visitRopeFxn__C = ((CT__xdc_runtime_Text_visitRopeFxn)((xdc_Fxn)xdc_runtime_Text_visitRope__I));

/* visitRopeFxn2__C */
__FAR__ const CT__xdc_runtime_Text_visitRopeFxn2 xdc_runtime_Text_visitRopeFxn2__C = ((CT__xdc_runtime_Text_visitRopeFxn2)((xdc_Fxn)xdc_runtime_Text_visitRope2__I));


/*
 * ======== hello.mod.Talker FUNCTION STUBS ========
 */

/* print__E */
xdc_Void hello_mod_Talker_print__E( void ) 
{
    hello_mod_Talker_print__F();
}


/*
 * ======== xdc.runtime.Diags FUNCTION STUBS ========
 */

/* setMask__E */
xdc_Void xdc_runtime_Diags_setMask__E( xdc_String control ) 
{
    xdc_runtime_Diags_setMask__F(control);
}


/*
 * ======== xdc.runtime.Error FUNCTION STUBS ========
 */

/* check__E */
xdc_Bool xdc_runtime_Error_check__E( xdc_runtime_Error_Block* eb ) 
{
    return xdc_runtime_Error_check__F(eb);
}

/* getData__E */
xdc_runtime_Error_Data* xdc_runtime_Error_getData__E( xdc_runtime_Error_Block* eb ) 
{
    return xdc_runtime_Error_getData__F(eb);
}

/* getCode__E */
xdc_UInt16 xdc_runtime_Error_getCode__E( xdc_runtime_Error_Block* eb ) 
{
    return xdc_runtime_Error_getCode__F(eb);
}

/* getId__E */
xdc_runtime_Error_Id xdc_runtime_Error_getId__E( xdc_runtime_Error_Block* eb ) 
{
    return xdc_runtime_Error_getId__F(eb);
}

/* getMsg__E */
xdc_String xdc_runtime_Error_getMsg__E( xdc_runtime_Error_Block* eb ) 
{
    return xdc_runtime_Error_getMsg__F(eb);
}

/* getSite__E */
xdc_runtime_Types_Site* xdc_runtime_Error_getSite__E( xdc_runtime_Error_Block* eb ) 
{
    return xdc_runtime_Error_getSite__F(eb);
}

/* init__E */
xdc_Void xdc_runtime_Error_init__E( xdc_runtime_Error_Block* eb ) 
{
    xdc_runtime_Error_init__F(eb);
}

/* print__E */
xdc_Void xdc_runtime_Error_print__E( xdc_runtime_Error_Block* eb ) 
{
    xdc_runtime_Error_print__F(eb);
}

/* raiseX__E */
xdc_Void xdc_runtime_Error_raiseX__E( xdc_runtime_Error_Block* eb, xdc_runtime_Types_ModuleId mod, xdc_String file, xdc_Int line, xdc_runtime_Error_Id id, xdc_IArg arg1, xdc_IArg arg2 ) 
{
    xdc_runtime_Error_raiseX__F(eb, mod, file, line, id, arg1, arg2);
}


/*
 * ======== xdc.runtime.Gate FUNCTION STUBS ========
 */

/* enterSystem__E */
xdc_IArg xdc_runtime_Gate_enterSystem__E( void ) 
{
    return xdc_runtime_Gate_enterSystem__F();
}

/* leaveSystem__E */
xdc_Void xdc_runtime_Gate_leaveSystem__E( xdc_IArg key ) 
{
    xdc_runtime_Gate_leaveSystem__F(key);
}


/*
 * ======== xdc.runtime.GateNull FUNCTION STUBS ========
 */

/* query__E */
xdc_Bool xdc_runtime_GateNull_query__E( xdc_Int qual ) 
{
    return xdc_runtime_GateNull_query__F(qual);
}

/* enter__E */
xdc_IArg xdc_runtime_GateNull_enter__E( xdc_runtime_GateNull_Handle __inst ) 
{
    return xdc_runtime_GateNull_enter__F((void*)__inst);
}

/* leave__E */
xdc_Void xdc_runtime_GateNull_leave__E( xdc_runtime_GateNull_Handle __inst, xdc_IArg key ) 
{
    xdc_runtime_GateNull_leave__F((void*)__inst, key);
}


/*
 * ======== xdc.runtime.HeapStd FUNCTION STUBS ========
 */

/* free__E */
xdc_Void xdc_runtime_HeapStd_free__E( xdc_runtime_HeapStd_Handle __inst, xdc_Ptr block, xdc_SizeT size ) 
{
    xdc_runtime_HeapStd_free__F((void*)__inst, block, size);
}

/* getStats__E */
xdc_Void xdc_runtime_HeapStd_getStats__E( xdc_runtime_HeapStd_Handle __inst, xdc_runtime_Memory_Stats* stats ) 
{
    xdc_runtime_HeapStd_getStats__F((void*)__inst, stats);
}

/* alloc__E */
xdc_Ptr xdc_runtime_HeapStd_alloc__E( xdc_runtime_HeapStd_Handle __inst, xdc_SizeT size, xdc_SizeT align, xdc_runtime_Error_Block* eb ) 
{
    return xdc_runtime_HeapStd_alloc__F((void*)__inst, size, align, eb);
}

/* isBlocking__E */
xdc_Bool xdc_runtime_HeapStd_isBlocking__E( xdc_runtime_HeapStd_Handle __inst ) 
{
    return xdc_runtime_HeapStd_isBlocking__F((void*)__inst);
}


/*
 * ======== xdc.runtime.Log FUNCTION STUBS ========
 */

/* doPrint__E */
xdc_Void xdc_runtime_Log_doPrint__E( xdc_runtime_Log_EventRec* evRec ) 
{
    xdc_runtime_Log_doPrint__F(evRec);
}


/*
 * ======== xdc.runtime.Memory FUNCTION STUBS ========
 */

/* alloc__E */
xdc_Ptr xdc_runtime_Memory_alloc__E( xdc_runtime_IHeap_Handle heap, xdc_SizeT size, xdc_SizeT align, xdc_runtime_Error_Block* eb ) 
{
    return xdc_runtime_Memory_alloc__F(heap, size, align, eb);
}

/* calloc__E */
xdc_Ptr xdc_runtime_Memory_calloc__E( xdc_runtime_IHeap_Handle heap, xdc_SizeT size, xdc_SizeT align, xdc_runtime_Error_Block* eb ) 
{
    return xdc_runtime_Memory_calloc__F(heap, size, align, eb);
}

/* free__E */
xdc_Void xdc_runtime_Memory_free__E( xdc_runtime_IHeap_Handle heap, xdc_Ptr block, xdc_SizeT size ) 
{
    xdc_runtime_Memory_free__F(heap, block, size);
}

/* getStats__E */
xdc_Void xdc_runtime_Memory_getStats__E( xdc_runtime_IHeap_Handle heap, xdc_runtime_Memory_Stats* stats ) 
{
    xdc_runtime_Memory_getStats__F(heap, stats);
}

/* query__E */
xdc_Bool xdc_runtime_Memory_query__E( xdc_runtime_IHeap_Handle heap, xdc_Int qual ) 
{
    return xdc_runtime_Memory_query__F(heap, qual);
}

/* getMaxDefaultTypeAlign__E */
xdc_SizeT xdc_runtime_Memory_getMaxDefaultTypeAlign__E( void ) 
{
    return xdc_runtime_Memory_getMaxDefaultTypeAlign__F();
}

/* valloc__E */
xdc_Ptr xdc_runtime_Memory_valloc__E( xdc_runtime_IHeap_Handle heap, xdc_SizeT size, xdc_SizeT align, xdc_Char value, xdc_runtime_Error_Block* eb ) 
{
    return xdc_runtime_Memory_valloc__F(heap, size, align, value, eb);
}


/*
 * ======== xdc.runtime.Startup FUNCTION STUBS ========
 */

/* exec__E */
xdc_Void xdc_runtime_Startup_exec__E( void ) 
{
    xdc_runtime_Startup_exec__F();
}

/* rtsDone__E */
xdc_Bool xdc_runtime_Startup_rtsDone__E( void ) 
{
    return xdc_runtime_Startup_rtsDone__F();
}


/*
 * ======== xdc.runtime.SysMin FUNCTION STUBS ========
 */

/* abort__E */
xdc_Void xdc_runtime_SysMin_abort__E( xdc_String str ) 
{
    xdc_runtime_SysMin_abort__F(str);
}

/* exit__E */
xdc_Void xdc_runtime_SysMin_exit__E( xdc_Int stat ) 
{
    xdc_runtime_SysMin_exit__F(stat);
}

/* flush__E */
xdc_Void xdc_runtime_SysMin_flush__E( void ) 
{
    xdc_runtime_SysMin_flush__F();
}

/* putch__E */
xdc_Void xdc_runtime_SysMin_putch__E( xdc_Char ch ) 
{
    xdc_runtime_SysMin_putch__F(ch);
}

/* ready__E */
xdc_Bool xdc_runtime_SysMin_ready__E( void ) 
{
    return xdc_runtime_SysMin_ready__F();
}

/* Module_startup */
xdc_Int xdc_runtime_SysMin_Module_startup__E( xdc_Int state )
{
    return xdc_runtime_SysMin_Module_startup__F(state);
}


/*
 * ======== xdc.runtime.System FUNCTION STUBS ========
 */

/* abort__E */
xdc_Void xdc_runtime_System_abort__E( xdc_String str ) 
{
    xdc_runtime_System_abort__F(str);
}

/* atexit__E */
xdc_Bool xdc_runtime_System_atexit__E( xdc_runtime_System_AtexitHandler handler ) 
{
    return xdc_runtime_System_atexit__F(handler);
}

/* exit__E */
xdc_Void xdc_runtime_System_exit__E( xdc_Int stat ) 
{
    xdc_runtime_System_exit__F(stat);
}

/* putch__E */
xdc_Void xdc_runtime_System_putch__E( xdc_Char ch ) 
{
    xdc_runtime_System_putch__F(ch);
}

/* flush__E */
xdc_Void xdc_runtime_System_flush__E( void ) 
{
    xdc_runtime_System_flush__F();
}

/* printf_va__E */
xdc_Int xdc_runtime_System_printf_va__E( xdc_String fmt, va_list __va ) 
{
    return xdc_runtime_System_printf_va__F(fmt, __va);
}

/* printf__E */
xdc_Int xdc_runtime_System_printf__E( xdc_String fmt, ... ) 
{
    xdc_Int __ret;

    va_list __va; va_start(__va, fmt);
    __ret = xdc_runtime_System_printf_va__F(fmt, __va);

    va_end(__va);
    return __ret;
}

/* aprintf_va__E */
xdc_Int xdc_runtime_System_aprintf_va__E( xdc_String fmt, va_list __va ) 
{
    return xdc_runtime_System_aprintf_va__F(fmt, __va);
}

/* aprintf__E */
xdc_Int xdc_runtime_System_aprintf__E( xdc_String fmt, ... ) 
{
    xdc_Int __ret;

    va_list __va; va_start(__va, fmt);
    __ret = xdc_runtime_System_aprintf_va__F(fmt, __va);

    va_end(__va);
    return __ret;
}

/* sprintf_va__E */
xdc_Int xdc_runtime_System_sprintf_va__E( xdc_Char buf[], xdc_String fmt, va_list __va ) 
{
    return xdc_runtime_System_sprintf_va__F(buf, fmt, __va);
}

/* sprintf__E */
xdc_Int xdc_runtime_System_sprintf__E( xdc_Char buf[], xdc_String fmt, ... ) 
{
    xdc_Int __ret;

    va_list __va; va_start(__va, fmt);
    __ret = xdc_runtime_System_sprintf_va__F(buf, fmt, __va);

    va_end(__va);
    return __ret;
}

/* asprintf_va__E */
xdc_Int xdc_runtime_System_asprintf_va__E( xdc_Char buf[], xdc_String fmt, va_list __va ) 
{
    return xdc_runtime_System_asprintf_va__F(buf, fmt, __va);
}

/* asprintf__E */
xdc_Int xdc_runtime_System_asprintf__E( xdc_Char buf[], xdc_String fmt, ... ) 
{
    xdc_Int __ret;

    va_list __va; va_start(__va, fmt);
    __ret = xdc_runtime_System_asprintf_va__F(buf, fmt, __va);

    va_end(__va);
    return __ret;
}

/* vprintf__E */
xdc_Int xdc_runtime_System_vprintf__E( xdc_String fmt, xdc_VaList va ) 
{
    return xdc_runtime_System_vprintf__F(fmt, va);
}

/* avprintf__E */
xdc_Int xdc_runtime_System_avprintf__E( xdc_String fmt, xdc_VaList va ) 
{
    return xdc_runtime_System_avprintf__F(fmt, va);
}

/* vsprintf__E */
xdc_Int xdc_runtime_System_vsprintf__E( xdc_Char buf[], xdc_String fmt, xdc_VaList va ) 
{
    return xdc_runtime_System_vsprintf__F(buf, fmt, va);
}

/* avsprintf__E */
xdc_Int xdc_runtime_System_avsprintf__E( xdc_Char buf[], xdc_String fmt, xdc_VaList va ) 
{
    return xdc_runtime_System_avsprintf__F(buf, fmt, va);
}

/* Module_startup */
xdc_Int xdc_runtime_System_Module_startup__E( xdc_Int state )
{
    return xdc_runtime_System_Module_startup__F(state);
}


/*
 * ======== xdc.runtime.Text FUNCTION STUBS ========
 */

/* cordText__E */
xdc_String xdc_runtime_Text_cordText__E( xdc_runtime_Text_CordAddr cord ) 
{
    return xdc_runtime_Text_cordText__F(cord);
}

/* ropeText__E */
xdc_String xdc_runtime_Text_ropeText__E( xdc_runtime_Text_RopeId rope ) 
{
    return xdc_runtime_Text_ropeText__F(rope);
}

/* matchRope__E */
xdc_Int xdc_runtime_Text_matchRope__E( xdc_runtime_Text_RopeId rope, xdc_String pat, xdc_Int* lenp ) 
{
    return xdc_runtime_Text_matchRope__F(rope, pat, lenp);
}

/* putLab__E */
xdc_Int xdc_runtime_Text_putLab__E( xdc_runtime_Types_Label* lab, xdc_Char** bufp, xdc_Int len ) 
{
    return xdc_runtime_Text_putLab__F(lab, bufp, len);
}

/* putMod__E */
xdc_Int xdc_runtime_Text_putMod__E( xdc_runtime_Types_ModuleId mid, xdc_Char** bufp, xdc_Int len ) 
{
    return xdc_runtime_Text_putMod__F(mid, bufp, len);
}

/* putSite__E */
xdc_Int xdc_runtime_Text_putSite__E( xdc_runtime_Types_Site* site, xdc_Char** bufp, xdc_Int len ) 
{
    return xdc_runtime_Text_putSite__F(site, bufp, len);
}


/*
 * ======== xdc.runtime.Main_Module_GateProxy PROXY BODY ========
 */

/* DELEGATES TO xdc.runtime.GateNull */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Main_Module_GateProxy_Module__startupDone__S( void ) 
{
    return xdc_runtime_GateNull_Module__startupDone__S();
}

/* Object__create__S */
xdc_Ptr xdc_runtime_Main_Module_GateProxy_Object__create__S (
    xdc_Ptr oa,
    xdc_SizeT osz,
    const xdc_Ptr aa,
    const xdc_runtime_Main_Module_GateProxy___ParamsPtr pa,
    xdc_SizeT psz,
    xdc_runtime_Error_Block* eb )
{
    return xdc_runtime_GateNull_Object__create__S(oa, osz, aa, (xdc_runtime_GateNull___ParamsPtr)pa, sizeof(xdc_runtime_IGateProvider_Params), eb);
}

/* Object__delete__S */
void xdc_runtime_Main_Module_GateProxy_Object__delete__S( Ptr instp ) 
{
    xdc_runtime_GateNull_Object__delete__S(instp);
}

/* Params__init__S */
void xdc_runtime_Main_Module_GateProxy_Params__init__S( xdc_Ptr dst, const xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz )
{
    xdc_runtime_GateNull_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label* xdc_runtime_Main_Module_GateProxy_Handle__label__S( Ptr obj, xdc_runtime_Types_Label* lab )
{
    return xdc_runtime_GateNull_Handle__label__S(obj, lab);
}

/* query__E */
xdc_Bool xdc_runtime_Main_Module_GateProxy_query__E( xdc_Int qual )
{
    return xdc_runtime_GateNull_query(qual);
}

/* enter__E */
xdc_IArg xdc_runtime_Main_Module_GateProxy_enter__E( xdc_runtime_Main_Module_GateProxy_Handle __inst )
{
    return xdc_runtime_GateNull_enter((xdc_runtime_GateNull_Handle)__inst);
}

/* leave__E */
xdc_Void xdc_runtime_Main_Module_GateProxy_leave__E( xdc_runtime_Main_Module_GateProxy_Handle __inst, xdc_IArg key )
{
    xdc_runtime_GateNull_leave((xdc_runtime_GateNull_Handle)__inst, key);
}


/*
 * ======== xdc.runtime.Memory_HeapProxy PROXY BODY ========
 */

/* DELEGATES TO xdc.runtime.HeapStd */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Memory_HeapProxy_Module__startupDone__S( void ) 
{
    return xdc_runtime_HeapStd_Module__startupDone__S();
}

/* Object__create__S */
xdc_Ptr xdc_runtime_Memory_HeapProxy_Object__create__S (
    xdc_Ptr oa,
    xdc_SizeT osz,
    const xdc_Ptr aa,
    const xdc_runtime_Memory_HeapProxy___ParamsPtr pa,
    xdc_SizeT psz,
    xdc_runtime_Error_Block* eb )
{
    return xdc_runtime_HeapStd_Object__create__S(oa, osz, aa, (xdc_runtime_HeapStd___ParamsPtr)pa, sizeof(xdc_runtime_IHeap_Params), eb);
}

/* Object__delete__S */
void xdc_runtime_Memory_HeapProxy_Object__delete__S( Ptr instp ) 
{
    xdc_runtime_HeapStd_Object__delete__S(instp);
}

/* Params__init__S */
void xdc_runtime_Memory_HeapProxy_Params__init__S( xdc_Ptr dst, const xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz )
{
    xdc_runtime_HeapStd_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label* xdc_runtime_Memory_HeapProxy_Handle__label__S( Ptr obj, xdc_runtime_Types_Label* lab )
{
    return xdc_runtime_HeapStd_Handle__label__S(obj, lab);
}

/* alloc__E */
xdc_Ptr xdc_runtime_Memory_HeapProxy_alloc__E( xdc_runtime_Memory_HeapProxy_Handle __inst, xdc_SizeT size, xdc_SizeT align, xdc_runtime_Error_Block* eb )
{
    return xdc_runtime_IHeap_alloc((xdc_runtime_IHeap_Handle)__inst, size, align, eb);
}

/* free__E */
xdc_Void xdc_runtime_Memory_HeapProxy_free__E( xdc_runtime_Memory_HeapProxy_Handle __inst, xdc_Ptr block, xdc_SizeT size )
{
    xdc_runtime_IHeap_free((xdc_runtime_IHeap_Handle)__inst, block, size);
}

/* isBlocking__E */
xdc_Bool xdc_runtime_Memory_HeapProxy_isBlocking__E( xdc_runtime_Memory_HeapProxy_Handle __inst )
{
    return xdc_runtime_IHeap_isBlocking((xdc_runtime_IHeap_Handle)__inst);
}

/* getStats__E */
xdc_Void xdc_runtime_Memory_HeapProxy_getStats__E( xdc_runtime_Memory_HeapProxy_Handle __inst, xdc_runtime_Memory_Stats* stats )
{
    xdc_runtime_IHeap_getStats((xdc_runtime_IHeap_Handle)__inst, stats);
}


/*
 * ======== xdc.runtime.System_Module_GateProxy PROXY BODY ========
 */

/* DELEGATES TO xdc.runtime.GateNull */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_System_Module_GateProxy_Module__startupDone__S( void ) 
{
    return xdc_runtime_GateNull_Module__startupDone__S();
}

/* Object__create__S */
xdc_Ptr xdc_runtime_System_Module_GateProxy_Object__create__S (
    xdc_Ptr oa,
    xdc_SizeT osz,
    const xdc_Ptr aa,
    const xdc_runtime_System_Module_GateProxy___ParamsPtr pa,
    xdc_SizeT psz,
    xdc_runtime_Error_Block* eb )
{
    return xdc_runtime_GateNull_Object__create__S(oa, osz, aa, (xdc_runtime_GateNull___ParamsPtr)pa, sizeof(xdc_runtime_IGateProvider_Params), eb);
}

/* Object__delete__S */
void xdc_runtime_System_Module_GateProxy_Object__delete__S( Ptr instp ) 
{
    xdc_runtime_GateNull_Object__delete__S(instp);
}

/* Params__init__S */
void xdc_runtime_System_Module_GateProxy_Params__init__S( xdc_Ptr dst, const xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz )
{
    xdc_runtime_GateNull_Params__init__S(dst, src, psz, isz);
}

/* Handle__label__S */
xdc_runtime_Types_Label* xdc_runtime_System_Module_GateProxy_Handle__label__S( Ptr obj, xdc_runtime_Types_Label* lab )
{
    return xdc_runtime_GateNull_Handle__label__S(obj, lab);
}

/* query__E */
xdc_Bool xdc_runtime_System_Module_GateProxy_query__E( xdc_Int qual )
{
    return xdc_runtime_GateNull_query(qual);
}

/* enter__E */
xdc_IArg xdc_runtime_System_Module_GateProxy_enter__E( xdc_runtime_System_Module_GateProxy_Handle __inst )
{
    return xdc_runtime_GateNull_enter((xdc_runtime_GateNull_Handle)__inst);
}

/* leave__E */
xdc_Void xdc_runtime_System_Module_GateProxy_leave__E( xdc_runtime_System_Module_GateProxy_Handle __inst, xdc_IArg key )
{
    xdc_runtime_GateNull_leave((xdc_runtime_GateNull_Handle)__inst, key);
}


/*
 * ======== xdc.runtime.System_SupportProxy PROXY BODY ========
 */

/* DELEGATES TO xdc.runtime.SysMin */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_System_SupportProxy_Module__startupDone__S( void ) 
{
    return xdc_runtime_SysMin_Module__startupDone__S();
}

/* abort__E */
xdc_Void xdc_runtime_System_SupportProxy_abort__E( xdc_String str )
{
    xdc_runtime_SysMin_abort(str);
}

/* exit__E */
xdc_Void xdc_runtime_System_SupportProxy_exit__E( xdc_Int stat )
{
    xdc_runtime_SysMin_exit(stat);
}

/* flush__E */
xdc_Void xdc_runtime_System_SupportProxy_flush__E( void )
{
    xdc_runtime_SysMin_flush();
}

/* putch__E */
xdc_Void xdc_runtime_System_SupportProxy_putch__E( xdc_Char ch )
{
    xdc_runtime_SysMin_putch(ch);
}

/* ready__E */
xdc_Bool xdc_runtime_System_SupportProxy_ready__E( void )
{
    return xdc_runtime_SysMin_ready();
}


/*
 * ======== xdc.runtime.GateNull OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { xdc_runtime_GateNull_Object2__ s0; char c; } xdc_runtime_GateNull___S1;
__FAR__ const xdc_runtime_Core_ObjDesc xdc_runtime_GateNull_Object__DESC__C = {
    (Ptr)&xdc_runtime_GateNull_Module__FXNS__C, /* fxnTab */
    &xdc_runtime_GateNull_Module__root__V.hdr.link, /* modLink */
    sizeof(xdc_runtime_GateNull___S1) - sizeof(xdc_runtime_GateNull_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(xdc_runtime_GateNull_Object2__), /* objSize */
    (Ptr)&xdc_runtime_GateNull_Object__PARAMS__C, /* prmsInit */
    sizeof(xdc_runtime_GateNull_Params), /* prmsSize */
};



/*
 * ======== xdc.runtime.HeapStd OBJECT DESCRIPTOR ========
 */

/* Object__DESC__C */
typedef struct { xdc_runtime_HeapStd_Object2__ s0; char c; } xdc_runtime_HeapStd___S1;
__FAR__ const xdc_runtime_Core_ObjDesc xdc_runtime_HeapStd_Object__DESC__C = {
    (Ptr)&xdc_runtime_HeapStd_Module__FXNS__C, /* fxnTab */
    &xdc_runtime_HeapStd_Module__root__V.hdr.link, /* modLink */
    sizeof(xdc_runtime_HeapStd___S1) - sizeof(xdc_runtime_HeapStd_Object2__), /* objAlign */
    0, /* objHeap */
    0, /* objName */
    sizeof(xdc_runtime_HeapStd_Object2__), /* objSize */
    (Ptr)&xdc_runtime_HeapStd_Object__PARAMS__C, /* prmsInit */
    sizeof(xdc_runtime_HeapStd_Params), /* prmsSize */
};



/*
 * ======== hello.mod.Talker SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool hello_mod_Talker_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.Assert SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Assert_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.Core SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Core_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.Defaults SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Defaults_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.Diags SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Diags_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.Error SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Error_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.Gate SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Gate_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.GateNull SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_GateNull_Module__startupDone__S( void ) 
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label* xdc_runtime_GateNull_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) 
{
    lab->handle = obj;
    lab->modId = 32776;
    xdc_runtime_Core_assignLabel(lab, 0, 0);

    return lab;
}

/* Params__init__S */
xdc_Void xdc_runtime_GateNull_Params__init__S( xdc_Ptr prms, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_Ptr)(src ? src : &xdc_runtime_GateNull_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr xdc_runtime_GateNull_Object__get__S( xdc_Ptr oa, xdc_Int i ) 
{
    if (oa) {
        return ((xdc_runtime_GateNull_Object__*)oa) + i;
    }

    if (xdc_runtime_GateNull_Object__count__C == 0) {
        return NULL;
    }

    return ((xdc_runtime_GateNull_Object__*)xdc_runtime_GateNull_Object__table__C) + i;
}

/* Object__first__S */
xdc_Ptr xdc_runtime_GateNull_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)xdc_runtime_GateNull_Module__root__V.hdr.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&xdc_runtime_GateNull_Module__root__V) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr xdc_runtime_GateNull_Object__next__S( xdc_Ptr obj ) 
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&xdc_runtime_GateNull_Module__root__V) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr xdc_runtime_GateNull_Object__create__S (
    xdc_Ptr oa,
    xdc_SizeT osz,
    const xdc_Ptr aa,
    const xdc_runtime_GateNull___ParamsPtr pa,
    xdc_SizeT psz,
    xdc_runtime_Error_Block* eb )
{
    xdc_runtime_GateNull_Params prms;
    xdc_runtime_GateNull_Object* obj;

    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&xdc_runtime_GateNull_Object__DESC__C, oa, osz, &prms, (xdc_Ptr)pa, psz, eb);
    if (obj == NULL) {
        return NULL;
    }

    return obj;
}

/* Object__destruct__S */
xdc_Void xdc_runtime_GateNull_Object__destruct__S( xdc_Ptr obj ) 
{
    xdc_runtime_Core_deleteObject__I(&xdc_runtime_GateNull_Object__DESC__C, obj, NULL, -1, TRUE);
}

/* Object__delete__S */
xdc_Void xdc_runtime_GateNull_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&xdc_runtime_GateNull_Object__DESC__C, *((xdc_runtime_GateNull_Object**)instp), NULL, -1, FALSE);
    *((xdc_runtime_GateNull_Handle*)instp) = NULL;
}


/*
 * ======== xdc.runtime.HeapStd SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_HeapStd_Module__startupDone__S( void ) 
{
    return 1;
}

/* Handle__label__S */
xdc_runtime_Types_Label* xdc_runtime_HeapStd_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) 
{
    lab->handle = obj;
    lab->modId = 32780;
    xdc_runtime_Core_assignLabel(lab, 0, 0);

    return lab;
}

/* Params__init__S */
xdc_Void xdc_runtime_HeapStd_Params__init__S( xdc_Ptr prms, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) 
{
    xdc_runtime_Core_assignParams__I(prms, (xdc_Ptr)(src ? src : &xdc_runtime_HeapStd_Object__PARAMS__C), psz, isz);
}

/* Object__get__S */
xdc_Ptr xdc_runtime_HeapStd_Object__get__S( xdc_Ptr oa, xdc_Int i ) 
{
    if (oa) {
        return ((xdc_runtime_HeapStd_Object__*)oa) + i;
    }

    if (xdc_runtime_HeapStd_Object__count__C == 0) {
        return NULL;
    }

    return ((xdc_runtime_HeapStd_Object__*)xdc_runtime_HeapStd_Object__table__C) + i;
}

/* Object__first__S */
xdc_Ptr xdc_runtime_HeapStd_Object__first__S( void ) 
{
    xdc_runtime_Types_InstHdr *iHdr = (xdc_runtime_Types_InstHdr *)xdc_runtime_HeapStd_Module__root__V.hdr.link.next;

    if (iHdr != (xdc_runtime_Types_InstHdr *)&xdc_runtime_HeapStd_Module__root__V) {
        return iHdr + 1;
    }
    else {
        return NULL;
    }
}

/* Object__next__S */
xdc_Ptr xdc_runtime_HeapStd_Object__next__S( xdc_Ptr obj ) 
{
    xdc_runtime_Types_InstHdr *iHdr = ((xdc_runtime_Types_InstHdr *)obj) - 1;

    if (iHdr->link.next != (xdc_runtime_Types_Link *)&xdc_runtime_HeapStd_Module__root__V) {
        return (xdc_runtime_Types_InstHdr *)(iHdr->link.next) + 1;
    }
    else {
        return NULL;
    }
}

/* Object__create__S */
xdc_Ptr xdc_runtime_HeapStd_Object__create__S (
    xdc_Ptr oa,
    xdc_SizeT osz,
    const xdc_Ptr aa,
    const xdc_runtime_HeapStd___ParamsPtr pa,
    xdc_SizeT psz,
    xdc_runtime_Error_Block* eb )
{
    xdc_runtime_HeapStd_Params prms;
    xdc_runtime_HeapStd_Object* obj;
    int iStat;

    /* common instance initialization */
    obj = xdc_runtime_Core_createObject__I(&xdc_runtime_HeapStd_Object__DESC__C, oa, osz, &prms, (xdc_Ptr)pa, psz, eb);
    if (obj == NULL) {
        return NULL;
    }

    /* module-specific initialization */
    iStat = xdc_runtime_HeapStd_Instance_init__F(obj, &prms, eb);
    if (xdc_runtime_Error_check(eb)) {
        xdc_runtime_Core_deleteObject__I(&xdc_runtime_HeapStd_Object__DESC__C, obj, NULL, iStat, (xdc_Bool)(oa != NULL));
        return NULL;
    }

    return obj;
}

/* Object__destruct__S */
xdc_Void xdc_runtime_HeapStd_Object__destruct__S( xdc_Ptr obj ) 
{
    xdc_runtime_Core_deleteObject__I(&xdc_runtime_HeapStd_Object__DESC__C, obj, NULL, 0, TRUE);
}

/* Object__delete__S */
xdc_Void xdc_runtime_HeapStd_Object__delete__S( xdc_Ptr instp ) 
{
    xdc_runtime_Core_deleteObject__I(&xdc_runtime_HeapStd_Object__DESC__C, *((xdc_runtime_HeapStd_Object**)instp), NULL, 0, FALSE);
    *((xdc_runtime_HeapStd_Handle*)instp) = NULL;
}


/*
 * ======== xdc.runtime.Log SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Log_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.Main SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Main_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.Main_Module_GateProxy SYSTEM FUNCTIONS ========
 */

xdc_Bool xdc_runtime_Main_Module_GateProxy_Proxy__abstract__S( void )
{
    return 0;
}
xdc_Ptr xdc_runtime_Main_Module_GateProxy_Proxy__delegate__S( void )
{
    return (xdc_Ptr)&xdc_runtime_GateNull_Module__FXNS__C;
}


/*
 * ======== xdc.runtime.Memory SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Memory_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.Memory_HeapProxy SYSTEM FUNCTIONS ========
 */

xdc_Bool xdc_runtime_Memory_HeapProxy_Proxy__abstract__S( void )
{
    return 1;
}
xdc_Ptr xdc_runtime_Memory_HeapProxy_Proxy__delegate__S( void )
{
    return (xdc_Ptr)&xdc_runtime_HeapStd_Module__FXNS__C;
}


/*
 * ======== xdc.runtime.Startup SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Startup_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== xdc.runtime.SysMin SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_SysMin_Module__startupDone__S( void ) 
{
    return xdc_runtime_SysMin_Module__startupDone__F();
}



/*
 * ======== xdc.runtime.System SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_System_Module__startupDone__S( void ) 
{
    return xdc_runtime_System_Module__startupDone__F();
}



/*
 * ======== xdc.runtime.System_Module_GateProxy SYSTEM FUNCTIONS ========
 */

xdc_Bool xdc_runtime_System_Module_GateProxy_Proxy__abstract__S( void )
{
    return 0;
}
xdc_Ptr xdc_runtime_System_Module_GateProxy_Proxy__delegate__S( void )
{
    return (xdc_Ptr)&xdc_runtime_GateNull_Module__FXNS__C;
}


/*
 * ======== xdc.runtime.System_SupportProxy SYSTEM FUNCTIONS ========
 */

xdc_Bool xdc_runtime_System_SupportProxy_Proxy__abstract__S( void )
{
    return 0;
}
xdc_Ptr xdc_runtime_System_SupportProxy_Proxy__delegate__S( void )
{
    return (xdc_Ptr)&xdc_runtime_SysMin_Module__FXNS__C;
}


/*
 * ======== xdc.runtime.Text SYSTEM FUNCTIONS ========
 */

/* Module__startupDone__S */
xdc_Bool xdc_runtime_Text_Module__startupDone__S( void ) 
{
    return 1;
}



/*
 * ======== hello.mod.Talker PRAGMAS ========
 */


#ifdef __ti__
    #pragma FUNC_EXT_CALLED(hello_mod_Talker_Module__startupDone__S);
    #pragma FUNC_EXT_CALLED(hello_mod_Talker_print__E);
#endif

#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__hello_mod_Talker_Module__diagsEnabled hello_mod_Talker_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Module__diagsIncluded hello_mod_Talker_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Module__diagsMask hello_mod_Talker_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Module__gateObj hello_mod_Talker_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Module__gatePrms hello_mod_Talker_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Module__id hello_mod_Talker_Module__id__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Module__loggerDefined hello_mod_Talker_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Module__loggerObj hello_mod_Talker_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Module__loggerFxn4 hello_mod_Talker_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Module__loggerFxn8 hello_mod_Talker_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Module__startupDoneFxn hello_mod_Talker_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Object__count hello_mod_Talker_Object__count__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Object__heap hello_mod_Talker_Object__heap__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Object__sizeof hello_mod_Talker_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_Object__table hello_mod_Talker_Object__table__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_text hello_mod_Talker_text__C __attribute__ ((externally_visible));
    const CT__hello_mod_Talker_count hello_mod_Talker_count__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* hello_mod_Talker_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool hello_mod_Talker_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr hello_mod_Talker_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void hello_mod_Talker_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void hello_mod_Talker_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr hello_mod_Talker_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr hello_mod_Talker_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr hello_mod_Talker_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void hello_mod_Talker_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool hello_mod_Talker_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr hello_mod_Talker_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_Void hello_mod_Talker_print__E( void ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Assert PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Assert_Module__diagsEnabled xdc_runtime_Assert_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Module__diagsIncluded xdc_runtime_Assert_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Module__diagsMask xdc_runtime_Assert_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Module__gateObj xdc_runtime_Assert_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Module__gatePrms xdc_runtime_Assert_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Module__id xdc_runtime_Assert_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Module__loggerDefined xdc_runtime_Assert_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Module__loggerObj xdc_runtime_Assert_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Module__loggerFxn4 xdc_runtime_Assert_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Module__loggerFxn8 xdc_runtime_Assert_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Module__startupDoneFxn xdc_runtime_Assert_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Object__count xdc_runtime_Assert_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Object__heap xdc_runtime_Assert_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Object__sizeof xdc_runtime_Assert_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_Object__table xdc_runtime_Assert_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Assert_E_assertFailed xdc_runtime_Assert_E_assertFailed__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Assert_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Assert_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Assert_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Assert_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Assert_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Assert_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Assert_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Assert_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Assert_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Assert_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Assert_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Core PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Core_Module__diagsEnabled xdc_runtime_Core_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Module__diagsIncluded xdc_runtime_Core_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Module__diagsMask xdc_runtime_Core_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Module__gateObj xdc_runtime_Core_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Module__gatePrms xdc_runtime_Core_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Module__id xdc_runtime_Core_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Module__loggerDefined xdc_runtime_Core_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Module__loggerObj xdc_runtime_Core_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Module__loggerFxn4 xdc_runtime_Core_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Module__loggerFxn8 xdc_runtime_Core_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Module__startupDoneFxn xdc_runtime_Core_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Object__count xdc_runtime_Core_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Object__heap xdc_runtime_Core_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Object__sizeof xdc_runtime_Core_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_Object__table xdc_runtime_Core_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Core_A_initializedParams xdc_runtime_Core_A_initializedParams__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Core_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Core_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Core_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Core_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Core_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Core_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Core_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Core_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Core_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Core_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Core_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Defaults PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Defaults_Module__diagsEnabled xdc_runtime_Defaults_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Module__diagsIncluded xdc_runtime_Defaults_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Module__diagsMask xdc_runtime_Defaults_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Module__gateObj xdc_runtime_Defaults_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Module__gatePrms xdc_runtime_Defaults_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Module__id xdc_runtime_Defaults_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Module__loggerDefined xdc_runtime_Defaults_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Module__loggerObj xdc_runtime_Defaults_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Module__loggerFxn4 xdc_runtime_Defaults_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Module__loggerFxn8 xdc_runtime_Defaults_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Module__startupDoneFxn xdc_runtime_Defaults_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Object__count xdc_runtime_Defaults_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Object__heap xdc_runtime_Defaults_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Object__sizeof xdc_runtime_Defaults_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Defaults_Object__table xdc_runtime_Defaults_Object__table__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Defaults_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Defaults_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Defaults_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Defaults_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Defaults_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Defaults_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Defaults_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Defaults_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Defaults_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Defaults_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Defaults_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Diags PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Diags_Module__diagsEnabled xdc_runtime_Diags_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Module__diagsIncluded xdc_runtime_Diags_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Module__diagsMask xdc_runtime_Diags_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Module__gateObj xdc_runtime_Diags_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Module__gatePrms xdc_runtime_Diags_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Module__id xdc_runtime_Diags_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Module__loggerDefined xdc_runtime_Diags_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Module__loggerObj xdc_runtime_Diags_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Module__loggerFxn4 xdc_runtime_Diags_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Module__loggerFxn8 xdc_runtime_Diags_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Module__startupDoneFxn xdc_runtime_Diags_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Object__count xdc_runtime_Diags_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Object__heap xdc_runtime_Diags_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Object__sizeof xdc_runtime_Diags_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_Object__table xdc_runtime_Diags_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_setMaskEnabled xdc_runtime_Diags_setMaskEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Diags_dictBase xdc_runtime_Diags_dictBase__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Diags_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Diags_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Diags_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Diags_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Diags_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Diags_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Diags_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Diags_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Diags_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Diags_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Diags_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Diags_setMask__E( xdc_String control ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Error PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Error_Module__diagsEnabled xdc_runtime_Error_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Module__diagsIncluded xdc_runtime_Error_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Module__diagsMask xdc_runtime_Error_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Module__gateObj xdc_runtime_Error_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Module__gatePrms xdc_runtime_Error_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Module__id xdc_runtime_Error_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Module__loggerDefined xdc_runtime_Error_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Module__loggerObj xdc_runtime_Error_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Module__loggerFxn4 xdc_runtime_Error_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Module__loggerFxn8 xdc_runtime_Error_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Module__startupDoneFxn xdc_runtime_Error_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Object__count xdc_runtime_Error_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Object__heap xdc_runtime_Error_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Object__sizeof xdc_runtime_Error_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_Object__table xdc_runtime_Error_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_E_generic xdc_runtime_Error_E_generic__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_E_memory xdc_runtime_Error_E_memory__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_policy xdc_runtime_Error_policy__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_raiseHook xdc_runtime_Error_raiseHook__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Error_maxDepth xdc_runtime_Error_maxDepth__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Error_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Error_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Error_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Error_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Error_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Error_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Error_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Error_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Error_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Error_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Error_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Error_check__E( xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_runtime_Error_Data* xdc_runtime_Error_getData__E( xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_UInt16 xdc_runtime_Error_getCode__E( xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_runtime_Error_Id xdc_runtime_Error_getId__E( xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_String xdc_runtime_Error_getMsg__E( xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_runtime_Types_Site* xdc_runtime_Error_getSite__E( xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Error_init__E( xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Error_print__E( xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Error_raiseX__E( xdc_runtime_Error_Block* eb, xdc_runtime_Types_ModuleId mod, xdc_String file, xdc_Int line, xdc_runtime_Error_Id id, xdc_IArg arg1, xdc_IArg arg2 ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Gate PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Gate_Module__diagsEnabled xdc_runtime_Gate_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Module__diagsIncluded xdc_runtime_Gate_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Module__diagsMask xdc_runtime_Gate_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Module__gateObj xdc_runtime_Gate_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Module__gatePrms xdc_runtime_Gate_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Module__id xdc_runtime_Gate_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Module__loggerDefined xdc_runtime_Gate_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Module__loggerObj xdc_runtime_Gate_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Module__loggerFxn4 xdc_runtime_Gate_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Module__loggerFxn8 xdc_runtime_Gate_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Module__startupDoneFxn xdc_runtime_Gate_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Object__count xdc_runtime_Gate_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Object__heap xdc_runtime_Gate_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Object__sizeof xdc_runtime_Gate_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Gate_Object__table xdc_runtime_Gate_Object__table__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Gate_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Gate_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Gate_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Gate_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Gate_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Gate_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Gate_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Gate_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Gate_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Gate_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Gate_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_IArg xdc_runtime_Gate_enterSystem__E( void ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Gate_leaveSystem__E( xdc_IArg key ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.GateNull PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_GateNull_Module__diagsEnabled xdc_runtime_GateNull_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Module__diagsIncluded xdc_runtime_GateNull_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Module__diagsMask xdc_runtime_GateNull_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Module__gateObj xdc_runtime_GateNull_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Module__gatePrms xdc_runtime_GateNull_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Module__id xdc_runtime_GateNull_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Module__loggerDefined xdc_runtime_GateNull_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Module__loggerObj xdc_runtime_GateNull_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Module__loggerFxn4 xdc_runtime_GateNull_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Module__loggerFxn8 xdc_runtime_GateNull_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Module__startupDoneFxn xdc_runtime_GateNull_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Object__count xdc_runtime_GateNull_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Object__heap xdc_runtime_GateNull_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Object__sizeof xdc_runtime_GateNull_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_GateNull_Object__table xdc_runtime_GateNull_Object__table__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_GateNull_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_GateNull_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_GateNull_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_GateNull_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_GateNull_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_GateNull_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_GateNull_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_GateNull_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_GateNull_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_GateNull_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_GateNull_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_GateNull_query__E( xdc_Int qual ) __attribute__ ((externally_visible));
    xdc_IArg xdc_runtime_GateNull_enter__E( xdc_runtime_GateNull_Handle ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_GateNull_leave__E( xdc_runtime_GateNull_Handle, xdc_IArg key ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.HeapStd PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_HeapStd_Module__diagsEnabled xdc_runtime_HeapStd_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Module__diagsIncluded xdc_runtime_HeapStd_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Module__diagsMask xdc_runtime_HeapStd_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Module__gateObj xdc_runtime_HeapStd_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Module__gatePrms xdc_runtime_HeapStd_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Module__id xdc_runtime_HeapStd_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Module__loggerDefined xdc_runtime_HeapStd_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Module__loggerObj xdc_runtime_HeapStd_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Module__loggerFxn4 xdc_runtime_HeapStd_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Module__loggerFxn8 xdc_runtime_HeapStd_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Module__startupDoneFxn xdc_runtime_HeapStd_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Object__count xdc_runtime_HeapStd_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Object__heap xdc_runtime_HeapStd_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Object__sizeof xdc_runtime_HeapStd_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_Object__table xdc_runtime_HeapStd_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_E_noRTSMemory xdc_runtime_HeapStd_E_noRTSMemory__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_A_zeroSize xdc_runtime_HeapStd_A_zeroSize__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_A_invalidTotalFreeSize xdc_runtime_HeapStd_A_invalidTotalFreeSize__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_HeapStd_A_invalidAlignment xdc_runtime_HeapStd_A_invalidAlignment__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_HeapStd_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_HeapStd_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_HeapStd_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_HeapStd_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_HeapStd_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_HeapStd_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_HeapStd_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_HeapStd_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_HeapStd_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_HeapStd_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_HeapStd_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_HeapStd_alloc__E( xdc_runtime_HeapStd_Handle, xdc_SizeT size, xdc_SizeT align, xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_HeapStd_free__E( xdc_runtime_HeapStd_Handle, xdc_Ptr block, xdc_SizeT size ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_HeapStd_isBlocking__E( xdc_runtime_HeapStd_Handle ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_HeapStd_getStats__E( xdc_runtime_HeapStd_Handle, xdc_runtime_Memory_Stats* stats ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_HeapStd_alloc__E( xdc_runtime_HeapStd_Handle, xdc_SizeT size, xdc_SizeT align, xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_HeapStd_isBlocking__E( xdc_runtime_HeapStd_Handle ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Log PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Log_Module__diagsEnabled xdc_runtime_Log_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Module__diagsIncluded xdc_runtime_Log_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Module__diagsMask xdc_runtime_Log_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Module__gateObj xdc_runtime_Log_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Module__gatePrms xdc_runtime_Log_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Module__id xdc_runtime_Log_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Module__loggerDefined xdc_runtime_Log_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Module__loggerObj xdc_runtime_Log_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Module__loggerFxn4 xdc_runtime_Log_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Module__loggerFxn8 xdc_runtime_Log_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Module__startupDoneFxn xdc_runtime_Log_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Object__count xdc_runtime_Log_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Object__heap xdc_runtime_Log_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Object__sizeof xdc_runtime_Log_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_Object__table xdc_runtime_Log_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_L_construct xdc_runtime_Log_L_construct__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_L_create xdc_runtime_Log_L_create__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_L_destruct xdc_runtime_Log_L_destruct__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Log_L_delete xdc_runtime_Log_L_delete__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Log_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Log_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Log_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Log_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Log_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Log_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Log_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Log_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Log_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Log_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Log_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Log_doPrint__E( xdc_runtime_Log_EventRec* evRec ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Main PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Main_Module__diagsEnabled xdc_runtime_Main_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Module__diagsIncluded xdc_runtime_Main_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Module__diagsMask xdc_runtime_Main_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Module__gateObj xdc_runtime_Main_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Module__gatePrms xdc_runtime_Main_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Module__id xdc_runtime_Main_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Module__loggerDefined xdc_runtime_Main_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Module__loggerObj xdc_runtime_Main_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Module__loggerFxn4 xdc_runtime_Main_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Module__loggerFxn8 xdc_runtime_Main_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Module__startupDoneFxn xdc_runtime_Main_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Object__count xdc_runtime_Main_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Object__heap xdc_runtime_Main_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Object__sizeof xdc_runtime_Main_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Main_Object__table xdc_runtime_Main_Object__table__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Main_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Main_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Main_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Main_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Main_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Main_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Main_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Main_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Main_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Main_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Main_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Memory PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Memory_Module__diagsEnabled xdc_runtime_Memory_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Module__diagsIncluded xdc_runtime_Memory_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Module__diagsMask xdc_runtime_Memory_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Module__gateObj xdc_runtime_Memory_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Module__gatePrms xdc_runtime_Memory_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Module__id xdc_runtime_Memory_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Module__loggerDefined xdc_runtime_Memory_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Module__loggerObj xdc_runtime_Memory_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Module__loggerFxn4 xdc_runtime_Memory_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Module__loggerFxn8 xdc_runtime_Memory_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Module__startupDoneFxn xdc_runtime_Memory_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Object__count xdc_runtime_Memory_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Object__heap xdc_runtime_Memory_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Object__sizeof xdc_runtime_Memory_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_Object__table xdc_runtime_Memory_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Memory_defaultHeapInstance xdc_runtime_Memory_defaultHeapInstance__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Memory_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Memory_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Memory_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Memory_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Memory_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Memory_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Memory_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Memory_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Memory_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Memory_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Memory_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Memory_alloc__E( xdc_runtime_IHeap_Handle heap, xdc_SizeT size, xdc_SizeT align, xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Memory_calloc__E( xdc_runtime_IHeap_Handle heap, xdc_SizeT size, xdc_SizeT align, xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Memory_free__E( xdc_runtime_IHeap_Handle heap, xdc_Ptr block, xdc_SizeT size ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Memory_getStats__E( xdc_runtime_IHeap_Handle heap, xdc_runtime_Memory_Stats* stats ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Memory_query__E( xdc_runtime_IHeap_Handle heap, xdc_Int qual ) __attribute__ ((externally_visible));
    xdc_SizeT xdc_runtime_Memory_getMaxDefaultTypeAlign__E( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Memory_valloc__E( xdc_runtime_IHeap_Handle heap, xdc_SizeT size, xdc_SizeT align, xdc_Char value, xdc_runtime_Error_Block* eb ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Startup PRAGMAS ========
 */


#ifdef __ti__
    #pragma FUNC_EXT_CALLED(xdc_runtime_Startup_Module__startupDone__S);
    #pragma FUNC_EXT_CALLED(xdc_runtime_Startup_exec__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_Startup_rtsDone__E);
#endif

#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Startup_Module__diagsEnabled xdc_runtime_Startup_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Module__diagsIncluded xdc_runtime_Startup_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Module__diagsMask xdc_runtime_Startup_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Module__gateObj xdc_runtime_Startup_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Module__gatePrms xdc_runtime_Startup_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Module__id xdc_runtime_Startup_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Module__loggerDefined xdc_runtime_Startup_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Module__loggerObj xdc_runtime_Startup_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Module__loggerFxn4 xdc_runtime_Startup_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Module__loggerFxn8 xdc_runtime_Startup_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Module__startupDoneFxn xdc_runtime_Startup_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Object__count xdc_runtime_Startup_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Object__heap xdc_runtime_Startup_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Object__sizeof xdc_runtime_Startup_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_Object__table xdc_runtime_Startup_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_maxPasses xdc_runtime_Startup_maxPasses__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_firstFxns xdc_runtime_Startup_firstFxns__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_lastFxns xdc_runtime_Startup_lastFxns__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_startModsFxn xdc_runtime_Startup_startModsFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_execImpl xdc_runtime_Startup_execImpl__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_sfxnTab xdc_runtime_Startup_sfxnTab__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Startup_sfxnRts xdc_runtime_Startup_sfxnRts__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Startup_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Startup_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Startup_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Startup_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Startup_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Startup_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Startup_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Startup_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Startup_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Startup_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Startup_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Startup_exec__E( void ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Startup_rtsDone__E( void ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.SysMin PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_SysMin_Module__diagsEnabled xdc_runtime_SysMin_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Module__diagsIncluded xdc_runtime_SysMin_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Module__diagsMask xdc_runtime_SysMin_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Module__gateObj xdc_runtime_SysMin_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Module__gatePrms xdc_runtime_SysMin_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Module__id xdc_runtime_SysMin_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Module__loggerDefined xdc_runtime_SysMin_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Module__loggerObj xdc_runtime_SysMin_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Module__loggerFxn4 xdc_runtime_SysMin_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Module__loggerFxn8 xdc_runtime_SysMin_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Module__startupDoneFxn xdc_runtime_SysMin_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Object__count xdc_runtime_SysMin_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Object__heap xdc_runtime_SysMin_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Object__sizeof xdc_runtime_SysMin_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_Object__table xdc_runtime_SysMin_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_bufSize xdc_runtime_SysMin_bufSize__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_flushAtExit xdc_runtime_SysMin_flushAtExit__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_outputFxn xdc_runtime_SysMin_outputFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_SysMin_outputFunc xdc_runtime_SysMin_outputFunc__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_SysMin_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_SysMin_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_SysMin_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_SysMin_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_SysMin_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_SysMin_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_SysMin_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_SysMin_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_abort__E( xdc_String str ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_exit__E( xdc_Int stat ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_flush__E( void ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_putch__E( xdc_Char ch ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_SysMin_ready__E( void ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_abort__E( xdc_String str ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_exit__E( xdc_Int stat ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_flush__E( void ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_SysMin_putch__E( xdc_Char ch ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_SysMin_ready__E( void ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.System PRAGMAS ========
 */


#ifdef __ti__
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_Module__startupDone__S);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_Module_startup__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_abort__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_atexit__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_exit__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_putch__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_flush__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_printf__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_aprintf__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_sprintf__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_asprintf__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_vprintf__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_avprintf__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_vsprintf__E);
    #pragma FUNC_EXT_CALLED(xdc_runtime_System_avsprintf__E);
#endif

#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_System_Module__diagsEnabled xdc_runtime_System_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Module__diagsIncluded xdc_runtime_System_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Module__diagsMask xdc_runtime_System_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Module__gateObj xdc_runtime_System_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Module__gatePrms xdc_runtime_System_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Module__id xdc_runtime_System_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Module__loggerDefined xdc_runtime_System_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Module__loggerObj xdc_runtime_System_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Module__loggerFxn4 xdc_runtime_System_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Module__loggerFxn8 xdc_runtime_System_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Module__startupDoneFxn xdc_runtime_System_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Object__count xdc_runtime_System_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Object__heap xdc_runtime_System_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Object__sizeof xdc_runtime_System_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_Object__table xdc_runtime_System_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_A_cannotFitIntoArg xdc_runtime_System_A_cannotFitIntoArg__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_maxAtexitHandlers xdc_runtime_System_maxAtexitHandlers__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_System_extendFxn xdc_runtime_System_extendFxn__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_System_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_System_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_System_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_System_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_System_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_System_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_System_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_System_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_System_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_System_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_System_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_System_abort__E( xdc_String str ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_System_atexit__E( xdc_runtime_System_AtexitHandler handler ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_System_exit__E( xdc_Int stat ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_System_putch__E( xdc_Char ch ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_System_flush__E( void ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_System_printf__E( xdc_String fmt, ... ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_System_aprintf__E( xdc_String fmt, ... ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_System_sprintf__E( xdc_Char buf[], xdc_String fmt, ... ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_System_asprintf__E( xdc_Char buf[], xdc_String fmt, ... ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_System_vprintf__E( xdc_String fmt, xdc_VaList va ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_System_avprintf__E( xdc_String fmt, xdc_VaList va ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_System_vsprintf__E( xdc_Char buf[], xdc_String fmt, xdc_VaList va ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_System_avsprintf__E( xdc_Char buf[], xdc_String fmt, xdc_VaList va ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== xdc.runtime.Text PRAGMAS ========
 */


#ifdef __GNUC__
#if __GNUC__ >= 4
    const CT__xdc_runtime_Text_Module__diagsEnabled xdc_runtime_Text_Module__diagsEnabled__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Module__diagsIncluded xdc_runtime_Text_Module__diagsIncluded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Module__diagsMask xdc_runtime_Text_Module__diagsMask__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Module__gateObj xdc_runtime_Text_Module__gateObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Module__gatePrms xdc_runtime_Text_Module__gatePrms__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Module__id xdc_runtime_Text_Module__id__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Module__loggerDefined xdc_runtime_Text_Module__loggerDefined__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Module__loggerObj xdc_runtime_Text_Module__loggerObj__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Module__loggerFxn4 xdc_runtime_Text_Module__loggerFxn4__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Module__loggerFxn8 xdc_runtime_Text_Module__loggerFxn8__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Module__startupDoneFxn xdc_runtime_Text_Module__startupDoneFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Object__count xdc_runtime_Text_Object__count__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Object__heap xdc_runtime_Text_Object__heap__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Object__sizeof xdc_runtime_Text_Object__sizeof__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_Object__table xdc_runtime_Text_Object__table__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_nameUnknown xdc_runtime_Text_nameUnknown__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_nameEmpty xdc_runtime_Text_nameEmpty__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_nameStatic xdc_runtime_Text_nameStatic__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_isLoaded xdc_runtime_Text_isLoaded__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_charTab xdc_runtime_Text_charTab__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_nodeTab xdc_runtime_Text_nodeTab__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_charCnt xdc_runtime_Text_charCnt__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_nodeCnt xdc_runtime_Text_nodeCnt__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_visitRopeFxn xdc_runtime_Text_visitRopeFxn__C __attribute__ ((externally_visible));
    const CT__xdc_runtime_Text_visitRopeFxn2 xdc_runtime_Text_visitRopeFxn2__C __attribute__ ((externally_visible));
    xdc_runtime_Types_Label* xdc_runtime_Text_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Text_Module__startupDone__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Text_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Text_Object__delete__S( xdc_Ptr instp ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Text_Object__destruct__S( xdc_Ptr objp ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Text_Object__get__S( xdc_Ptr oarr, xdc_Int i ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Text_Object__first__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Text_Object__next__S( xdc_Ptr obj ) __attribute__ ((externally_visible));
    xdc_Void xdc_runtime_Text_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz ) __attribute__ ((externally_visible));
    xdc_Bool xdc_runtime_Text_Proxy__abstract__S( void ) __attribute__ ((externally_visible));
    xdc_Ptr xdc_runtime_Text_Proxy__delegate__S( void ) __attribute__ ((externally_visible));
    xdc_String xdc_runtime_Text_cordText__E( xdc_runtime_Text_CordAddr cord ) __attribute__ ((externally_visible));
    xdc_String xdc_runtime_Text_ropeText__E( xdc_runtime_Text_RopeId rope ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_Text_matchRope__E( xdc_runtime_Text_RopeId rope, xdc_String pat, xdc_Int* lenp ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_Text_putLab__E( xdc_runtime_Types_Label* lab, xdc_Char** bufp, xdc_Int len ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_Text_putMod__E( xdc_runtime_Types_ModuleId mid, xdc_Char** bufp, xdc_Int len ) __attribute__ ((externally_visible));
    xdc_Int xdc_runtime_Text_putSite__E( xdc_runtime_Types_Site* site, xdc_Char** bufp, xdc_Int len ) __attribute__ ((externally_visible));
#endif
#endif

/*
 * ======== INITIALIZATION ENTRY POINT ========
 */

extern int __xdc__init(void);
#ifdef __GNUC__
#if __GNUC__ >= 4
    __attribute__ ((externally_visible))
#endif
#endif
__FAR__ int (* volatile __xdc__init__addr)(void) = &__xdc__init;


/*
 * ======== PROGRAM GLOBALS ========
 */


/*
 * ======== CLINK DIRECTIVES ========
 */

#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_IModule_Interface__BASE__C, ".const:xdc_runtime_IModule_Interface__BASE__C");
    asm("	.sect \".const:xdc_runtime_IModule_Interface__BASE__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_IModule_Interface__BASE__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_IModule_Interface__BASE__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_IGateProvider_Interface__BASE__C, ".const:xdc_runtime_IGateProvider_Interface__BASE__C");
    asm("	.sect \".const:xdc_runtime_IGateProvider_Interface__BASE__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_IGateProvider_Interface__BASE__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_IGateProvider_Interface__BASE__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_IHeap_Interface__BASE__C, ".const:xdc_runtime_IHeap_Interface__BASE__C");
    asm("	.sect \".const:xdc_runtime_IHeap_Interface__BASE__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_IHeap_Interface__BASE__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_IHeap_Interface__BASE__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_ISystemSupport_Interface__BASE__C, ".const:xdc_runtime_ISystemSupport_Interface__BASE__C");
    asm("	.sect \".const:xdc_runtime_ISystemSupport_Interface__BASE__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_ISystemSupport_Interface__BASE__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_ISystemSupport_Interface__BASE__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__FXNS__C, ".const:xdc_runtime_GateNull_Module__FXNS__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__FXNS__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__FXNS__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__FXNS__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__FXNS__C, ".const:xdc_runtime_HeapStd_Module__FXNS__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__FXNS__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__FXNS__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__FXNS__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__FXNS__C, ".const:xdc_runtime_SysMin_Module__FXNS__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__FXNS__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__FXNS__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__FXNS__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Object__PARAMS__C, ".const:xdc_runtime_GateNull_Object__PARAMS__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Object__PARAMS__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Object__PARAMS__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Object__PARAMS__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Object__PARAMS__C, ".const:xdc_runtime_HeapStd_Object__PARAMS__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Object__PARAMS__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Object__PARAMS__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Object__PARAMS__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__diagsEnabled__C, ".const:hello_mod_Talker_Module__diagsEnabled__C");
    asm("	.sect \".const:hello_mod_Talker_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__diagsIncluded__C, ".const:hello_mod_Talker_Module__diagsIncluded__C");
    asm("	.sect \".const:hello_mod_Talker_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__diagsMask__C, ".const:hello_mod_Talker_Module__diagsMask__C");
    asm("	.sect \".const:hello_mod_Talker_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__gateObj__C, ".const:hello_mod_Talker_Module__gateObj__C");
    asm("	.sect \".const:hello_mod_Talker_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__gatePrms__C, ".const:hello_mod_Talker_Module__gatePrms__C");
    asm("	.sect \".const:hello_mod_Talker_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__id__C, ".const:hello_mod_Talker_Module__id__C");
    asm("	.sect \".const:hello_mod_Talker_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__loggerDefined__C, ".const:hello_mod_Talker_Module__loggerDefined__C");
    asm("	.sect \".const:hello_mod_Talker_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__loggerObj__C, ".const:hello_mod_Talker_Module__loggerObj__C");
    asm("	.sect \".const:hello_mod_Talker_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__loggerFxn4__C, ".const:hello_mod_Talker_Module__loggerFxn4__C");
    asm("	.sect \".const:hello_mod_Talker_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__loggerFxn8__C, ".const:hello_mod_Talker_Module__loggerFxn8__C");
    asm("	.sect \".const:hello_mod_Talker_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Module__startupDoneFxn__C, ".const:hello_mod_Talker_Module__startupDoneFxn__C");
    asm("	.sect \".const:hello_mod_Talker_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Object__count__C, ".const:hello_mod_Talker_Object__count__C");
    asm("	.sect \".const:hello_mod_Talker_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Object__heap__C, ".const:hello_mod_Talker_Object__heap__C");
    asm("	.sect \".const:hello_mod_Talker_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Object__sizeof__C, ".const:hello_mod_Talker_Object__sizeof__C");
    asm("	.sect \".const:hello_mod_Talker_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_Object__table__C, ".const:hello_mod_Talker_Object__table__C");
    asm("	.sect \".const:hello_mod_Talker_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_text__C, ".const:hello_mod_Talker_text__C");
    asm("	.sect \".const:hello_mod_Talker_text__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_text__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_text__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(hello_mod_Talker_count__C, ".const:hello_mod_Talker_count__C");
    asm("	.sect \".const:hello_mod_Talker_count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:hello_mod_Talker_count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:hello_mod_Talker_count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__diagsEnabled__C, ".const:xdc_runtime_Assert_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__diagsIncluded__C, ".const:xdc_runtime_Assert_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__diagsMask__C, ".const:xdc_runtime_Assert_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__gateObj__C, ".const:xdc_runtime_Assert_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__gatePrms__C, ".const:xdc_runtime_Assert_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__id__C, ".const:xdc_runtime_Assert_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerDefined__C, ".const:xdc_runtime_Assert_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerObj__C, ".const:xdc_runtime_Assert_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerFxn4__C, ".const:xdc_runtime_Assert_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__loggerFxn8__C, ".const:xdc_runtime_Assert_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Module__startupDoneFxn__C, ".const:xdc_runtime_Assert_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Assert_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Object__count__C, ".const:xdc_runtime_Assert_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Assert_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Object__heap__C, ".const:xdc_runtime_Assert_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Assert_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Object__sizeof__C, ".const:xdc_runtime_Assert_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Assert_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_Object__table__C, ".const:xdc_runtime_Assert_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Assert_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Assert_E_assertFailed__C, ".const:xdc_runtime_Assert_E_assertFailed__C");
    asm("	.sect \".const:xdc_runtime_Assert_E_assertFailed__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Assert_E_assertFailed__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Assert_E_assertFailed__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__diagsEnabled__C, ".const:xdc_runtime_Core_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__diagsIncluded__C, ".const:xdc_runtime_Core_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__diagsMask__C, ".const:xdc_runtime_Core_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__gateObj__C, ".const:xdc_runtime_Core_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__gatePrms__C, ".const:xdc_runtime_Core_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__id__C, ".const:xdc_runtime_Core_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__loggerDefined__C, ".const:xdc_runtime_Core_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__loggerObj__C, ".const:xdc_runtime_Core_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__loggerFxn4__C, ".const:xdc_runtime_Core_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__loggerFxn8__C, ".const:xdc_runtime_Core_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Module__startupDoneFxn__C, ".const:xdc_runtime_Core_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Core_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Object__count__C, ".const:xdc_runtime_Core_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Core_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Object__heap__C, ".const:xdc_runtime_Core_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Core_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Object__sizeof__C, ".const:xdc_runtime_Core_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Core_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_Object__table__C, ".const:xdc_runtime_Core_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Core_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Core_A_initializedParams__C, ".const:xdc_runtime_Core_A_initializedParams__C");
    asm("	.sect \".const:xdc_runtime_Core_A_initializedParams__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Core_A_initializedParams__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Core_A_initializedParams__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__diagsEnabled__C, ".const:xdc_runtime_Defaults_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__diagsIncluded__C, ".const:xdc_runtime_Defaults_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__diagsMask__C, ".const:xdc_runtime_Defaults_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__gateObj__C, ".const:xdc_runtime_Defaults_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__gatePrms__C, ".const:xdc_runtime_Defaults_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__id__C, ".const:xdc_runtime_Defaults_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerDefined__C, ".const:xdc_runtime_Defaults_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerObj__C, ".const:xdc_runtime_Defaults_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerFxn4__C, ".const:xdc_runtime_Defaults_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__loggerFxn8__C, ".const:xdc_runtime_Defaults_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Module__startupDoneFxn__C, ".const:xdc_runtime_Defaults_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Object__count__C, ".const:xdc_runtime_Defaults_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Object__heap__C, ".const:xdc_runtime_Defaults_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Object__sizeof__C, ".const:xdc_runtime_Defaults_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Defaults_Object__table__C, ".const:xdc_runtime_Defaults_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Defaults_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Defaults_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Defaults_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__diagsEnabled__C, ".const:xdc_runtime_Diags_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__diagsIncluded__C, ".const:xdc_runtime_Diags_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__diagsMask__C, ".const:xdc_runtime_Diags_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__gateObj__C, ".const:xdc_runtime_Diags_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__gatePrms__C, ".const:xdc_runtime_Diags_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__id__C, ".const:xdc_runtime_Diags_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerDefined__C, ".const:xdc_runtime_Diags_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerObj__C, ".const:xdc_runtime_Diags_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerFxn4__C, ".const:xdc_runtime_Diags_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__loggerFxn8__C, ".const:xdc_runtime_Diags_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Module__startupDoneFxn__C, ".const:xdc_runtime_Diags_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Diags_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Object__count__C, ".const:xdc_runtime_Diags_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Diags_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Object__heap__C, ".const:xdc_runtime_Diags_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Diags_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Object__sizeof__C, ".const:xdc_runtime_Diags_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Diags_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_Object__table__C, ".const:xdc_runtime_Diags_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Diags_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_setMaskEnabled__C, ".const:xdc_runtime_Diags_setMaskEnabled__C");
    asm("	.sect \".const:xdc_runtime_Diags_setMaskEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_setMaskEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_setMaskEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Diags_dictBase__C, ".const:xdc_runtime_Diags_dictBase__C");
    asm("	.sect \".const:xdc_runtime_Diags_dictBase__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Diags_dictBase__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Diags_dictBase__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__diagsEnabled__C, ".const:xdc_runtime_Error_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__diagsIncluded__C, ".const:xdc_runtime_Error_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__diagsMask__C, ".const:xdc_runtime_Error_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__gateObj__C, ".const:xdc_runtime_Error_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__gatePrms__C, ".const:xdc_runtime_Error_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__id__C, ".const:xdc_runtime_Error_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__loggerDefined__C, ".const:xdc_runtime_Error_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__loggerObj__C, ".const:xdc_runtime_Error_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__loggerFxn4__C, ".const:xdc_runtime_Error_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__loggerFxn8__C, ".const:xdc_runtime_Error_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Module__startupDoneFxn__C, ".const:xdc_runtime_Error_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Error_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Object__count__C, ".const:xdc_runtime_Error_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Error_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Object__heap__C, ".const:xdc_runtime_Error_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Error_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Object__sizeof__C, ".const:xdc_runtime_Error_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Error_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_Object__table__C, ".const:xdc_runtime_Error_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Error_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_E_generic__C, ".const:xdc_runtime_Error_E_generic__C");
    asm("	.sect \".const:xdc_runtime_Error_E_generic__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_E_generic__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_E_generic__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_E_memory__C, ".const:xdc_runtime_Error_E_memory__C");
    asm("	.sect \".const:xdc_runtime_Error_E_memory__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_E_memory__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_E_memory__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_policy__C, ".const:xdc_runtime_Error_policy__C");
    asm("	.sect \".const:xdc_runtime_Error_policy__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_policy__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_policy__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_raiseHook__C, ".const:xdc_runtime_Error_raiseHook__C");
    asm("	.sect \".const:xdc_runtime_Error_raiseHook__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_raiseHook__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_raiseHook__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Error_maxDepth__C, ".const:xdc_runtime_Error_maxDepth__C");
    asm("	.sect \".const:xdc_runtime_Error_maxDepth__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Error_maxDepth__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Error_maxDepth__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__diagsEnabled__C, ".const:xdc_runtime_Gate_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__diagsIncluded__C, ".const:xdc_runtime_Gate_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__diagsMask__C, ".const:xdc_runtime_Gate_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__gateObj__C, ".const:xdc_runtime_Gate_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__gatePrms__C, ".const:xdc_runtime_Gate_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__id__C, ".const:xdc_runtime_Gate_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerDefined__C, ".const:xdc_runtime_Gate_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerObj__C, ".const:xdc_runtime_Gate_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerFxn4__C, ".const:xdc_runtime_Gate_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__loggerFxn8__C, ".const:xdc_runtime_Gate_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Module__startupDoneFxn__C, ".const:xdc_runtime_Gate_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Gate_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Object__count__C, ".const:xdc_runtime_Gate_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Gate_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Object__heap__C, ".const:xdc_runtime_Gate_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Gate_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Object__sizeof__C, ".const:xdc_runtime_Gate_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Gate_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Gate_Object__table__C, ".const:xdc_runtime_Gate_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Gate_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Gate_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Gate_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Object__DESC__C, ".const:xdc_runtime_GateNull_Object__DESC__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Object__DESC__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Object__DESC__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Object__DESC__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Object__PARAMS__C, ".const:xdc_runtime_GateNull_Object__PARAMS__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Object__PARAMS__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Object__PARAMS__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Object__PARAMS__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__diagsEnabled__C, ".const:xdc_runtime_GateNull_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__diagsIncluded__C, ".const:xdc_runtime_GateNull_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__diagsMask__C, ".const:xdc_runtime_GateNull_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__gateObj__C, ".const:xdc_runtime_GateNull_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__gatePrms__C, ".const:xdc_runtime_GateNull_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__id__C, ".const:xdc_runtime_GateNull_Module__id__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__loggerDefined__C, ".const:xdc_runtime_GateNull_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__loggerObj__C, ".const:xdc_runtime_GateNull_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__loggerFxn4__C, ".const:xdc_runtime_GateNull_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__loggerFxn8__C, ".const:xdc_runtime_GateNull_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Module__startupDoneFxn__C, ".const:xdc_runtime_GateNull_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Object__count__C, ".const:xdc_runtime_GateNull_Object__count__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Object__heap__C, ".const:xdc_runtime_GateNull_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Object__sizeof__C, ".const:xdc_runtime_GateNull_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_GateNull_Object__table__C, ".const:xdc_runtime_GateNull_Object__table__C");
    asm("	.sect \".const:xdc_runtime_GateNull_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_GateNull_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_GateNull_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Object__DESC__C, ".const:xdc_runtime_HeapStd_Object__DESC__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Object__DESC__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Object__DESC__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Object__DESC__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Object__PARAMS__C, ".const:xdc_runtime_HeapStd_Object__PARAMS__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Object__PARAMS__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Object__PARAMS__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Object__PARAMS__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__diagsEnabled__C, ".const:xdc_runtime_HeapStd_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__diagsIncluded__C, ".const:xdc_runtime_HeapStd_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__diagsMask__C, ".const:xdc_runtime_HeapStd_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__gateObj__C, ".const:xdc_runtime_HeapStd_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__gatePrms__C, ".const:xdc_runtime_HeapStd_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__id__C, ".const:xdc_runtime_HeapStd_Module__id__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__loggerDefined__C, ".const:xdc_runtime_HeapStd_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__loggerObj__C, ".const:xdc_runtime_HeapStd_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__loggerFxn4__C, ".const:xdc_runtime_HeapStd_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__loggerFxn8__C, ".const:xdc_runtime_HeapStd_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Module__startupDoneFxn__C, ".const:xdc_runtime_HeapStd_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Object__count__C, ".const:xdc_runtime_HeapStd_Object__count__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Object__heap__C, ".const:xdc_runtime_HeapStd_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Object__sizeof__C, ".const:xdc_runtime_HeapStd_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_Object__table__C, ".const:xdc_runtime_HeapStd_Object__table__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_E_noRTSMemory__C, ".const:xdc_runtime_HeapStd_E_noRTSMemory__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_E_noRTSMemory__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_E_noRTSMemory__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_E_noRTSMemory__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_A_zeroSize__C, ".const:xdc_runtime_HeapStd_A_zeroSize__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_A_zeroSize__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_A_zeroSize__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_A_zeroSize__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_A_invalidTotalFreeSize__C, ".const:xdc_runtime_HeapStd_A_invalidTotalFreeSize__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_A_invalidTotalFreeSize__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_A_invalidTotalFreeSize__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_A_invalidTotalFreeSize__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_HeapStd_A_invalidAlignment__C, ".const:xdc_runtime_HeapStd_A_invalidAlignment__C");
    asm("	.sect \".const:xdc_runtime_HeapStd_A_invalidAlignment__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_HeapStd_A_invalidAlignment__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_HeapStd_A_invalidAlignment__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__diagsEnabled__C, ".const:xdc_runtime_Log_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__diagsIncluded__C, ".const:xdc_runtime_Log_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__diagsMask__C, ".const:xdc_runtime_Log_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__gateObj__C, ".const:xdc_runtime_Log_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__gatePrms__C, ".const:xdc_runtime_Log_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__id__C, ".const:xdc_runtime_Log_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__loggerDefined__C, ".const:xdc_runtime_Log_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__loggerObj__C, ".const:xdc_runtime_Log_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__loggerFxn4__C, ".const:xdc_runtime_Log_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__loggerFxn8__C, ".const:xdc_runtime_Log_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Module__startupDoneFxn__C, ".const:xdc_runtime_Log_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Log_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Object__count__C, ".const:xdc_runtime_Log_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Log_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Object__heap__C, ".const:xdc_runtime_Log_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Log_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Object__sizeof__C, ".const:xdc_runtime_Log_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Log_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_Object__table__C, ".const:xdc_runtime_Log_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Log_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_L_construct__C, ".const:xdc_runtime_Log_L_construct__C");
    asm("	.sect \".const:xdc_runtime_Log_L_construct__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_L_construct__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_L_construct__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_L_create__C, ".const:xdc_runtime_Log_L_create__C");
    asm("	.sect \".const:xdc_runtime_Log_L_create__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_L_create__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_L_create__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_L_destruct__C, ".const:xdc_runtime_Log_L_destruct__C");
    asm("	.sect \".const:xdc_runtime_Log_L_destruct__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_L_destruct__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_L_destruct__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Log_L_delete__C, ".const:xdc_runtime_Log_L_delete__C");
    asm("	.sect \".const:xdc_runtime_Log_L_delete__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Log_L_delete__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Log_L_delete__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__diagsEnabled__C, ".const:xdc_runtime_Main_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__diagsIncluded__C, ".const:xdc_runtime_Main_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__diagsMask__C, ".const:xdc_runtime_Main_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__gateObj__C, ".const:xdc_runtime_Main_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__gatePrms__C, ".const:xdc_runtime_Main_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__id__C, ".const:xdc_runtime_Main_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__loggerDefined__C, ".const:xdc_runtime_Main_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__loggerObj__C, ".const:xdc_runtime_Main_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__loggerFxn4__C, ".const:xdc_runtime_Main_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__loggerFxn8__C, ".const:xdc_runtime_Main_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Module__startupDoneFxn__C, ".const:xdc_runtime_Main_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Main_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Object__count__C, ".const:xdc_runtime_Main_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Main_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Object__heap__C, ".const:xdc_runtime_Main_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Main_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Object__sizeof__C, ".const:xdc_runtime_Main_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Main_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Main_Object__table__C, ".const:xdc_runtime_Main_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Main_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Main_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Main_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__diagsEnabled__C, ".const:xdc_runtime_Memory_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__diagsIncluded__C, ".const:xdc_runtime_Memory_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__diagsMask__C, ".const:xdc_runtime_Memory_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__gateObj__C, ".const:xdc_runtime_Memory_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__gatePrms__C, ".const:xdc_runtime_Memory_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__id__C, ".const:xdc_runtime_Memory_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerDefined__C, ".const:xdc_runtime_Memory_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerObj__C, ".const:xdc_runtime_Memory_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerFxn4__C, ".const:xdc_runtime_Memory_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__loggerFxn8__C, ".const:xdc_runtime_Memory_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Module__startupDoneFxn__C, ".const:xdc_runtime_Memory_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Memory_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Object__count__C, ".const:xdc_runtime_Memory_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Memory_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Object__heap__C, ".const:xdc_runtime_Memory_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Memory_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Object__sizeof__C, ".const:xdc_runtime_Memory_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Memory_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_Object__table__C, ".const:xdc_runtime_Memory_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Memory_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Memory_defaultHeapInstance__C, ".const:xdc_runtime_Memory_defaultHeapInstance__C");
    asm("	.sect \".const:xdc_runtime_Memory_defaultHeapInstance__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Memory_defaultHeapInstance__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Memory_defaultHeapInstance__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_sfxnTab__A, ".const:xdc_runtime_Startup_sfxnTab__A");
    asm("	.sect \".const:xdc_runtime_Startup_sfxnTab__A\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_sfxnTab__A\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_sfxnTab__A\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_sfxnRts__A, ".const:xdc_runtime_Startup_sfxnRts__A");
    asm("	.sect \".const:xdc_runtime_Startup_sfxnRts__A\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_sfxnRts__A\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_sfxnRts__A\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__diagsEnabled__C, ".const:xdc_runtime_Startup_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__diagsIncluded__C, ".const:xdc_runtime_Startup_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__diagsMask__C, ".const:xdc_runtime_Startup_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__gateObj__C, ".const:xdc_runtime_Startup_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__gatePrms__C, ".const:xdc_runtime_Startup_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__id__C, ".const:xdc_runtime_Startup_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerDefined__C, ".const:xdc_runtime_Startup_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerObj__C, ".const:xdc_runtime_Startup_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerFxn4__C, ".const:xdc_runtime_Startup_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__loggerFxn8__C, ".const:xdc_runtime_Startup_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Module__startupDoneFxn__C, ".const:xdc_runtime_Startup_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Startup_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Object__count__C, ".const:xdc_runtime_Startup_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Startup_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Object__heap__C, ".const:xdc_runtime_Startup_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Startup_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Object__sizeof__C, ".const:xdc_runtime_Startup_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Startup_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_Object__table__C, ".const:xdc_runtime_Startup_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Startup_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_maxPasses__C, ".const:xdc_runtime_Startup_maxPasses__C");
    asm("	.sect \".const:xdc_runtime_Startup_maxPasses__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_maxPasses__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_maxPasses__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_firstFxns__C, ".const:xdc_runtime_Startup_firstFxns__C");
    asm("	.sect \".const:xdc_runtime_Startup_firstFxns__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_firstFxns__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_firstFxns__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_lastFxns__C, ".const:xdc_runtime_Startup_lastFxns__C");
    asm("	.sect \".const:xdc_runtime_Startup_lastFxns__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_lastFxns__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_lastFxns__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_startModsFxn__C, ".const:xdc_runtime_Startup_startModsFxn__C");
    asm("	.sect \".const:xdc_runtime_Startup_startModsFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_startModsFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_startModsFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_execImpl__C, ".const:xdc_runtime_Startup_execImpl__C");
    asm("	.sect \".const:xdc_runtime_Startup_execImpl__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_execImpl__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_execImpl__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_sfxnTab__C, ".const:xdc_runtime_Startup_sfxnTab__C");
    asm("	.sect \".const:xdc_runtime_Startup_sfxnTab__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_sfxnTab__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_sfxnTab__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Startup_sfxnRts__C, ".const:xdc_runtime_Startup_sfxnRts__C");
    asm("	.sect \".const:xdc_runtime_Startup_sfxnRts__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Startup_sfxnRts__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Startup_sfxnRts__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__diagsEnabled__C, ".const:xdc_runtime_SysMin_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__diagsIncluded__C, ".const:xdc_runtime_SysMin_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__diagsMask__C, ".const:xdc_runtime_SysMin_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__gateObj__C, ".const:xdc_runtime_SysMin_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__gatePrms__C, ".const:xdc_runtime_SysMin_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__id__C, ".const:xdc_runtime_SysMin_Module__id__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerDefined__C, ".const:xdc_runtime_SysMin_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerObj__C, ".const:xdc_runtime_SysMin_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerFxn4__C, ".const:xdc_runtime_SysMin_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__loggerFxn8__C, ".const:xdc_runtime_SysMin_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Module__startupDoneFxn__C, ".const:xdc_runtime_SysMin_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Object__count__C, ".const:xdc_runtime_SysMin_Object__count__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Object__heap__C, ".const:xdc_runtime_SysMin_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Object__sizeof__C, ".const:xdc_runtime_SysMin_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_Object__table__C, ".const:xdc_runtime_SysMin_Object__table__C");
    asm("	.sect \".const:xdc_runtime_SysMin_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_bufSize__C, ".const:xdc_runtime_SysMin_bufSize__C");
    asm("	.sect \".const:xdc_runtime_SysMin_bufSize__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_bufSize__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_bufSize__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_flushAtExit__C, ".const:xdc_runtime_SysMin_flushAtExit__C");
    asm("	.sect \".const:xdc_runtime_SysMin_flushAtExit__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_flushAtExit__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_flushAtExit__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_outputFxn__C, ".const:xdc_runtime_SysMin_outputFxn__C");
    asm("	.sect \".const:xdc_runtime_SysMin_outputFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_outputFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_outputFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_SysMin_outputFunc__C, ".const:xdc_runtime_SysMin_outputFunc__C");
    asm("	.sect \".const:xdc_runtime_SysMin_outputFunc__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_SysMin_outputFunc__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_SysMin_outputFunc__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__diagsEnabled__C, ".const:xdc_runtime_System_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_System_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__diagsIncluded__C, ".const:xdc_runtime_System_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_System_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__diagsMask__C, ".const:xdc_runtime_System_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_System_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__gateObj__C, ".const:xdc_runtime_System_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_System_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__gatePrms__C, ".const:xdc_runtime_System_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_System_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__id__C, ".const:xdc_runtime_System_Module__id__C");
    asm("	.sect \".const:xdc_runtime_System_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__loggerDefined__C, ".const:xdc_runtime_System_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_System_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__loggerObj__C, ".const:xdc_runtime_System_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_System_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__loggerFxn4__C, ".const:xdc_runtime_System_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_System_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__loggerFxn8__C, ".const:xdc_runtime_System_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_System_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Module__startupDoneFxn__C, ".const:xdc_runtime_System_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_System_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Object__count__C, ".const:xdc_runtime_System_Object__count__C");
    asm("	.sect \".const:xdc_runtime_System_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Object__heap__C, ".const:xdc_runtime_System_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_System_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Object__sizeof__C, ".const:xdc_runtime_System_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_System_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_Object__table__C, ".const:xdc_runtime_System_Object__table__C");
    asm("	.sect \".const:xdc_runtime_System_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_A_cannotFitIntoArg__C, ".const:xdc_runtime_System_A_cannotFitIntoArg__C");
    asm("	.sect \".const:xdc_runtime_System_A_cannotFitIntoArg__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_A_cannotFitIntoArg__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_A_cannotFitIntoArg__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_maxAtexitHandlers__C, ".const:xdc_runtime_System_maxAtexitHandlers__C");
    asm("	.sect \".const:xdc_runtime_System_maxAtexitHandlers__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_maxAtexitHandlers__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_maxAtexitHandlers__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_System_extendFxn__C, ".const:xdc_runtime_System_extendFxn__C");
    asm("	.sect \".const:xdc_runtime_System_extendFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_System_extendFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_System_extendFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_charTab__A, ".const:xdc_runtime_Text_charTab__A");
    asm("	.sect \".const:xdc_runtime_Text_charTab__A\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_charTab__A\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_charTab__A\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_nodeTab__A, ".const:xdc_runtime_Text_nodeTab__A");
    asm("	.sect \".const:xdc_runtime_Text_nodeTab__A\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_nodeTab__A\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_nodeTab__A\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__diagsEnabled__C, ".const:xdc_runtime_Text_Module__diagsEnabled__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__diagsEnabled__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__diagsEnabled__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__diagsIncluded__C, ".const:xdc_runtime_Text_Module__diagsIncluded__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__diagsIncluded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__diagsIncluded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__diagsMask__C, ".const:xdc_runtime_Text_Module__diagsMask__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__diagsMask__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__diagsMask__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__gateObj__C, ".const:xdc_runtime_Text_Module__gateObj__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__gateObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__gateObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__gatePrms__C, ".const:xdc_runtime_Text_Module__gatePrms__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__gatePrms__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__gatePrms__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__id__C, ".const:xdc_runtime_Text_Module__id__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__id__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__id__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__loggerDefined__C, ".const:xdc_runtime_Text_Module__loggerDefined__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__loggerDefined__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__loggerDefined__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__loggerObj__C, ".const:xdc_runtime_Text_Module__loggerObj__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__loggerObj__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__loggerObj__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__loggerFxn4__C, ".const:xdc_runtime_Text_Module__loggerFxn4__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__loggerFxn4__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__loggerFxn4__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__loggerFxn8__C, ".const:xdc_runtime_Text_Module__loggerFxn8__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__loggerFxn8__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__loggerFxn8__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Module__startupDoneFxn__C, ".const:xdc_runtime_Text_Module__startupDoneFxn__C");
    asm("	.sect \".const:xdc_runtime_Text_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Module__startupDoneFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Module__startupDoneFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Object__count__C, ".const:xdc_runtime_Text_Object__count__C");
    asm("	.sect \".const:xdc_runtime_Text_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Object__count__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Object__count__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Object__heap__C, ".const:xdc_runtime_Text_Object__heap__C");
    asm("	.sect \".const:xdc_runtime_Text_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Object__heap__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Object__heap__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Object__sizeof__C, ".const:xdc_runtime_Text_Object__sizeof__C");
    asm("	.sect \".const:xdc_runtime_Text_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Object__sizeof__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Object__sizeof__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_Object__table__C, ".const:xdc_runtime_Text_Object__table__C");
    asm("	.sect \".const:xdc_runtime_Text_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_Object__table__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_Object__table__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_nameUnknown__C, ".const:xdc_runtime_Text_nameUnknown__C");
    asm("	.sect \".const:xdc_runtime_Text_nameUnknown__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_nameUnknown__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_nameUnknown__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_nameEmpty__C, ".const:xdc_runtime_Text_nameEmpty__C");
    asm("	.sect \".const:xdc_runtime_Text_nameEmpty__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_nameEmpty__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_nameEmpty__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_nameStatic__C, ".const:xdc_runtime_Text_nameStatic__C");
    asm("	.sect \".const:xdc_runtime_Text_nameStatic__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_nameStatic__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_nameStatic__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_isLoaded__C, ".const:xdc_runtime_Text_isLoaded__C");
    asm("	.sect \".const:xdc_runtime_Text_isLoaded__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_isLoaded__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_isLoaded__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_charTab__C, ".const:xdc_runtime_Text_charTab__C");
    asm("	.sect \".const:xdc_runtime_Text_charTab__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_charTab__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_charTab__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_nodeTab__C, ".const:xdc_runtime_Text_nodeTab__C");
    asm("	.sect \".const:xdc_runtime_Text_nodeTab__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_nodeTab__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_nodeTab__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_charCnt__C, ".const:xdc_runtime_Text_charCnt__C");
    asm("	.sect \".const:xdc_runtime_Text_charCnt__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_charCnt__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_charCnt__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_nodeCnt__C, ".const:xdc_runtime_Text_nodeCnt__C");
    asm("	.sect \".const:xdc_runtime_Text_nodeCnt__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_nodeCnt__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_nodeCnt__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_visitRopeFxn__C, ".const:xdc_runtime_Text_visitRopeFxn__C");
    asm("	.sect \".const:xdc_runtime_Text_visitRopeFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_visitRopeFxn__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_visitRopeFxn__C\"");
    asm("	 .clink");
#endif
#ifdef __ti__
    #pragma DATA_SECTION(xdc_runtime_Text_visitRopeFxn2__C, ".const:xdc_runtime_Text_visitRopeFxn2__C");
    asm("	.sect \".const:xdc_runtime_Text_visitRopeFxn2__C\"");
    asm("	 .clink");
    asm("	.sect \"[0].const:xdc_runtime_Text_visitRopeFxn2__C\"");
    asm("	 .clink");
    asm("	.sect \"[1].const:xdc_runtime_Text_visitRopeFxn2__C\"");
    asm("	 .clink");
#endif
