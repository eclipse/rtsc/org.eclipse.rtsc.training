/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

#ifndef hello_mod_Talker__INTERNAL__
#define hello_mod_Talker__INTERNAL__

#ifndef hello_mod_Talker__internalaccess
#define hello_mod_Talker__internalaccess
#endif

#include <hello/mod/Talker.h>

#undef xdc_FILE__
#ifndef xdc_FILE
#define xdc_FILE__ NULL
#else
#define xdc_FILE__ xdc_FILE
#endif

/* print */
#undef hello_mod_Talker_print
#define hello_mod_Talker_print hello_mod_Talker_print__F

/* Module_startup */
#undef hello_mod_Talker_Module_startup
#define hello_mod_Talker_Module_startup hello_mod_Talker_Module_startup__F

/* Instance_init */
#undef hello_mod_Talker_Instance_init
#define hello_mod_Talker_Instance_init hello_mod_Talker_Instance_init__F

/* Instance_finalize */
#undef hello_mod_Talker_Instance_finalize
#define hello_mod_Talker_Instance_finalize hello_mod_Talker_Instance_finalize__F

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID hello_mod_Talker_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL hello_mod_Talker_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB hello_mod_Talker_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK hello_mod_Talker_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF hello_mod_Talker_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ hello_mod_Talker_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 hello_mod_Talker_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 hello_mod_Talker_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ hello_mod_Talker_Module__gateObj__C
#undef Module__G_PRMS
#define Module__G_PRMS hello_mod_Talker_Module__gatePrms__C
#undef Module__GP_create
#define Module__GP_create hello_mod_Talker_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete hello_mod_Talker_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter hello_mod_Talker_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave hello_mod_Talker_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query hello_mod_Talker_Module_GateProxy_query


#endif /* hello_mod_Talker__INTERNAL____ */
