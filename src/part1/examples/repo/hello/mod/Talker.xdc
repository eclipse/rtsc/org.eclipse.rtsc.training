/*
 *  ======== hello/mod/Talker.xdc ========
 */

/*! Our first module */
module Talker {
 
    /*! What to say */
    config String text = "Hello World";
    /*! How many times */
    config Int count = 1;
 
    /*! Say it */
    Void print();
}
