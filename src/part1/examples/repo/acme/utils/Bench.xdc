/*
 *  ======== acme/utils/Bench.xdc ========
 */

import acme.utils.clocks.IClock;

/*! Benchmark timing support */
@ModuleStartup
module Bench {
 
    /*! Selectable IClock service provider */
    proxy PClock inherits IClock;
 
    /*! Enable/disable benchmark functions */
    config Bool enableFlag = true;
 
    /*! Start the benchmark */
    Void begin(String msg);
 
    /*! End the benchmark and print the current time */
    Void end();
 
internal:
 
    struct Module_State {
        String beginMsg;    /* current begin() message */
        Int beginClock;     /* begin() start time */
        Int overhead;       /* timing overhead factor */
    };
}
