/*
 *  ======== acme/utils/Bench.c ========
 */

#include <xdc/runtime/Startup.h>
#include <xdc/runtime/System.h>
#include "package/internal/Bench.xdc.h"
 
Int Bench_Module_startup(Int state)
{
    Bench_begin(NULL);
    Bench_end();  	/* first-time calibration benchmark */
 
    return Startup_DONE;
}
 
Void Bench_begin(String msg)
{
    if (Bench_enableFlag) {
        module->beginClock = Bench_PClock_getTime();
        module->beginMsg = msg;
    }
}
 
Void Bench_end()
{
    if (Bench_enableFlag) {
        Int elapsed = Bench_PClock_getTime() - module->beginClock; /* elapsed time between now and last begin() */
        if (module->beginMsg == NULL) {                            /* first-time calibration call */
            module->overhead = elapsed;                            /* retain elapsed time for later correction */
        }
        else {                                                     /* correct elapsed time and print result */
            elapsed -= module->overhead;
            System_printf("%s [%d]\n", module->beginMsg, elapsed);
        }
    }
}
