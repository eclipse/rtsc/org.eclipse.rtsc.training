/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

/*
 * ======== GENERATED SECTIONS ========
 *     
 *     PROLOGUE
 *     INCLUDES
 *     
 *     INTERNAL DEFINITIONS
 *     MODULE-WIDE CONFIGS
 *     FUNCTION DECLARATIONS
 *     SYSTEM FUNCTIONS
 *     C++ CLIENT WRAPPER [experimental]
 *     
 *     EPILOGUE
 *     STATE STRUCTURES
 *     PREFIX ALIASES
 *     C++ SUPPLIER WRAPPER [experimental]
 */


/*
 * ======== PROLOGUE ========
 */

#ifndef acme_utils_Bench__include
#define acme_utils_Bench__include

#ifndef __nested__
#define __nested__
#define acme_utils_Bench__top__
#endif

#ifdef __cplusplus
#define __extern extern "C"
#else
#define __extern extern
#endif

#define acme_utils_Bench___VERS 150


/*
 * ======== INCLUDES ========
 */

#include <xdc/std.h>

#include <xdc/runtime/xdc.h>
#include <xdc/runtime/Types.h>
#include <acme/utils/package/package.defs.h>

#include <xdc/runtime/IModule.h>
#include <acme/utils/clocks/IClock.h>
#include <acme/utils/package/Bench_PClock.h>


/*
 * ======== AUXILIARY DEFINITIONS ========
 */


/*
 * ======== INTERNAL DEFINITIONS ========
 */


/*
 * ======== MODULE-WIDE CONFIGS ========
 */

/* Module__diagsEnabled */
typedef xdc_Bits32 CT__acme_utils_Bench_Module__diagsEnabled;
__extern __FAR__ const CT__acme_utils_Bench_Module__diagsEnabled acme_utils_Bench_Module__diagsEnabled__C;

/* Module__diagsIncluded */
typedef xdc_Bits32 CT__acme_utils_Bench_Module__diagsIncluded;
__extern __FAR__ const CT__acme_utils_Bench_Module__diagsIncluded acme_utils_Bench_Module__diagsIncluded__C;

/* Module__diagsMask */
typedef xdc_Bits16* CT__acme_utils_Bench_Module__diagsMask;
__extern __FAR__ const CT__acme_utils_Bench_Module__diagsMask acme_utils_Bench_Module__diagsMask__C;

/* Module__gateObj */
typedef xdc_Ptr CT__acme_utils_Bench_Module__gateObj;
__extern __FAR__ const CT__acme_utils_Bench_Module__gateObj acme_utils_Bench_Module__gateObj__C;

/* Module__gatePrms */
typedef xdc_Ptr CT__acme_utils_Bench_Module__gatePrms;
__extern __FAR__ const CT__acme_utils_Bench_Module__gatePrms acme_utils_Bench_Module__gatePrms__C;

/* Module__id */
typedef xdc_runtime_Types_ModuleId CT__acme_utils_Bench_Module__id;
__extern __FAR__ const CT__acme_utils_Bench_Module__id acme_utils_Bench_Module__id__C;

/* Module__loggerDefined */
typedef xdc_Bool CT__acme_utils_Bench_Module__loggerDefined;
__extern __FAR__ const CT__acme_utils_Bench_Module__loggerDefined acme_utils_Bench_Module__loggerDefined__C;

/* Module__loggerObj */
typedef xdc_Ptr CT__acme_utils_Bench_Module__loggerObj;
__extern __FAR__ const CT__acme_utils_Bench_Module__loggerObj acme_utils_Bench_Module__loggerObj__C;

/* Module__loggerFxn4 */
typedef xdc_runtime_Types_LoggerFxn4 CT__acme_utils_Bench_Module__loggerFxn4;
__extern __FAR__ const CT__acme_utils_Bench_Module__loggerFxn4 acme_utils_Bench_Module__loggerFxn4__C;

/* Module__loggerFxn8 */
typedef xdc_runtime_Types_LoggerFxn8 CT__acme_utils_Bench_Module__loggerFxn8;
__extern __FAR__ const CT__acme_utils_Bench_Module__loggerFxn8 acme_utils_Bench_Module__loggerFxn8__C;

/* Module__startupDoneFxn */
typedef xdc_Bool (*CT__acme_utils_Bench_Module__startupDoneFxn)(void);
__extern __FAR__ const CT__acme_utils_Bench_Module__startupDoneFxn acme_utils_Bench_Module__startupDoneFxn__C;

/* Object__count */
typedef xdc_Int CT__acme_utils_Bench_Object__count;
__extern __FAR__ const CT__acme_utils_Bench_Object__count acme_utils_Bench_Object__count__C;

/* Object__heap */
typedef xdc_runtime_IHeap_Handle CT__acme_utils_Bench_Object__heap;
__extern __FAR__ const CT__acme_utils_Bench_Object__heap acme_utils_Bench_Object__heap__C;

/* Object__sizeof */
typedef xdc_SizeT CT__acme_utils_Bench_Object__sizeof;
__extern __FAR__ const CT__acme_utils_Bench_Object__sizeof acme_utils_Bench_Object__sizeof__C;

/* Object__table */
typedef xdc_Ptr CT__acme_utils_Bench_Object__table;
__extern __FAR__ const CT__acme_utils_Bench_Object__table acme_utils_Bench_Object__table__C;

/* enableFlag */
#define acme_utils_Bench_enableFlag (acme_utils_Bench_enableFlag__C)
typedef xdc_Bool CT__acme_utils_Bench_enableFlag;
__extern __FAR__ const CT__acme_utils_Bench_enableFlag acme_utils_Bench_enableFlag__C;


/*
 * ======== FUNCTION DECLARATIONS ========
 */

/* Module_startup */
#define acme_utils_Bench_Module_startup acme_utils_Bench_Module_startup__E
xdc__CODESECT(acme_utils_Bench_Module_startup__E, "acme_utils_Bench_Module_startup")
__extern xdc_Int acme_utils_Bench_Module_startup__E( xdc_Int state );
xdc__CODESECT(acme_utils_Bench_Module_startup__F, "acme_utils_Bench_Module_startup")
__extern xdc_Int acme_utils_Bench_Module_startup__F( xdc_Int state );
xdc__CODESECT(acme_utils_Bench_Module_startup__R, "acme_utils_Bench_Module_startup")
__extern xdc_Int acme_utils_Bench_Module_startup__R( xdc_Int state );

/* Handle__label__S */
xdc__CODESECT(acme_utils_Bench_Handle__label__S, "acme_utils_Bench_Handle__label")
__extern xdc_runtime_Types_Label* acme_utils_Bench_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab );

/* Module__startupDone__S */
xdc__CODESECT(acme_utils_Bench_Module__startupDone__S, "acme_utils_Bench_Module__startupDone")
__extern xdc_Bool acme_utils_Bench_Module__startupDone__S( void );

/* Object__create__S */
xdc__CODESECT(acme_utils_Bench_Object__create__S, "acme_utils_Bench_Object__create")
__extern xdc_Ptr acme_utils_Bench_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb );

/* Object__delete__S */
xdc__CODESECT(acme_utils_Bench_Object__delete__S, "acme_utils_Bench_Object__delete")
__extern xdc_Void acme_utils_Bench_Object__delete__S( xdc_Ptr instp );

/* Object__destruct__S */
xdc__CODESECT(acme_utils_Bench_Object__destruct__S, "acme_utils_Bench_Object__destruct")
__extern xdc_Void acme_utils_Bench_Object__destruct__S( xdc_Ptr objp );

/* Object__get__S */
xdc__CODESECT(acme_utils_Bench_Object__get__S, "acme_utils_Bench_Object__get")
__extern xdc_Ptr acme_utils_Bench_Object__get__S( xdc_Ptr oarr, xdc_Int i );

/* Object__first__S */
xdc__CODESECT(acme_utils_Bench_Object__first__S, "acme_utils_Bench_Object__first")
__extern xdc_Ptr acme_utils_Bench_Object__first__S( void );

/* Object__next__S */
xdc__CODESECT(acme_utils_Bench_Object__next__S, "acme_utils_Bench_Object__next")
__extern xdc_Ptr acme_utils_Bench_Object__next__S( xdc_Ptr obj );

/* Params__init__S */
xdc__CODESECT(acme_utils_Bench_Params__init__S, "acme_utils_Bench_Params__init")
__extern xdc_Void acme_utils_Bench_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz );

/* Proxy__abstract__S */
xdc__CODESECT(acme_utils_Bench_Proxy__abstract__S, "acme_utils_Bench_Proxy__abstract")
__extern xdc_Bool acme_utils_Bench_Proxy__abstract__S( void );

/* Proxy__delegate__S */
xdc__CODESECT(acme_utils_Bench_Proxy__delegate__S, "acme_utils_Bench_Proxy__delegate")
__extern xdc_Ptr acme_utils_Bench_Proxy__delegate__S( void );

/* begin__E */
#define acme_utils_Bench_begin acme_utils_Bench_begin__E
xdc__CODESECT(acme_utils_Bench_begin__E, "acme_utils_Bench_begin")
__extern xdc_Void acme_utils_Bench_begin__E( xdc_String msg );
xdc__CODESECT(acme_utils_Bench_begin__F, "acme_utils_Bench_begin")
__extern xdc_Void acme_utils_Bench_begin__F( xdc_String msg );
__extern xdc_Void acme_utils_Bench_begin__R( xdc_String msg );

/* end__E */
#define acme_utils_Bench_end acme_utils_Bench_end__E
xdc__CODESECT(acme_utils_Bench_end__E, "acme_utils_Bench_end")
__extern xdc_Void acme_utils_Bench_end__E( void );
xdc__CODESECT(acme_utils_Bench_end__F, "acme_utils_Bench_end")
__extern xdc_Void acme_utils_Bench_end__F( void );
__extern xdc_Void acme_utils_Bench_end__R( void );


/*
 * ======== SYSTEM FUNCTIONS ========
 */

/* Module_startupDone */
#define acme_utils_Bench_Module_startupDone() acme_utils_Bench_Module__startupDone__S()

/* Object_heap */
#define acme_utils_Bench_Object_heap() acme_utils_Bench_Object__heap__C

/* Module_heap */
#define acme_utils_Bench_Module_heap() acme_utils_Bench_Object__heap__C

/* Module_id */
static inline CT__acme_utils_Bench_Module__id acme_utils_Bench_Module_id( void ) 
{
    return acme_utils_Bench_Module__id__C;
}

/* Module_hasMask */
static inline xdc_Bool acme_utils_Bench_Module_hasMask( void ) 
{
    return acme_utils_Bench_Module__diagsMask__C != NULL;
}

/* Module_getMask */
static inline xdc_Bits16 acme_utils_Bench_Module_getMask( void ) 
{
    return acme_utils_Bench_Module__diagsMask__C != NULL ? *acme_utils_Bench_Module__diagsMask__C : 0;
}

/* Module_setMask */
static inline xdc_Void acme_utils_Bench_Module_setMask( xdc_Bits16 mask ) 
{
    if (acme_utils_Bench_Module__diagsMask__C != NULL) *acme_utils_Bench_Module__diagsMask__C = mask;
}


/*
 * ======== C++ CLIENT WRAPPER [experimental] ========
 */

#if defined(__cplusplus)
namespace acme_utils { namespace Bench {

/* Bench::PClock */
namespace PClock {


/* Bench::PClock::TimeValue */
typedef xdc_Bits32 TimeValue;

/* Bench::PClock::getTime */
static inline acme_utils_clocks_IClock_TimeValue getTime(  )
{
    return acme_utils_Bench_PClock_getTime();
}

}

/* Bench::enableFlag */
static const CT__acme_utils_Bench_enableFlag& enableFlag = acme_utils_Bench_enableFlag__C;

/* Bench::begin */
static inline xdc_Void begin( xdc_String msg )
{
    acme_utils_Bench_begin(msg);
}

/* Bench::end */
static inline xdc_Void end(  )
{
    acme_utils_Bench_end();
}

}}
#endif /* __cplusplus */


/*
 * ======== EPILOGUE ========
 */

#ifdef acme_utils_Bench__top__
#undef __nested__
#endif

#endif /* acme_utils_Bench__include */


/*
 * ======== STATE STRUCTURES ========
 */

#if defined(__config__) || (!defined(__nested__) && defined(acme_utils_Bench__internalaccess))

#ifndef acme_utils_Bench__include_state
#define acme_utils_Bench__include_state

/* Module_State */
struct acme_utils_Bench_Module_State {
    xdc_String beginMsg;
    xdc_Int beginClock;
    xdc_Int overhead;
};

/* Module__state__V */
#ifdef __config__
extern struct acme_utils_Bench_Module_State__ acme_utils_Bench_Module__state__V;
#else
extern acme_utils_Bench_Module_State acme_utils_Bench_Module__state__V;
#endif

/* C++ wrapper */
#if defined(__cplusplus)
namespace acme_utils { namespace Bench {


/* Bench::Module_State */
struct Module_State {
    xdc_String beginMsg;
    xdc_Int beginClock;
    xdc_Int overhead;
};

/* Bench::module */
static Module_State* const module = (Module_State*)&acme_utils_Bench_Module__state__V;

}}
#endif /* __cplusplus */

#endif /* acme_utils_Bench__include_state */

#endif


/*
 * ======== PREFIX ALIASES ========
 */

#if !defined(__nested__) && !defined(acme_utils_Bench__nolocalnames)

/* module prefix */
#define Bench_Module_State acme_utils_Bench_Module_State
#define Bench_enableFlag acme_utils_Bench_enableFlag
#define Bench_begin acme_utils_Bench_begin
#define Bench_end acme_utils_Bench_end
#define Bench_Module_name acme_utils_Bench_Module_name
#define Bench_Module_id acme_utils_Bench_Module_id
#define Bench_Module_startup acme_utils_Bench_Module_startup
#define Bench_Module_startupDone acme_utils_Bench_Module_startupDone
#define Bench_Module_hasMask acme_utils_Bench_Module_hasMask
#define Bench_Module_getMask acme_utils_Bench_Module_getMask
#define Bench_Module_setMask acme_utils_Bench_Module_setMask
#define Bench_Object_heap acme_utils_Bench_Object_heap
#define Bench_Module_heap acme_utils_Bench_Module_heap

/* proxies */
#include <acme/utils/package/Bench_PClock.h>

/* C++ wrapper */
#if defined(__cplusplus) && !defined(acme_utils_Bench__INTERNAL__)
namespace Bench = acme_utils::Bench;
#endif

#endif


/*
 * ======== C++ SUPPLIER WRAPPER [experimental] ========
 */

#if defined(__cplusplus) && defined(acme_utils_Bench__INTERNAL__)
namespace Bench {
    using acme_utils::Bench::enableFlag;
    using acme_utils::Bench::Module_State;
    using acme_utils::Bench::module;
    namespace PClock = acme_utils::Bench::PClock;

/* Bench::Module_startup */
inline int Module_startup( int state );
#define DEFINE__Module_startup\
    int acme_utils_Bench_Module_startup( int state )\
    {\
        return Bench::Module_startup(state);\
    }\

/* Bench::begin */
inline xdc_Void begin( xdc_String msg );
#define DEFINE__begin\
    xdc_Void acme_utils_Bench_begin( xdc_String msg )\
    {\
        Bench::begin(msg);\
    }\

/* Bench::end */
inline xdc_Void end(  );
#define DEFINE__end\
    xdc_Void acme_utils_Bench_end( void )\
    {\
        Bench::end();\
    }\

}
#endif /* __cplusplus */
