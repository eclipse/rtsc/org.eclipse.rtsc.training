/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */
import java.util.*;
import org.mozilla.javascript.*;
import xdc.services.intern.xsr.*;
import xdc.services.spec.*;

public class acme_utils_clocks
{
    static final String VERS = "@(#) xdc-t32\n";

    static final Proto.Elm $$T_Bool = Proto.Elm.newBool();
    static final Proto.Elm $$T_Num = Proto.Elm.newNum();
    static final Proto.Elm $$T_Str = Proto.Elm.newStr();
    static final Proto.Elm $$T_Obj = Proto.Elm.newObj();

    static final Proto.Fxn $$T_Met = new Proto.Fxn(null, null, 0, -1, false);
    static final Proto.Map $$T_Map = new Proto.Map($$T_Obj);
    static final Proto.Arr $$T_Vec = new Proto.Arr($$T_Obj);

    static final XScriptO $$DEFAULT = Value.DEFAULT;
    static final Object $$UNDEF = Undefined.instance;

    static final Proto.Obj $$Package = (Proto.Obj)Global.get("$$Package");
    static final Proto.Obj $$Module = (Proto.Obj)Global.get("$$Module");
    static final Proto.Obj $$Instance = (Proto.Obj)Global.get("$$Instance");
    static final Proto.Obj $$Params = (Proto.Obj)Global.get("$$Params");

    static final Object $$objFldGet = Global.get("$$objFldGet");
    static final Object $$objFldSet = Global.get("$$objFldSet");
    static final Object $$proxyGet = Global.get("$$proxyGet");
    static final Object $$proxySet = Global.get("$$proxySet");
    static final Object $$delegGet = Global.get("$$delegGet");
    static final Object $$delegSet = Global.get("$$delegSet");

    Scriptable xdcO;
    Session ses;
    Value.Obj om;

    boolean isROV;
    boolean isCFG;

    Proto.Obj pkgP;
    Value.Obj pkgV;

    ArrayList<Object> imports = new ArrayList<Object>();
    ArrayList<Object> loggables = new ArrayList<Object>();
    ArrayList<Object> mcfgs = new ArrayList<Object>();
    ArrayList<Object> proxies = new ArrayList<Object>();
    ArrayList<Object> sizes = new ArrayList<Object>();
    ArrayList<Object> tdefs = new ArrayList<Object>();

    void $$IMPORTS()
    {
        Global.callFxn("loadPackage", xdcO, "xdc");
        Global.callFxn("loadPackage", xdcO, "xdc.corevers");
        Global.callFxn("loadPackage", xdcO, "xdc.runtime");
    }

    void $$OBJECTS()
    {
        pkgP = (Proto.Obj)om.bind("acme.utils.clocks.Package", new Proto.Obj());
        pkgV = (Value.Obj)om.bind("acme.utils.clocks", new Value.Obj("acme.utils.clocks", pkgP));
    }

    void IClock$$OBJECTS()
    {
        Proto.Obj po, spo;
        Value.Obj vo;

        po = (Proto.Obj)om.bind("acme.utils.clocks.IClock.Module", new Proto.Obj());
        vo = (Value.Obj)om.bind("acme.utils.clocks.IClock", new Value.Obj("acme.utils.clocks.IClock", po));
        pkgV.bind("IClock", vo);
        // decls 
    }

    void ClockStd$$OBJECTS()
    {
        Proto.Obj po, spo;
        Value.Obj vo;

        po = (Proto.Obj)om.bind("acme.utils.clocks.ClockStd.Module", new Proto.Obj());
        vo = (Value.Obj)om.bind("acme.utils.clocks.ClockStd", new Value.Obj("acme.utils.clocks.ClockStd", po));
        pkgV.bind("ClockStd", vo);
        // decls 
    }

    void IClock$$CONSTS()
    {
        // interface IClock
    }

    void ClockStd$$CONSTS()
    {
        // module ClockStd
        om.bind("acme.utils.clocks.ClockStd.getTime", new Extern("acme_utils_clocks_ClockStd_getTime__E", "xdc_Bits32(*)(xdc_Void)", true, false, "acme.utils.clocks.ClockStd.getTime"));
    }

    void IClock$$CREATES()
    {
        Proto.Fxn fxn;
        StringBuilder sb;

    }

    void ClockStd$$CREATES()
    {
        Proto.Fxn fxn;
        StringBuilder sb;

    }

    void IClock$$FUNCTIONS()
    {
        Proto.Fxn fxn;

    }

    void ClockStd$$FUNCTIONS()
    {
        Proto.Fxn fxn;

    }

    void IClock$$SIZES()
    {
        Proto.Str so;
        Object fxn;

    }

    void ClockStd$$SIZES()
    {
        Proto.Str so;
        Object fxn;

    }

    void IClock$$TYPES()
    {
        Scriptable cap;
        Proto.Obj po;
        Proto.Str ps;
        Proto.Typedef pt;
        Object fxn;

        po = (Proto.Obj)om.find("acme.utils.clocks.IClock.Module");
        po.init("acme.utils.clocks.IClock.Module", om.find("xdc.runtime.IModule.Module"));
                po.addFld("$hostonly", $$T_Num, 0, "r");
        if (isCFG) {
        }//isCFG
        // typedef IClock.TimeValue
        om.bind("acme.utils.clocks.IClock.TimeValue", Proto.Elm.newCNum("(xdc_Bits32)"));
    }

    void ClockStd$$TYPES()
    {
        Scriptable cap;
        Proto.Obj po;
        Proto.Str ps;
        Proto.Typedef pt;
        Object fxn;

        po = (Proto.Obj)om.find("acme.utils.clocks.ClockStd.Module");
        po.init("acme.utils.clocks.ClockStd.Module", om.find("acme.utils.clocks.IClock.Module"));
                po.addFld("$hostonly", $$T_Num, 0, "r");
        if (isCFG) {
        }//isCFG
    }

    void IClock$$ROV()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("acme.utils.clocks.IClock");
    }

    void ClockStd$$ROV()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("acme.utils.clocks.ClockStd");
    }

    void $$SINGLETONS()
    {
        pkgP.init("acme.utils.clocks.Package", (Proto.Obj)om.find("xdc.IPackage.Module"));
        pkgP.bind("$capsule", $$UNDEF);
        pkgV.init2(pkgP, "acme.utils.clocks", Value.DEFAULT, false);
        pkgV.bind("$name", "acme.utils.clocks");
        pkgV.bind("$category", "Package");
        pkgV.bind("$$qn", "acme.utils.clocks.");
        pkgV.bind("$vers", Global.newArray());
        Value.Map atmap = (Value.Map)pkgV.getv("$attr");
        atmap.seal("length");
        imports.clear();
        pkgV.bind("$imports", imports);
        StringBuilder sb = new StringBuilder();
        sb.append("var pkg = xdc.om['acme.utils.clocks'];\n");
        sb.append("if (pkg.$vers.length >= 3) {\n");
            sb.append("pkg.$vers.push(Packages.xdc.services.global.Vers.getDate(xdc.csd() + '/..'));\n");
        sb.append("}\n");
        sb.append("pkg.build.libraries = [\n");
            sb.append("'lib/acme.utils.clocks.a86GW',\n");
            sb.append("'lib/acme.utils.clocks.a64P',\n");
        sb.append("];\n");
        sb.append("pkg.build.libDesc = [\n");
            sb.append("['lib/acme.utils.clocks.a86GW', {target: 'gnu.targets.Mingw'}],\n");
            sb.append("['lib/acme.utils.clocks.a64P', {target: 'ti.targets.C64P'}],\n");
        sb.append("];\n");
        sb.append("if('suffix' in xdc.om['xdc.IPackage$$LibDesc']) {\n");
            sb.append("pkg.build.libDesc['lib/acme.utils.clocks.a86GW'].suffix = '86GW';\n");
            sb.append("pkg.build.libDesc['lib/acme.utils.clocks.a64P'].suffix = '64P';\n");
        sb.append("}\n");
        Global.eval(sb.toString());
    }

    void IClock$$SINGLETONS()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("acme.utils.clocks.IClock");
        po = (Proto.Obj)om.find("acme.utils.clocks.IClock.Module");
        vo.init2(po, "acme.utils.clocks.IClock", $$DEFAULT, false);
        vo.bind("Module", po);
        vo.bind("$category", "Interface");
        vo.bind("$capsule", $$UNDEF);
        vo.bind("$package", om.find("acme.utils.clocks"));
        tdefs.clear();
        proxies.clear();
        vo.bind("TimeValue", om.find("acme.utils.clocks.IClock.TimeValue"));
        vo.bind("$$tdefs", Global.newArray(tdefs.toArray()));
        vo.bind("$$proxies", Global.newArray(proxies.toArray()));
        ((Value.Arr)pkgV.getv("$interfaces")).add(vo);
        pkgV.bind("IClock", vo);
        ((Value.Arr)pkgV.getv("$unitNames")).add("IClock");
        vo.seal(null);
    }

    void ClockStd$$SINGLETONS()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("acme.utils.clocks.ClockStd");
        po = (Proto.Obj)om.find("acme.utils.clocks.ClockStd.Module");
        vo.init2(po, "acme.utils.clocks.ClockStd", $$DEFAULT, false);
        vo.bind("Module", po);
        vo.bind("$category", "Module");
        vo.bind("$capsule", $$UNDEF);
        vo.bind("$package", om.find("acme.utils.clocks"));
        tdefs.clear();
        proxies.clear();
        mcfgs.clear();
        mcfgs.add("Module__diagsEnabled");
        mcfgs.add("Module__diagsIncluded");
        mcfgs.add("Module__diagsMask");
        mcfgs.add("Module__gateObj");
        mcfgs.add("Module__gatePrms");
        mcfgs.add("Module__id");
        mcfgs.add("Module__loggerDefined");
        mcfgs.add("Module__loggerObj");
        mcfgs.add("Module__loggerFxn4");
        mcfgs.add("Module__loggerFxn8");
        mcfgs.add("Module__startupDoneFxn");
        mcfgs.add("Object__count");
        mcfgs.add("Object__heap");
        mcfgs.add("Object__sizeof");
        mcfgs.add("Object__table");
        vo.bind("TimeValue", om.find("acme.utils.clocks.IClock.TimeValue"));
        vo.bind("$$tdefs", Global.newArray(tdefs.toArray()));
        vo.bind("$$proxies", Global.newArray(proxies.toArray()));
        vo.bind("$$mcfgs", Global.newArray(mcfgs.toArray()));
        ((Value.Arr)pkgV.getv("$modules")).add(vo);
        ((Value.Arr)om.find("$modules")).add(vo);
        vo.bind("$$instflag", 0);
        vo.bind("$$iobjflag", 0);
        vo.bind("$$sizeflag", 1);
        vo.bind("$$dlgflag", 0);
        vo.bind("$$iflag", 1);
        vo.bind("$$romcfgs", "|");
        if (isCFG) {
            Proto.Str ps = (Proto.Str)vo.find("Module_State");
            if (ps != null) vo.bind("$object", ps.newInstance());
            vo.bind("$$meta_iobj", 1);
        }//isCFG
        vo.bind("getTime", om.find("acme.utils.clocks.ClockStd.getTime"));
        vo.bind("$$fxntab", Global.newArray("acme_utils_clocks_ClockStd_Handle__label__E", "acme_utils_clocks_ClockStd_Module__startupDone__E", "acme_utils_clocks_ClockStd_Object__create__E", "acme_utils_clocks_ClockStd_Object__delete__E", "acme_utils_clocks_ClockStd_Object__destruct__E", "acme_utils_clocks_ClockStd_Object__get__E", "acme_utils_clocks_ClockStd_Object__first__E", "acme_utils_clocks_ClockStd_Object__next__E", "acme_utils_clocks_ClockStd_Params__init__E", "acme_utils_clocks_ClockStd_Proxy__abstract__E", "acme_utils_clocks_ClockStd_Proxy__delegate__E", "acme_utils_clocks_ClockStd_getTime__E"));
        vo.bind("$$logEvtCfgs", Global.newArray());
        vo.bind("$$errorDescCfgs", Global.newArray());
        vo.bind("$$assertDescCfgs", Global.newArray());
        Value.Map atmap = (Value.Map)vo.getv("$attr");
        atmap.seal("length");
        vo.bind("MODULE_STARTUP$", 0);
        vo.bind("PROXY$", 0);
        loggables.clear();
        loggables.add(Global.newObject("name", "getTime", "entry", "", "exit", "%d"));
        vo.bind("$$loggables", loggables.toArray());
        pkgV.bind("ClockStd", vo);
        ((Value.Arr)pkgV.getv("$unitNames")).add("ClockStd");
    }

    void $$INITIALIZATION()
    {
        Value.Obj vo;

        if (isCFG) {
        }//isCFG
        Global.callFxn("module$meta$init", (Scriptable)om.find("acme.utils.clocks.ClockStd"));
        Global.callFxn("init", pkgV);
        ((Value.Obj)om.getv("acme.utils.clocks.IClock")).bless();
        ((Value.Obj)om.getv("acme.utils.clocks.ClockStd")).bless();
        ((Value.Arr)om.find("$packages")).add(pkgV);
    }

    public void exec( Scriptable xdcO, Session ses )
    {
        this.xdcO = xdcO;
        this.ses = ses;
        om = (Value.Obj)xdcO.get("om", null);

        Object o = om.geto("$name");
        String s = o instanceof String ? (String)o : null;
        isCFG = s != null && s.equals("cfg");
        isROV = s != null && s.equals("rov");

        $$IMPORTS();
        $$OBJECTS();
        IClock$$OBJECTS();
        ClockStd$$OBJECTS();
        IClock$$CONSTS();
        ClockStd$$CONSTS();
        IClock$$CREATES();
        ClockStd$$CREATES();
        IClock$$FUNCTIONS();
        ClockStd$$FUNCTIONS();
        IClock$$SIZES();
        ClockStd$$SIZES();
        IClock$$TYPES();
        ClockStd$$TYPES();
        if (isROV) {
            IClock$$ROV();
            ClockStd$$ROV();
        }//isROV
        $$SINGLETONS();
        IClock$$SINGLETONS();
        ClockStd$$SINGLETONS();
        $$INITIALIZATION();
    }
}
