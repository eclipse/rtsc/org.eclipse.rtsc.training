/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

#ifndef acme_utils_clocks__
#define acme_utils_clocks__


/*
 * ======== interface acme.utils.clocks.IClock ========
 */

typedef struct acme_utils_clocks_IClock_Fxns__ acme_utils_clocks_IClock_Fxns__;
typedef const acme_utils_clocks_IClock_Fxns__* acme_utils_clocks_IClock_Module;

/*
 * ======== module acme.utils.clocks.ClockStd ========
 */

typedef struct acme_utils_clocks_ClockStd_Fxns__ acme_utils_clocks_ClockStd_Fxns__;
typedef const acme_utils_clocks_ClockStd_Fxns__* acme_utils_clocks_ClockStd_Module;


#endif /* acme_utils_clocks__ */ 
