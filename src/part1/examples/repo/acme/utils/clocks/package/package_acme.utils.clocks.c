/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

#include <xdc/std.h>

__FAR__ char acme_utils_clocks__dummy__;

#define __xdc_PKGVERS null
#define __xdc_PKGNAME acme.utils.clocks
#define __xdc_PKGPREFIX acme_utils_clocks_

#ifdef __xdc_bld_pkg_c__
#define __stringify(a) #a
#define __local_include(a) __stringify(a)
#include __local_include(__xdc_bld_pkg_c__)
#endif

