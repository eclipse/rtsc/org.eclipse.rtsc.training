/*
 *  ======== acme/utils/IClock.xdc ========
 */

/*! Abstract clock */
interface IClock {
 
   /*! Time value type */
   typedef Bits32 TimeValue;
 
   /*! Get the current time */
   TimeValue getTime();
}
