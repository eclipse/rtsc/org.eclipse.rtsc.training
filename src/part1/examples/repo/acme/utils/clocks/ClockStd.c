/*
 *  ======== acme/utils/ClockStd.c ========
 */

#include "package/internal/ClockStd.xdc.h"
 
#include <time.h>
 
ClockStd_TimeValue ClockStd_getTime()
{
    return (ClockStd_TimeValue) clock();
}
