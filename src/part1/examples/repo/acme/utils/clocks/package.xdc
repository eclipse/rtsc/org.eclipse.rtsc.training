/*
 *  ======== acme/utils/clocks/package.xdc ========
 */

/*! Abstract clock support [RTSC Interface Primer] */
package acme.utils.clocks {
     interface IClock;
     module ClockStd;
}
 
