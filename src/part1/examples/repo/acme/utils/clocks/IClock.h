/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

/*
 * ======== GENERATED SECTIONS ========
 *     
 *     PROLOGUE
 *     INCLUDES
 *     
 *     VIRTUAL FUNCTIONS
 *     FUNCTION STUBS
 *     FUNCTION SELECTORS
 *     C++ CLIENT WRAPPER [experimental]
 *     
 *     EPILOGUE
 *     PREFIX ALIASES
 */


/*
 * ======== PROLOGUE ========
 */

#ifndef acme_utils_clocks_IClock__include
#define acme_utils_clocks_IClock__include

#ifndef __nested__
#define __nested__
#define acme_utils_clocks_IClock__top__
#endif

#ifdef __cplusplus
#define __extern extern "C"
#else
#define __extern extern
#endif

#define acme_utils_clocks_IClock___VERS 150


/*
 * ======== INCLUDES ========
 */

#include <xdc/std.h>

#include <xdc/runtime/xdc.h>
#include <xdc/runtime/Types.h>
#include <acme/utils/clocks/package/package.defs.h>

#include <xdc/runtime/IModule.h>


/*
 * ======== AUXILIARY DEFINITIONS ========
 */

/* TimeValue */
typedef xdc_Bits32 acme_utils_clocks_IClock_TimeValue;


/*
 * ======== VIRTUAL FUNCTIONS ========
 */

/* Fxns__ */
struct acme_utils_clocks_IClock_Fxns__ {
    xdc_runtime_Types_Base* __base;
    const xdc_runtime_Types_SysFxns2* __sysp;
    acme_utils_clocks_IClock_TimeValue (*getTime)(void);
    xdc_runtime_Types_SysFxns2 __sfxns;
};

/* Interface__BASE__C */
__extern const xdc_runtime_Types_Base acme_utils_clocks_IClock_Interface__BASE__C;


/*
 * ======== FUNCTION STUBS ========
 */

/* Module_id */
static inline xdc_runtime_Types_ModuleId acme_utils_clocks_IClock_Module_id( acme_utils_clocks_IClock_Module mod )
{
    return mod->__sysp->__mid;
}

/* getTime */
static inline acme_utils_clocks_IClock_TimeValue acme_utils_clocks_IClock_getTime( acme_utils_clocks_IClock_Module __inst )
{
    return __inst->getTime();
}


/*
 * ======== FUNCTION SELECTORS ========
 */

/* getTime_{FxnT,fxnP} */
typedef acme_utils_clocks_IClock_TimeValue (*acme_utils_clocks_IClock_getTime_FxnT)(void);
static inline acme_utils_clocks_IClock_getTime_FxnT acme_utils_clocks_IClock_getTime_fxnP( acme_utils_clocks_IClock_Module __inst )
{
    return (acme_utils_clocks_IClock_getTime_FxnT)__inst->getTime;
}


/*
 * ======== C++ CLIENT WRAPPER [experimental] ========
 */

#if defined(__cplusplus)
namespace acme_utils_clocks { namespace IClock {

/* IClock::TimeValue */
typedef xdc_Bits32 TimeValue;

/* IClock::Module */
class Module {
    acme_utils_clocks_IClock_Module __mod;
public:
    Module() { __mod = NULL; }
    Module( acme_utils_clocks_IClock_Module m ) { __mod = m; }
    Module& operator =( acme_utils_clocks_IClock_Module m ) { __mod = m; return *this; }
    operator acme_utils_clocks_IClock_Module() { return __mod; }

/* IClock::Module::getTime */
    acme_utils_clocks_IClock_TimeValue getTime(  )
    {
        return acme_utils_clocks_IClock_getTime(__mod);
    }
};

}}
#endif /* __cplusplus */


/*
 * ======== EPILOGUE ========
 */

#ifdef acme_utils_clocks_IClock__top__
#undef __nested__
#endif

#endif /* acme_utils_clocks_IClock__include */


/*
 * ======== PREFIX ALIASES ========
 */

#if !defined(__nested__) && !defined(acme_utils_clocks_IClock__nolocalnames)

/* module prefix */
#define IClock_Module acme_utils_clocks_IClock_Module
#define IClock_TimeValue acme_utils_clocks_IClock_TimeValue
#define IClock_getTime acme_utils_clocks_IClock_getTime
#define IClock_getTime_fxnP acme_utils_clocks_IClock_getTime_fxnP
#define IClock_getTime_FxnT acme_utils_clocks_IClock_getTime_FxnT
#define IClock_Module_name acme_utils_clocks_IClock_Module_name

/* C++ wrapper */
#if defined(__cplusplus) && !defined(acme_utils_clocks_IClock__INTERNAL__)
namespace IClock = acme_utils_clocks::IClock;
#endif

#endif
