/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

#ifndef acme_utils__
#define acme_utils__


/*
 * ======== module acme.utils.Bench ========
 */

typedef struct acme_utils_Bench_Module_State acme_utils_Bench_Module_State;

/*
 * ======== module acme.utils.Bench_PClock ========
 */

typedef struct acme_utils_Bench_PClock_Fxns__ acme_utils_Bench_PClock_Fxns__;
typedef const acme_utils_Bench_PClock_Fxns__* acme_utils_Bench_PClock_Module;


#endif /* acme_utils__ */ 
