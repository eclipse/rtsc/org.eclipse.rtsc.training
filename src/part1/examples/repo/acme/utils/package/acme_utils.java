/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */
import java.util.*;
import org.mozilla.javascript.*;
import xdc.services.intern.xsr.*;
import xdc.services.spec.*;

public class acme_utils
{
    static final String VERS = "@(#) xdc-t32\n";

    static final Proto.Elm $$T_Bool = Proto.Elm.newBool();
    static final Proto.Elm $$T_Num = Proto.Elm.newNum();
    static final Proto.Elm $$T_Str = Proto.Elm.newStr();
    static final Proto.Elm $$T_Obj = Proto.Elm.newObj();

    static final Proto.Fxn $$T_Met = new Proto.Fxn(null, null, 0, -1, false);
    static final Proto.Map $$T_Map = new Proto.Map($$T_Obj);
    static final Proto.Arr $$T_Vec = new Proto.Arr($$T_Obj);

    static final XScriptO $$DEFAULT = Value.DEFAULT;
    static final Object $$UNDEF = Undefined.instance;

    static final Proto.Obj $$Package = (Proto.Obj)Global.get("$$Package");
    static final Proto.Obj $$Module = (Proto.Obj)Global.get("$$Module");
    static final Proto.Obj $$Instance = (Proto.Obj)Global.get("$$Instance");
    static final Proto.Obj $$Params = (Proto.Obj)Global.get("$$Params");

    static final Object $$objFldGet = Global.get("$$objFldGet");
    static final Object $$objFldSet = Global.get("$$objFldSet");
    static final Object $$proxyGet = Global.get("$$proxyGet");
    static final Object $$proxySet = Global.get("$$proxySet");
    static final Object $$delegGet = Global.get("$$delegGet");
    static final Object $$delegSet = Global.get("$$delegSet");

    Scriptable xdcO;
    Session ses;
    Value.Obj om;

    boolean isROV;
    boolean isCFG;

    Proto.Obj pkgP;
    Value.Obj pkgV;

    ArrayList<Object> imports = new ArrayList<Object>();
    ArrayList<Object> loggables = new ArrayList<Object>();
    ArrayList<Object> mcfgs = new ArrayList<Object>();
    ArrayList<Object> proxies = new ArrayList<Object>();
    ArrayList<Object> sizes = new ArrayList<Object>();
    ArrayList<Object> tdefs = new ArrayList<Object>();

    void $$IMPORTS()
    {
        Global.callFxn("loadPackage", xdcO, "acme.utils.clocks");
        Global.callFxn("loadPackage", xdcO, "xdc");
        Global.callFxn("loadPackage", xdcO, "xdc.corevers");
        Global.callFxn("loadPackage", xdcO, "xdc.runtime");
    }

    void $$OBJECTS()
    {
        pkgP = (Proto.Obj)om.bind("acme.utils.Package", new Proto.Obj());
        pkgV = (Value.Obj)om.bind("acme.utils", new Value.Obj("acme.utils", pkgP));
    }

    void Bench$$OBJECTS()
    {
        Proto.Obj po, spo;
        Value.Obj vo;

        po = (Proto.Obj)om.bind("acme.utils.Bench.Module", new Proto.Obj());
        vo = (Value.Obj)om.bind("acme.utils.Bench", new Value.Obj("acme.utils.Bench", po));
        pkgV.bind("Bench", vo);
        // decls 
        spo = (Proto.Obj)om.bind("acme.utils.Bench$$Module_State", new Proto.Obj());
        om.bind("acme.utils.Bench.Module_State", new Proto.Str(spo, false));
    }

    void Bench_PClock$$OBJECTS()
    {
        Proto.Obj po, spo;
        Value.Obj vo;

        po = (Proto.Obj)om.bind("acme.utils.Bench_PClock.Module", new Proto.Obj());
        vo = (Value.Obj)om.bind("acme.utils.Bench_PClock", new Value.Obj("acme.utils.Bench_PClock", po));
        pkgV.bind("Bench_PClock", vo);
        // decls 
    }

    void Bench$$CONSTS()
    {
        // module Bench
        om.bind("acme.utils.Bench.begin", new Extern("acme_utils_Bench_begin__E", "xdc_Void(*)(xdc_String)", true, false, "acme.utils.Bench.begin"));
        om.bind("acme.utils.Bench.end", new Extern("acme_utils_Bench_end__E", "xdc_Void(*)(xdc_Void)", true, false, "acme.utils.Bench.end"));
    }

    void Bench_PClock$$CONSTS()
    {
        // module Bench_PClock
        om.bind("acme.utils.Bench_PClock.getTime", new Extern("acme_utils_Bench_PClock_getTime__E", "xdc_Bits32(*)(xdc_Void)", true, false, "acme.utils.Bench_PClock.getTime"));
    }

    void Bench$$CREATES()
    {
        Proto.Fxn fxn;
        StringBuilder sb;

    }

    void Bench_PClock$$CREATES()
    {
        Proto.Fxn fxn;
        StringBuilder sb;

    }

    void Bench$$FUNCTIONS()
    {
        Proto.Fxn fxn;

    }

    void Bench_PClock$$FUNCTIONS()
    {
        Proto.Fxn fxn;

    }

    void Bench$$SIZES()
    {
        Proto.Str so;
        Object fxn;

        so = (Proto.Str)om.find("acme.utils.Bench.Module_State");
        sizes.clear();
        sizes.add(Global.newArray("beginMsg", "UPtr"));
        sizes.add(Global.newArray("beginClock", "TInt"));
        sizes.add(Global.newArray("overhead", "TInt"));
        so.bind("$$sizes", Global.newArray(sizes.toArray()));
        fxn = Global.eval("function() { return $$sizeof(xdc.om['acme.utils.Bench.Module_State']); }");
        so.bind("$sizeof", fxn);
        fxn = Global.eval("function() { return $$alignof(xdc.om['acme.utils.Bench.Module_State']); }");
        so.bind("$alignof", fxn);
        fxn = Global.eval("function(fld) { return $$offsetof(xdc.om['acme.utils.Bench.Module_State'], fld); }");
        so.bind("$offsetof", fxn);
    }

    void Bench_PClock$$SIZES()
    {
        Proto.Str so;
        Object fxn;

    }

    void Bench$$TYPES()
    {
        Scriptable cap;
        Proto.Obj po;
        Proto.Str ps;
        Proto.Typedef pt;
        Object fxn;

        cap = (Scriptable)Global.callFxn("loadCapsule", xdcO, "acme/utils/Bench.xs");
        om.bind("acme.utils.Bench$$capsule", cap);
        po = (Proto.Obj)om.find("acme.utils.Bench.Module");
        po.init("acme.utils.Bench.Module", om.find("xdc.runtime.IModule.Module"));
                po.addFld("$hostonly", $$T_Num, 0, "r");
        if (isCFG) {
                        po.addFldV("PClock", (Proto)om.find("acme.utils.clocks.IClock.Module"), null, "wh", $$delegGet, $$delegSet);
                        po.addFld("enableFlag", $$T_Bool, true, "w");
        }//isCFG
                fxn = Global.get(cap, "module$use");
                if (fxn != null) om.bind("acme.utils.Bench$$module$use", true);
                if (fxn != null) po.addFxn("module$use", $$T_Met, fxn);
                fxn = Global.get(cap, "module$meta$init");
                if (fxn != null) om.bind("acme.utils.Bench$$module$meta$init", true);
                if (fxn != null) po.addFxn("module$meta$init", $$T_Met, fxn);
                fxn = Global.get(cap, "module$static$init");
                if (fxn != null) om.bind("acme.utils.Bench$$module$static$init", true);
                if (fxn != null) po.addFxn("module$static$init", $$T_Met, fxn);
                fxn = Global.get(cap, "module$validate");
                if (fxn != null) om.bind("acme.utils.Bench$$module$validate", true);
                if (fxn != null) po.addFxn("module$validate", $$T_Met, fxn);
        // struct Bench.Module_State
        po = (Proto.Obj)om.find("acme.utils.Bench$$Module_State");
        po.init("acme.utils.Bench.Module_State", null);
                po.addFld("$hostonly", $$T_Num, 0, "r");
                po.addFld("beginMsg", $$T_Str, $$UNDEF, "w");
                po.addFld("beginClock", Proto.Elm.newCNum("(xdc_Int)"), $$UNDEF, "w");
                po.addFld("overhead", Proto.Elm.newCNum("(xdc_Int)"), $$UNDEF, "w");
    }

    void Bench_PClock$$TYPES()
    {
        Scriptable cap;
        Proto.Obj po;
        Proto.Str ps;
        Proto.Typedef pt;
        Object fxn;

        po = (Proto.Obj)om.find("acme.utils.Bench_PClock.Module");
        po.init("acme.utils.Bench_PClock.Module", om.find("acme.utils.clocks.IClock.Module"));
                po.addFld("delegate$", (Proto)om.find("acme.utils.clocks.IClock.Module"), null, "wh");
                po.addFld("$hostonly", $$T_Num, 0, "r");
        if (isCFG) {
        }//isCFG
    }

    void Bench$$ROV()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("acme.utils.Bench");
        vo.bind("Module_State$fetchDesc", Global.newObject("type", "acme.utils.Bench.Module_State", "isScalar", false));
        po = (Proto.Obj)om.find("acme.utils.Bench$$Module_State");
    }

    void Bench_PClock$$ROV()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("acme.utils.Bench_PClock");
    }

    void $$SINGLETONS()
    {
        pkgP.init("acme.utils.Package", (Proto.Obj)om.find("xdc.IPackage.Module"));
        pkgP.bind("$capsule", $$UNDEF);
        pkgV.init2(pkgP, "acme.utils", Value.DEFAULT, false);
        pkgV.bind("$name", "acme.utils");
        pkgV.bind("$category", "Package");
        pkgV.bind("$$qn", "acme.utils.");
        pkgV.bind("$vers", Global.newArray());
        Value.Map atmap = (Value.Map)pkgV.getv("$attr");
        atmap.seal("length");
        imports.clear();
        imports.add(Global.newArray("acme.utils.clocks", Global.newArray()));
        pkgV.bind("$imports", imports);
        StringBuilder sb = new StringBuilder();
        sb.append("var pkg = xdc.om['acme.utils'];\n");
        sb.append("if (pkg.$vers.length >= 3) {\n");
            sb.append("pkg.$vers.push(Packages.xdc.services.global.Vers.getDate(xdc.csd() + '/..'));\n");
        sb.append("}\n");
        sb.append("pkg.build.libraries = [\n");
            sb.append("'lib/acme.utils.a64P',\n");
            sb.append("'lib/acme.utils.a86GW',\n");
        sb.append("];\n");
        sb.append("pkg.build.libDesc = [\n");
            sb.append("['lib/acme.utils.a64P', {target: 'ti.targets.C64P'}],\n");
            sb.append("['lib/acme.utils.a86GW', {target: 'gnu.targets.Mingw'}],\n");
        sb.append("];\n");
        sb.append("if('suffix' in xdc.om['xdc.IPackage$$LibDesc']) {\n");
            sb.append("pkg.build.libDesc['lib/acme.utils.a64P'].suffix = '64P';\n");
            sb.append("pkg.build.libDesc['lib/acme.utils.a86GW'].suffix = '86GW';\n");
        sb.append("}\n");
        Global.eval(sb.toString());
    }

    void Bench$$SINGLETONS()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("acme.utils.Bench");
        po = (Proto.Obj)om.find("acme.utils.Bench.Module");
        vo.init2(po, "acme.utils.Bench", $$DEFAULT, false);
        vo.bind("Module", po);
        vo.bind("$category", "Module");
        vo.bind("$capsule", om.find("acme.utils.Bench$$capsule"));
        vo.bind("$package", om.find("acme.utils"));
        tdefs.clear();
        proxies.clear();
        mcfgs.clear();
        mcfgs.add("Module__diagsEnabled");
        mcfgs.add("Module__diagsIncluded");
        mcfgs.add("Module__diagsMask");
        mcfgs.add("Module__gateObj");
        mcfgs.add("Module__gatePrms");
        mcfgs.add("Module__id");
        mcfgs.add("Module__loggerDefined");
        mcfgs.add("Module__loggerObj");
        mcfgs.add("Module__loggerFxn4");
        mcfgs.add("Module__loggerFxn8");
        mcfgs.add("Module__startupDoneFxn");
        mcfgs.add("Object__count");
        mcfgs.add("Object__heap");
        mcfgs.add("Object__sizeof");
        mcfgs.add("Object__table");
        vo.bind("PClock$proxy", om.find("acme.utils.Bench_PClock"));
        proxies.add("PClock");
        mcfgs.add("enableFlag");
        vo.bind("Module_State", om.find("acme.utils.Bench.Module_State"));
        tdefs.add(om.find("acme.utils.Bench.Module_State"));
        vo.bind("$$tdefs", Global.newArray(tdefs.toArray()));
        vo.bind("$$proxies", Global.newArray(proxies.toArray()));
        vo.bind("$$mcfgs", Global.newArray(mcfgs.toArray()));
        ((Value.Arr)pkgV.getv("$modules")).add(vo);
        ((Value.Arr)om.find("$modules")).add(vo);
        vo.bind("$$instflag", 0);
        vo.bind("$$iobjflag", 0);
        vo.bind("$$sizeflag", 1);
        vo.bind("$$dlgflag", 0);
        vo.bind("$$iflag", 0);
        vo.bind("$$romcfgs", "|");
        if (isCFG) {
            Proto.Str ps = (Proto.Str)vo.find("Module_State");
            if (ps != null) vo.bind("$object", ps.newInstance());
            vo.bind("$$meta_iobj", 1);
        }//isCFG
        vo.bind("begin", om.find("acme.utils.Bench.begin"));
        vo.bind("end", om.find("acme.utils.Bench.end"));
        vo.bind("$$fxntab", Global.newArray("acme_utils_Bench_Handle__label__E", "acme_utils_Bench_Module__startupDone__E", "acme_utils_Bench_Object__create__E", "acme_utils_Bench_Object__delete__E", "acme_utils_Bench_Object__destruct__E", "acme_utils_Bench_Object__get__E", "acme_utils_Bench_Object__first__E", "acme_utils_Bench_Object__next__E", "acme_utils_Bench_Params__init__E", "acme_utils_Bench_Proxy__abstract__E", "acme_utils_Bench_Proxy__delegate__E", "acme_utils_Bench_begin__E", "acme_utils_Bench_end__E"));
        vo.bind("$$logEvtCfgs", Global.newArray());
        vo.bind("$$errorDescCfgs", Global.newArray());
        vo.bind("$$assertDescCfgs", Global.newArray());
        Value.Map atmap = (Value.Map)vo.getv("$attr");
        atmap.setElem("", true);
        atmap.seal("length");
        vo.bind("MODULE_STARTUP$", 1);
        vo.bind("PROXY$", 0);
        loggables.clear();
        loggables.add(Global.newObject("name", "begin", "entry", "\"%s\"", "exit", ""));
        loggables.add(Global.newObject("name", "end", "entry", "", "exit", ""));
        vo.bind("$$loggables", loggables.toArray());
        pkgV.bind("Bench", vo);
        ((Value.Arr)pkgV.getv("$unitNames")).add("Bench");
    }

    void Bench_PClock$$SINGLETONS()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("acme.utils.Bench_PClock");
        po = (Proto.Obj)om.find("acme.utils.Bench_PClock.Module");
        vo.init2(po, "acme.utils.Bench_PClock", $$DEFAULT, false);
        vo.bind("Module", po);
        vo.bind("$category", "Module");
        vo.bind("$capsule", $$UNDEF);
        vo.bind("$package", om.find("acme.utils"));
        tdefs.clear();
        proxies.clear();
        proxies.add("delegate$");
        vo.bind("TimeValue", om.find("acme.utils.clocks.IClock.TimeValue"));
        vo.bind("$$tdefs", Global.newArray(tdefs.toArray()));
        vo.bind("$$proxies", Global.newArray(proxies.toArray()));
        ((Value.Arr)pkgV.getv("$modules")).add(vo);
        ((Value.Arr)om.find("$modules")).add(vo);
        vo.bind("$$instflag", 0);
        vo.bind("$$iobjflag", 0);
        vo.bind("$$sizeflag", 0);
        vo.bind("$$dlgflag", 0);
        vo.bind("$$iflag", 1);
        vo.bind("$$romcfgs", "|");
        if (isCFG) {
            Proto.Str ps = (Proto.Str)vo.find("Module_State");
            if (ps != null) vo.bind("$object", ps.newInstance());
            vo.bind("$$meta_iobj", 1);
        }//isCFG
        vo.bind("getTime", om.find("acme.utils.Bench_PClock.getTime"));
        vo.bind("$$fxntab", Global.newArray("acme_utils_Bench_PClock_DELEGATE__Handle__label", "acme_utils_Bench_PClock_DELEGATE__Module__startupDone", "acme_utils_Bench_PClock_DELEGATE__Object__create", "acme_utils_Bench_PClock_DELEGATE__Object__delete", "acme_utils_Bench_PClock_DELEGATE__Object__destruct", "acme_utils_Bench_PClock_DELEGATE__Object__get", "acme_utils_Bench_PClock_DELEGATE__Object__first", "acme_utils_Bench_PClock_DELEGATE__Object__next", "acme_utils_Bench_PClock_DELEGATE__Params__init", "acme_utils_Bench_PClock_DELEGATE__Proxy__abstract", "acme_utils_Bench_PClock_DELEGATE__Proxy__delegate", "acme_utils_Bench_PClock_DELEGATE__getTime"));
        vo.bind("$$logEvtCfgs", Global.newArray());
        vo.bind("$$errorDescCfgs", Global.newArray());
        vo.bind("$$assertDescCfgs", Global.newArray());
        Value.Map atmap = (Value.Map)vo.getv("$attr");
        atmap.seal("length");
        vo.bind("MODULE_STARTUP$", 0);
        vo.bind("PROXY$", 1);
        loggables.clear();
        loggables.add(Global.newObject("name", "getTime", "entry", "", "exit", "%d"));
        vo.bind("$$loggables", loggables.toArray());
        pkgV.bind("Bench_PClock", vo);
        ((Value.Arr)pkgV.getv("$unitNames")).add("Bench_PClock");
    }

    void $$INITIALIZATION()
    {
        Value.Obj vo;

        if (isCFG) {
        }//isCFG
        Global.callFxn("module$meta$init", (Scriptable)om.find("acme.utils.Bench"));
        Global.callFxn("module$meta$init", (Scriptable)om.find("acme.utils.Bench_PClock"));
        Global.callFxn("init", pkgV);
        ((Value.Obj)om.getv("acme.utils.Bench")).bless();
        ((Value.Obj)om.getv("acme.utils.Bench_PClock")).bless();
        ((Value.Arr)om.find("$packages")).add(pkgV);
    }

    public void exec( Scriptable xdcO, Session ses )
    {
        this.xdcO = xdcO;
        this.ses = ses;
        om = (Value.Obj)xdcO.get("om", null);

        Object o = om.geto("$name");
        String s = o instanceof String ? (String)o : null;
        isCFG = s != null && s.equals("cfg");
        isROV = s != null && s.equals("rov");

        $$IMPORTS();
        $$OBJECTS();
        Bench$$OBJECTS();
        Bench_PClock$$OBJECTS();
        Bench$$CONSTS();
        Bench_PClock$$CONSTS();
        Bench$$CREATES();
        Bench_PClock$$CREATES();
        Bench$$FUNCTIONS();
        Bench_PClock$$FUNCTIONS();
        Bench$$SIZES();
        Bench_PClock$$SIZES();
        Bench$$TYPES();
        Bench_PClock$$TYPES();
        if (isROV) {
            Bench$$ROV();
            Bench_PClock$$ROV();
        }//isROV
        $$SINGLETONS();
        Bench$$SINGLETONS();
        Bench_PClock$$SINGLETONS();
        $$INITIALIZATION();
    }
}
