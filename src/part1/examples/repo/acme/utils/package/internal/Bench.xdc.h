/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

#ifndef acme_utils_Bench__INTERNAL__
#define acme_utils_Bench__INTERNAL__

#ifndef acme_utils_Bench__internalaccess
#define acme_utils_Bench__internalaccess
#endif

#include <acme/utils/Bench.h>

#undef xdc_FILE__
#ifndef xdc_FILE
#define xdc_FILE__ NULL
#else
#define xdc_FILE__ xdc_FILE
#endif

/* begin */
#undef acme_utils_Bench_begin
#define acme_utils_Bench_begin acme_utils_Bench_begin__F

/* end */
#undef acme_utils_Bench_end
#define acme_utils_Bench_end acme_utils_Bench_end__F

/* Module_startup */
#undef acme_utils_Bench_Module_startup
#define acme_utils_Bench_Module_startup acme_utils_Bench_Module_startup__F

/* Instance_init */
#undef acme_utils_Bench_Instance_init
#define acme_utils_Bench_Instance_init acme_utils_Bench_Instance_init__F

/* Instance_finalize */
#undef acme_utils_Bench_Instance_finalize
#define acme_utils_Bench_Instance_finalize acme_utils_Bench_Instance_finalize__F

/* module */
#define Bench_module ((acme_utils_Bench_Module_State *)(xdc__MODOBJADDR__(acme_utils_Bench_Module__state__V)))
#if !defined(__cplusplus) || !defined(acme_utils_Bench__cplusplus)
#define module ((acme_utils_Bench_Module_State *)(xdc__MODOBJADDR__(acme_utils_Bench_Module__state__V)))
#endif
/* per-module runtime symbols */
#undef Module__MID
#define Module__MID acme_utils_Bench_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL acme_utils_Bench_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB acme_utils_Bench_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK acme_utils_Bench_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF acme_utils_Bench_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ acme_utils_Bench_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 acme_utils_Bench_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 acme_utils_Bench_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ acme_utils_Bench_Module__gateObj__C
#undef Module__G_PRMS
#define Module__G_PRMS acme_utils_Bench_Module__gatePrms__C
#undef Module__GP_create
#define Module__GP_create acme_utils_Bench_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete acme_utils_Bench_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter acme_utils_Bench_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave acme_utils_Bench_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query acme_utils_Bench_Module_GateProxy_query


#endif /* acme_utils_Bench__INTERNAL____ */
