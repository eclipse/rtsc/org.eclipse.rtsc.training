/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

#ifndef acme_utils_Bench_PClock__INTERNAL__
#define acme_utils_Bench_PClock__INTERNAL__

#ifndef acme_utils_Bench_PClock__internalaccess
#define acme_utils_Bench_PClock__internalaccess
#endif

#include <acme/utils/Bench_PClock.h>

#undef xdc_FILE__
#ifndef xdc_FILE
#define xdc_FILE__ NULL
#else
#define xdc_FILE__ xdc_FILE
#endif

/* getTime */
#undef acme_utils_Bench_PClock_getTime
#define acme_utils_Bench_PClock_getTime acme_utils_Bench_PClock_getTime__F

/* Module_startup */
#undef acme_utils_Bench_PClock_Module_startup
#define acme_utils_Bench_PClock_Module_startup acme_utils_Bench_PClock_Module_startup__F

/* Instance_init */
#undef acme_utils_Bench_PClock_Instance_init
#define acme_utils_Bench_PClock_Instance_init acme_utils_Bench_PClock_Instance_init__F

/* Instance_finalize */
#undef acme_utils_Bench_PClock_Instance_finalize
#define acme_utils_Bench_PClock_Instance_finalize acme_utils_Bench_PClock_Instance_finalize__F

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID acme_utils_Bench_PClock_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL acme_utils_Bench_PClock_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB acme_utils_Bench_PClock_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK acme_utils_Bench_PClock_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF acme_utils_Bench_PClock_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ acme_utils_Bench_PClock_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 acme_utils_Bench_PClock_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 acme_utils_Bench_PClock_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ acme_utils_Bench_PClock_Module__gateObj__C
#undef Module__G_PRMS
#define Module__G_PRMS acme_utils_Bench_PClock_Module__gatePrms__C
#undef Module__GP_create
#define Module__GP_create acme_utils_Bench_PClock_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete acme_utils_Bench_PClock_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter acme_utils_Bench_PClock_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave acme_utils_Bench_PClock_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query acme_utils_Bench_PClock_Module_GateProxy_query


#endif /* acme_utils_Bench_PClock__INTERNAL____ */
