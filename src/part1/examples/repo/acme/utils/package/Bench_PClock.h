/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

/*
 * ======== GENERATED SECTIONS ========
 *     
 *     PROLOGUE
 *     INCLUDES
 *     
 *     MODULE-WIDE CONFIGS
 *     VIRTUAL FUNCTIONS
 *     FUNCTION DECLARATIONS
 *     CONVERTORS
 *     SYSTEM FUNCTIONS
 *     C++ CLIENT WRAPPER [experimental]
 *     
 *     EPILOGUE
 *     PREFIX ALIASES
 */


/*
 * ======== PROLOGUE ========
 */

#ifndef acme_utils_Bench_PClock__include
#define acme_utils_Bench_PClock__include

#ifndef __nested__
#define __nested__
#define acme_utils_Bench_PClock__top__
#endif

#ifdef __cplusplus
#define __extern extern "C"
#else
#define __extern extern
#endif

#define acme_utils_Bench_PClock___VERS 150


/*
 * ======== INCLUDES ========
 */

#include <xdc/std.h>

#include <xdc/runtime/xdc.h>
#include <xdc/runtime/Types.h>
#include <acme/utils/package/package.defs.h>

#include <acme/utils/clocks/IClock.h>


/*
 * ======== AUXILIARY DEFINITIONS ========
 */

/* TimeValue */
typedef acme_utils_clocks_IClock_TimeValue acme_utils_Bench_PClock_TimeValue;


/*
 * ======== MODULE-WIDE CONFIGS ========
 */

/* Module__diagsEnabled */
typedef xdc_Bits32 CT__acme_utils_Bench_PClock_Module__diagsEnabled;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__diagsEnabled acme_utils_Bench_PClock_Module__diagsEnabled__C;

/* Module__diagsIncluded */
typedef xdc_Bits32 CT__acme_utils_Bench_PClock_Module__diagsIncluded;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__diagsIncluded acme_utils_Bench_PClock_Module__diagsIncluded__C;

/* Module__diagsMask */
typedef xdc_Bits16* CT__acme_utils_Bench_PClock_Module__diagsMask;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__diagsMask acme_utils_Bench_PClock_Module__diagsMask__C;

/* Module__gateObj */
typedef xdc_Ptr CT__acme_utils_Bench_PClock_Module__gateObj;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__gateObj acme_utils_Bench_PClock_Module__gateObj__C;

/* Module__gatePrms */
typedef xdc_Ptr CT__acme_utils_Bench_PClock_Module__gatePrms;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__gatePrms acme_utils_Bench_PClock_Module__gatePrms__C;

/* Module__id */
typedef xdc_runtime_Types_ModuleId CT__acme_utils_Bench_PClock_Module__id;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__id acme_utils_Bench_PClock_Module__id__C;

/* Module__loggerDefined */
typedef xdc_Bool CT__acme_utils_Bench_PClock_Module__loggerDefined;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__loggerDefined acme_utils_Bench_PClock_Module__loggerDefined__C;

/* Module__loggerObj */
typedef xdc_Ptr CT__acme_utils_Bench_PClock_Module__loggerObj;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__loggerObj acme_utils_Bench_PClock_Module__loggerObj__C;

/* Module__loggerFxn4 */
typedef xdc_runtime_Types_LoggerFxn4 CT__acme_utils_Bench_PClock_Module__loggerFxn4;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__loggerFxn4 acme_utils_Bench_PClock_Module__loggerFxn4__C;

/* Module__loggerFxn8 */
typedef xdc_runtime_Types_LoggerFxn8 CT__acme_utils_Bench_PClock_Module__loggerFxn8;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__loggerFxn8 acme_utils_Bench_PClock_Module__loggerFxn8__C;

/* Module__startupDoneFxn */
typedef xdc_Bool (*CT__acme_utils_Bench_PClock_Module__startupDoneFxn)(void);
__extern __FAR__ const CT__acme_utils_Bench_PClock_Module__startupDoneFxn acme_utils_Bench_PClock_Module__startupDoneFxn__C;

/* Object__count */
typedef xdc_Int CT__acme_utils_Bench_PClock_Object__count;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Object__count acme_utils_Bench_PClock_Object__count__C;

/* Object__heap */
typedef xdc_runtime_IHeap_Handle CT__acme_utils_Bench_PClock_Object__heap;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Object__heap acme_utils_Bench_PClock_Object__heap__C;

/* Object__sizeof */
typedef xdc_SizeT CT__acme_utils_Bench_PClock_Object__sizeof;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Object__sizeof acme_utils_Bench_PClock_Object__sizeof__C;

/* Object__table */
typedef xdc_Ptr CT__acme_utils_Bench_PClock_Object__table;
__extern __FAR__ const CT__acme_utils_Bench_PClock_Object__table acme_utils_Bench_PClock_Object__table__C;


/*
 * ======== VIRTUAL FUNCTIONS ========
 */

/* Fxns__ */
struct acme_utils_Bench_PClock_Fxns__ {
    xdc_runtime_Types_Base* __base;
    const xdc_runtime_Types_SysFxns2* __sysp;
    acme_utils_clocks_IClock_TimeValue (*getTime)(void);
    xdc_runtime_Types_SysFxns2 __sfxns;
};

/* Module__FXNS__C */
__extern const acme_utils_Bench_PClock_Fxns__ acme_utils_Bench_PClock_Module__FXNS__C;


/*
 * ======== FUNCTION DECLARATIONS ========
 */

/* Module_startup */
#define acme_utils_Bench_PClock_Module_startup( state ) -1

/* Handle__label__S */
xdc__CODESECT(acme_utils_Bench_PClock_Handle__label__S, "acme_utils_Bench_PClock_Handle__label")
__extern xdc_runtime_Types_Label* acme_utils_Bench_PClock_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab );

/* Module__startupDone__S */
xdc__CODESECT(acme_utils_Bench_PClock_Module__startupDone__S, "acme_utils_Bench_PClock_Module__startupDone")
__extern xdc_Bool acme_utils_Bench_PClock_Module__startupDone__S( void );

/* Object__create__S */
xdc__CODESECT(acme_utils_Bench_PClock_Object__create__S, "acme_utils_Bench_PClock_Object__create")
__extern xdc_Ptr acme_utils_Bench_PClock_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb );

/* Object__delete__S */
xdc__CODESECT(acme_utils_Bench_PClock_Object__delete__S, "acme_utils_Bench_PClock_Object__delete")
__extern xdc_Void acme_utils_Bench_PClock_Object__delete__S( xdc_Ptr instp );

/* Object__destruct__S */
xdc__CODESECT(acme_utils_Bench_PClock_Object__destruct__S, "acme_utils_Bench_PClock_Object__destruct")
__extern xdc_Void acme_utils_Bench_PClock_Object__destruct__S( xdc_Ptr objp );

/* Object__get__S */
xdc__CODESECT(acme_utils_Bench_PClock_Object__get__S, "acme_utils_Bench_PClock_Object__get")
__extern xdc_Ptr acme_utils_Bench_PClock_Object__get__S( xdc_Ptr oarr, xdc_Int i );

/* Object__first__S */
xdc__CODESECT(acme_utils_Bench_PClock_Object__first__S, "acme_utils_Bench_PClock_Object__first")
__extern xdc_Ptr acme_utils_Bench_PClock_Object__first__S( void );

/* Object__next__S */
xdc__CODESECT(acme_utils_Bench_PClock_Object__next__S, "acme_utils_Bench_PClock_Object__next")
__extern xdc_Ptr acme_utils_Bench_PClock_Object__next__S( xdc_Ptr obj );

/* Params__init__S */
xdc__CODESECT(acme_utils_Bench_PClock_Params__init__S, "acme_utils_Bench_PClock_Params__init")
__extern xdc_Void acme_utils_Bench_PClock_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz );

/* Proxy__abstract__S */
xdc__CODESECT(acme_utils_Bench_PClock_Proxy__abstract__S, "acme_utils_Bench_PClock_Proxy__abstract")
__extern xdc_Bool acme_utils_Bench_PClock_Proxy__abstract__S( void );

/* Proxy__delegate__S */
xdc__CODESECT(acme_utils_Bench_PClock_Proxy__delegate__S, "acme_utils_Bench_PClock_Proxy__delegate")
__extern xdc_Ptr acme_utils_Bench_PClock_Proxy__delegate__S( void );

/* getTime__E */
#define acme_utils_Bench_PClock_getTime acme_utils_Bench_PClock_getTime__E
xdc__CODESECT(acme_utils_Bench_PClock_getTime__E, "acme_utils_Bench_PClock_getTime")
__extern acme_utils_clocks_IClock_TimeValue acme_utils_Bench_PClock_getTime__E( void );
xdc__CODESECT(acme_utils_Bench_PClock_getTime__R, "acme_utils_Bench_PClock_getTime")
__extern acme_utils_clocks_IClock_TimeValue acme_utils_Bench_PClock_getTime__R( void );


/*
 * ======== CONVERTORS ========
 */

/* Module_upCast */
static inline acme_utils_clocks_IClock_Module acme_utils_Bench_PClock_Module_upCast( void )
{
    return (acme_utils_clocks_IClock_Module)acme_utils_Bench_PClock_Proxy__delegate__S();
}

/* Module_to_acme_utils_clocks_IClock */
#define acme_utils_Bench_PClock_Module_to_acme_utils_clocks_IClock acme_utils_Bench_PClock_Module_upCast


/*
 * ======== SYSTEM FUNCTIONS ========
 */

/* Module_startupDone */
#define acme_utils_Bench_PClock_Module_startupDone() acme_utils_Bench_PClock_Module__startupDone__S()

/* Object_heap */
#define acme_utils_Bench_PClock_Object_heap() acme_utils_Bench_PClock_Object__heap__C

/* Module_heap */
#define acme_utils_Bench_PClock_Module_heap() acme_utils_Bench_PClock_Object__heap__C

/* Module_id */
static inline CT__acme_utils_Bench_PClock_Module__id acme_utils_Bench_PClock_Module_id( void ) 
{
    return acme_utils_Bench_PClock_Module__id__C;
}

/* Proxy_abstract */
#define acme_utils_Bench_PClock_Proxy_abstract() acme_utils_Bench_PClock_Proxy__abstract__S()

/* Proxy_delegate */
#define acme_utils_Bench_PClock_Proxy_delegate() ((acme_utils_clocks_IClock_Module)acme_utils_Bench_PClock_Proxy__delegate__S())


/*
 * ======== C++ CLIENT WRAPPER [experimental] ========
 */


/*
 * ======== EPILOGUE ========
 */

#ifdef acme_utils_Bench_PClock__top__
#undef __nested__
#endif

#endif /* acme_utils_Bench_PClock__include */


/*
 * ======== PREFIX ALIASES ========
 */

#if !defined(__nested__) && !defined(acme_utils_Bench_PClock__nolocalnames)

/* module prefix */
#define Bench_PClock_TimeValue acme_utils_Bench_PClock_TimeValue
#define Bench_PClock_getTime acme_utils_Bench_PClock_getTime
#define Bench_PClock_Module_name acme_utils_Bench_PClock_Module_name
#define Bench_PClock_Module_id acme_utils_Bench_PClock_Module_id
#define Bench_PClock_Module_startup acme_utils_Bench_PClock_Module_startup
#define Bench_PClock_Module_startupDone acme_utils_Bench_PClock_Module_startupDone
#define Bench_PClock_Module_hasMask acme_utils_Bench_PClock_Module_hasMask
#define Bench_PClock_Module_getMask acme_utils_Bench_PClock_Module_getMask
#define Bench_PClock_Module_setMask acme_utils_Bench_PClock_Module_setMask
#define Bench_PClock_Object_heap acme_utils_Bench_PClock_Object_heap
#define Bench_PClock_Module_heap acme_utils_Bench_PClock_Module_heap
#define Bench_PClock_Proxy_abstract acme_utils_Bench_PClock_Proxy_abstract
#define Bench_PClock_Proxy_delegate acme_utils_Bench_PClock_Proxy_delegate
#define Bench_PClock_Module_upCast acme_utils_Bench_PClock_Module_upCast
#define Bench_PClock_Module_to_acme_utils_clocks_IClock acme_utils_Bench_PClock_Module_to_acme_utils_clocks_IClock

#endif
