/*
 *  ======== acme/utils/Bench.xs ========
 */

function module$use()
{
    xdc.useModule('xdc.runtime.Startup');
    xdc.useModule('xdc.runtime.System');
 
    var Bench = xdc.useModule('acme.utils.Bench');

    if (Bench.PClock == null) {
        var ClockStd = xdc.useModule('acme.utils.clocks.ClockStd');
        Bench.PClock = ClockStd;
    }
}
 
function module$static$init(obj, params)
{
    obj.beginMsg = null;
    obj.beginClock = 0;
    obj.overhead = 0;
}
