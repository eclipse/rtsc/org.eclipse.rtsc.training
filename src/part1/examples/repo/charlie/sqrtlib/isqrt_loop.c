unsigned int isqrt( unsigned long val ) {
    unsigned long temp, g = 0, b = 0x8000, bshft = 15;
    do {
        if (val >= (temp = (((g << 1) + b) << bshft--))) {
           g += b;
           val -= temp;
        }
    } while (b >>= 1);
    return g;
}
