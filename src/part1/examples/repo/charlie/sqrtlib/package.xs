/*
 *  ======== charlie/sqrtlib/package.xs ========
 */

function getLibs( prog )
{
    var pkg = this;
    var Settings = this.Settings;
    
    var suffix = prog.build.target.findSuffix(pkg);
    var name;

    switch (Settings.optimize) {
        case Settings.OPTIMIZE_FOR_SPACE:
            name = "isqrt_loop";
            break;
        case Settings.OPTIMIZE_FOR_TIME:
            name = "isqrt_unroll";
            break;
    }
 
    if (suffix && name) {
        return "lib/" + name + ".a" + suffix;
    }
    else {
        return null;
    }
}
