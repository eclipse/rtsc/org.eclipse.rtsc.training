/*
 *  ======== charlie/sqrtlib/Settings.xdc ========
 */

metaonly module Settings {

    enum OptType {
        OPTIMIZE_FOR_SPACE,
        OPTIMIZE_FOR_TIME
    };

    config OptType optimize = OPTIMIZE_FOR_TIME;
}
