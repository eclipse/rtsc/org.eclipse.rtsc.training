/*
 *  ======== charlie/sqrtlib/test/isqrtTest1.c ========
 */

#include <acme/utils/Bench.h>
#include <charlie/sqrtlib/isqrt.h>

#include <xdc/runtime/System.h>
 
#include <stdlib.h>

int main( int argc, char* argv[] )
{
    unsigned int x, y;

    x = argc > 0 ? atoi(argv[1]) : 0;
    System_printf("==> isqrt(%d)\n", x);

    Bench_begin("cycle count:");
    y = isqrt(x);
    Bench_end();

    System_printf("<== %d\n", y);
    return 0;
}
