/*
 *  ======== charlie/sqrtlib/test/package.xdc ========
 */

requires acme.utils;
requires charlie.sqrtlib;
 
/*! Companion tests for charlie.sqrtlib [RTSC Packaging Primer] */
package charlie.sqrtlib.test {    
    /* no modules */	
};
