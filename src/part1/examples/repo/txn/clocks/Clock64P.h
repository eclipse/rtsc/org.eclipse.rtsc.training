/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

/*
 * ======== GENERATED SECTIONS ========
 *     
 *     PROLOGUE
 *     INCLUDES
 *     
 *     INTERNAL DEFINITIONS
 *     MODULE-WIDE CONFIGS
 *     VIRTUAL FUNCTIONS
 *     FUNCTION DECLARATIONS
 *     CONVERTORS
 *     SYSTEM FUNCTIONS
 *     C++ CLIENT WRAPPER [experimental]
 *     
 *     EPILOGUE
 *     STATE STRUCTURES
 *     PREFIX ALIASES
 *     C++ SUPPLIER WRAPPER [experimental]
 */


/*
 * ======== PROLOGUE ========
 */

#ifndef txn_clocks_Clock64P__include
#define txn_clocks_Clock64P__include

#ifndef __nested__
#define __nested__
#define txn_clocks_Clock64P__top__
#endif

#ifdef __cplusplus
#define __extern extern "C"
#else
#define __extern extern
#endif

#define txn_clocks_Clock64P___VERS 150


/*
 * ======== INCLUDES ========
 */

#include <xdc/std.h>

#include <xdc/runtime/xdc.h>
#include <xdc/runtime/Types.h>
#include <txn/clocks/package/package.defs.h>

#include <acme/utils/clocks/IClock.h>


/*
 * ======== AUXILIARY DEFINITIONS ========
 */

/* TimeValue */
typedef acme_utils_clocks_IClock_TimeValue txn_clocks_Clock64P_TimeValue;


/*
 * ======== INTERNAL DEFINITIONS ========
 */


/*
 * ======== MODULE-WIDE CONFIGS ========
 */

/* Module__diagsEnabled */
typedef xdc_Bits32 CT__txn_clocks_Clock64P_Module__diagsEnabled;
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__diagsEnabled txn_clocks_Clock64P_Module__diagsEnabled__C;

/* Module__diagsIncluded */
typedef xdc_Bits32 CT__txn_clocks_Clock64P_Module__diagsIncluded;
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__diagsIncluded txn_clocks_Clock64P_Module__diagsIncluded__C;

/* Module__diagsMask */
typedef xdc_Bits16* CT__txn_clocks_Clock64P_Module__diagsMask;
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__diagsMask txn_clocks_Clock64P_Module__diagsMask__C;

/* Module__gateObj */
typedef xdc_Ptr CT__txn_clocks_Clock64P_Module__gateObj;
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__gateObj txn_clocks_Clock64P_Module__gateObj__C;

/* Module__gatePrms */
typedef xdc_Ptr CT__txn_clocks_Clock64P_Module__gatePrms;
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__gatePrms txn_clocks_Clock64P_Module__gatePrms__C;

/* Module__id */
typedef xdc_runtime_Types_ModuleId CT__txn_clocks_Clock64P_Module__id;
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__id txn_clocks_Clock64P_Module__id__C;

/* Module__loggerDefined */
typedef xdc_Bool CT__txn_clocks_Clock64P_Module__loggerDefined;
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__loggerDefined txn_clocks_Clock64P_Module__loggerDefined__C;

/* Module__loggerObj */
typedef xdc_Ptr CT__txn_clocks_Clock64P_Module__loggerObj;
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__loggerObj txn_clocks_Clock64P_Module__loggerObj__C;

/* Module__loggerFxn4 */
typedef xdc_runtime_Types_LoggerFxn4 CT__txn_clocks_Clock64P_Module__loggerFxn4;
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__loggerFxn4 txn_clocks_Clock64P_Module__loggerFxn4__C;

/* Module__loggerFxn8 */
typedef xdc_runtime_Types_LoggerFxn8 CT__txn_clocks_Clock64P_Module__loggerFxn8;
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__loggerFxn8 txn_clocks_Clock64P_Module__loggerFxn8__C;

/* Module__startupDoneFxn */
typedef xdc_Bool (*CT__txn_clocks_Clock64P_Module__startupDoneFxn)(void);
__extern __FAR__ const CT__txn_clocks_Clock64P_Module__startupDoneFxn txn_clocks_Clock64P_Module__startupDoneFxn__C;

/* Object__count */
typedef xdc_Int CT__txn_clocks_Clock64P_Object__count;
__extern __FAR__ const CT__txn_clocks_Clock64P_Object__count txn_clocks_Clock64P_Object__count__C;

/* Object__heap */
typedef xdc_runtime_IHeap_Handle CT__txn_clocks_Clock64P_Object__heap;
__extern __FAR__ const CT__txn_clocks_Clock64P_Object__heap txn_clocks_Clock64P_Object__heap__C;

/* Object__sizeof */
typedef xdc_SizeT CT__txn_clocks_Clock64P_Object__sizeof;
__extern __FAR__ const CT__txn_clocks_Clock64P_Object__sizeof txn_clocks_Clock64P_Object__sizeof__C;

/* Object__table */
typedef xdc_Ptr CT__txn_clocks_Clock64P_Object__table;
__extern __FAR__ const CT__txn_clocks_Clock64P_Object__table txn_clocks_Clock64P_Object__table__C;


/*
 * ======== VIRTUAL FUNCTIONS ========
 */

/* Fxns__ */
struct txn_clocks_Clock64P_Fxns__ {
    xdc_runtime_Types_Base* __base;
    const xdc_runtime_Types_SysFxns2* __sysp;
    acme_utils_clocks_IClock_TimeValue (*getTime)(void);
    xdc_runtime_Types_SysFxns2 __sfxns;
};

/* Module__FXNS__C */
__extern const txn_clocks_Clock64P_Fxns__ txn_clocks_Clock64P_Module__FXNS__C;


/*
 * ======== FUNCTION DECLARATIONS ========
 */

/* Module_startup */
#define txn_clocks_Clock64P_Module_startup txn_clocks_Clock64P_Module_startup__E
xdc__CODESECT(txn_clocks_Clock64P_Module_startup__E, "txn_clocks_Clock64P_Module_startup")
__extern xdc_Int txn_clocks_Clock64P_Module_startup__E( xdc_Int state );
xdc__CODESECT(txn_clocks_Clock64P_Module_startup__F, "txn_clocks_Clock64P_Module_startup")
__extern xdc_Int txn_clocks_Clock64P_Module_startup__F( xdc_Int state );
xdc__CODESECT(txn_clocks_Clock64P_Module_startup__R, "txn_clocks_Clock64P_Module_startup")
__extern xdc_Int txn_clocks_Clock64P_Module_startup__R( xdc_Int state );

/* Handle__label__S */
xdc__CODESECT(txn_clocks_Clock64P_Handle__label__S, "txn_clocks_Clock64P_Handle__label")
__extern xdc_runtime_Types_Label* txn_clocks_Clock64P_Handle__label__S( xdc_Ptr obj, xdc_runtime_Types_Label* lab );

/* Module__startupDone__S */
xdc__CODESECT(txn_clocks_Clock64P_Module__startupDone__S, "txn_clocks_Clock64P_Module__startupDone")
__extern xdc_Bool txn_clocks_Clock64P_Module__startupDone__S( void );

/* Object__create__S */
xdc__CODESECT(txn_clocks_Clock64P_Object__create__S, "txn_clocks_Clock64P_Object__create")
__extern xdc_Ptr txn_clocks_Clock64P_Object__create__S( xdc_Ptr __oa, xdc_SizeT __osz, xdc_Ptr __aa, const xdc_UChar* __pa, xdc_SizeT __psz, xdc_runtime_Error_Block* __eb );

/* Object__delete__S */
xdc__CODESECT(txn_clocks_Clock64P_Object__delete__S, "txn_clocks_Clock64P_Object__delete")
__extern xdc_Void txn_clocks_Clock64P_Object__delete__S( xdc_Ptr instp );

/* Object__destruct__S */
xdc__CODESECT(txn_clocks_Clock64P_Object__destruct__S, "txn_clocks_Clock64P_Object__destruct")
__extern xdc_Void txn_clocks_Clock64P_Object__destruct__S( xdc_Ptr objp );

/* Object__get__S */
xdc__CODESECT(txn_clocks_Clock64P_Object__get__S, "txn_clocks_Clock64P_Object__get")
__extern xdc_Ptr txn_clocks_Clock64P_Object__get__S( xdc_Ptr oarr, xdc_Int i );

/* Object__first__S */
xdc__CODESECT(txn_clocks_Clock64P_Object__first__S, "txn_clocks_Clock64P_Object__first")
__extern xdc_Ptr txn_clocks_Clock64P_Object__first__S( void );

/* Object__next__S */
xdc__CODESECT(txn_clocks_Clock64P_Object__next__S, "txn_clocks_Clock64P_Object__next")
__extern xdc_Ptr txn_clocks_Clock64P_Object__next__S( xdc_Ptr obj );

/* Params__init__S */
xdc__CODESECT(txn_clocks_Clock64P_Params__init__S, "txn_clocks_Clock64P_Params__init")
__extern xdc_Void txn_clocks_Clock64P_Params__init__S( xdc_Ptr dst, xdc_Ptr src, xdc_SizeT psz, xdc_SizeT isz );

/* Proxy__abstract__S */
xdc__CODESECT(txn_clocks_Clock64P_Proxy__abstract__S, "txn_clocks_Clock64P_Proxy__abstract")
__extern xdc_Bool txn_clocks_Clock64P_Proxy__abstract__S( void );

/* Proxy__delegate__S */
xdc__CODESECT(txn_clocks_Clock64P_Proxy__delegate__S, "txn_clocks_Clock64P_Proxy__delegate")
__extern xdc_Ptr txn_clocks_Clock64P_Proxy__delegate__S( void );

/* getTime__E */
#define txn_clocks_Clock64P_getTime txn_clocks_Clock64P_getTime__E
xdc__CODESECT(txn_clocks_Clock64P_getTime__E, "txn_clocks_Clock64P_getTime")
__extern acme_utils_clocks_IClock_TimeValue txn_clocks_Clock64P_getTime__E( void );
xdc__CODESECT(txn_clocks_Clock64P_getTime__F, "txn_clocks_Clock64P_getTime")
__extern acme_utils_clocks_IClock_TimeValue txn_clocks_Clock64P_getTime__F( void );
__extern acme_utils_clocks_IClock_TimeValue txn_clocks_Clock64P_getTime__R( void );


/*
 * ======== CONVERTORS ========
 */

/* Module_upCast */
static inline acme_utils_clocks_IClock_Module txn_clocks_Clock64P_Module_upCast( void )
{
    return (acme_utils_clocks_IClock_Module)&txn_clocks_Clock64P_Module__FXNS__C;
}

/* Module_to_acme_utils_clocks_IClock */
#define txn_clocks_Clock64P_Module_to_acme_utils_clocks_IClock txn_clocks_Clock64P_Module_upCast


/*
 * ======== SYSTEM FUNCTIONS ========
 */

/* Module_startupDone */
#define txn_clocks_Clock64P_Module_startupDone() txn_clocks_Clock64P_Module__startupDone__S()

/* Object_heap */
#define txn_clocks_Clock64P_Object_heap() txn_clocks_Clock64P_Object__heap__C

/* Module_heap */
#define txn_clocks_Clock64P_Module_heap() txn_clocks_Clock64P_Object__heap__C

/* Module_id */
static inline CT__txn_clocks_Clock64P_Module__id txn_clocks_Clock64P_Module_id( void ) 
{
    return txn_clocks_Clock64P_Module__id__C;
}

/* Module_hasMask */
static inline xdc_Bool txn_clocks_Clock64P_Module_hasMask( void ) 
{
    return txn_clocks_Clock64P_Module__diagsMask__C != NULL;
}

/* Module_getMask */
static inline xdc_Bits16 txn_clocks_Clock64P_Module_getMask( void ) 
{
    return txn_clocks_Clock64P_Module__diagsMask__C != NULL ? *txn_clocks_Clock64P_Module__diagsMask__C : 0;
}

/* Module_setMask */
static inline xdc_Void txn_clocks_Clock64P_Module_setMask( xdc_Bits16 mask ) 
{
    if (txn_clocks_Clock64P_Module__diagsMask__C != NULL) *txn_clocks_Clock64P_Module__diagsMask__C = mask;
}


/*
 * ======== C++ CLIENT WRAPPER [experimental] ========
 */

#if defined(__cplusplus)
namespace txn_clocks { namespace Clock64P {

/* Clock64P::TimeValue */
typedef xdc_Bits32 TimeValue;

/* Clock64P::getTime */
static inline acme_utils_clocks_IClock_TimeValue getTime(  )
{
    return txn_clocks_Clock64P_getTime();
}

/* Clock64P::Module */
class Module {
    txn_clocks_Clock64P_Module __mod;
public:
    Module() { __mod = &txn_clocks_Clock64P_Module__FXNS__C; }
    operator txn_clocks_Clock64P_Module() { return __mod; }
    operator acme_utils_clocks_IClock_Module() { return (acme_utils_clocks_IClock_Module)__mod; }
    operator acme_utils_clocks::IClock::Module&() { return reinterpret_cast<acme_utils_clocks::IClock::Module&>(*this); }
};

}}
#endif /* __cplusplus */


/*
 * ======== EPILOGUE ========
 */

#ifdef txn_clocks_Clock64P__top__
#undef __nested__
#endif

#endif /* txn_clocks_Clock64P__include */


/*
 * ======== STATE STRUCTURES ========
 */

#if defined(__config__) || (!defined(__nested__) && defined(txn_clocks_Clock64P__internalaccess))

#ifndef txn_clocks_Clock64P__include_state
#define txn_clocks_Clock64P__include_state

/* C++ wrapper */
#if defined(__cplusplus)
namespace txn_clocks { namespace Clock64P {


}}
#endif /* __cplusplus */

#endif /* txn_clocks_Clock64P__include_state */

#endif


/*
 * ======== PREFIX ALIASES ========
 */

#if !defined(__nested__) && !defined(txn_clocks_Clock64P__nolocalnames)

/* module prefix */
#define Clock64P_TimeValue txn_clocks_Clock64P_TimeValue
#define Clock64P_getTime txn_clocks_Clock64P_getTime
#define Clock64P_Module_name txn_clocks_Clock64P_Module_name
#define Clock64P_Module_id txn_clocks_Clock64P_Module_id
#define Clock64P_Module_startup txn_clocks_Clock64P_Module_startup
#define Clock64P_Module_startupDone txn_clocks_Clock64P_Module_startupDone
#define Clock64P_Module_hasMask txn_clocks_Clock64P_Module_hasMask
#define Clock64P_Module_getMask txn_clocks_Clock64P_Module_getMask
#define Clock64P_Module_setMask txn_clocks_Clock64P_Module_setMask
#define Clock64P_Object_heap txn_clocks_Clock64P_Object_heap
#define Clock64P_Module_heap txn_clocks_Clock64P_Module_heap
#define Clock64P_Module_upCast txn_clocks_Clock64P_Module_upCast
#define Clock64P_Module_to_acme_utils_clocks_IClock txn_clocks_Clock64P_Module_to_acme_utils_clocks_IClock

/* C++ wrapper */
#if defined(__cplusplus) && !defined(txn_clocks_Clock64P__INTERNAL__)
namespace Clock64P = txn_clocks::Clock64P;
#endif

#endif


/*
 * ======== C++ SUPPLIER WRAPPER [experimental] ========
 */

#if defined(__cplusplus) && defined(txn_clocks_Clock64P__INTERNAL__)
namespace Clock64P {
    using txn_clocks::Clock64P::TimeValue;

/* Clock64P::Module_startup */
inline int Module_startup( int state );
#define DEFINE__Module_startup\
    int txn_clocks_Clock64P_Module_startup( int state )\
    {\
        return Clock64P::Module_startup(state);\
    }\

/* Clock64P::getTime */
inline acme_utils_clocks_IClock_TimeValue getTime(  );
#define DEFINE__getTime\
    acme_utils_clocks_IClock_TimeValue txn_clocks_Clock64P_getTime( void )\
    {\
        return Clock64P::getTime();\
    }\

}
#endif /* __cplusplus */
