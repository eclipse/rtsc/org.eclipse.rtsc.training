/*
 *  ======== txn/clocks/Clock64P.c ========
 */

#include <xdc/runtime/Startup.h>
#include "package/internal/Clock64P.xdc.h"
 
extern volatile cregister unsigned int TSCL;
 
Int Clock64P_Module_startup(Int state)
{
    TSCL = 0;
    return Startup_DONE;
}
 
Clock64P_TimeValue Clock64P_getTime()
{
    return (Clock64P_TimeValue) TSCL;
}
