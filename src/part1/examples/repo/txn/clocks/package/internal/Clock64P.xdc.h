/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

#ifndef txn_clocks_Clock64P__INTERNAL__
#define txn_clocks_Clock64P__INTERNAL__

#ifndef txn_clocks_Clock64P__internalaccess
#define txn_clocks_Clock64P__internalaccess
#endif

#include <txn/clocks/Clock64P.h>

#undef xdc_FILE__
#ifndef xdc_FILE
#define xdc_FILE__ NULL
#else
#define xdc_FILE__ xdc_FILE
#endif

/* getTime */
#undef txn_clocks_Clock64P_getTime
#define txn_clocks_Clock64P_getTime txn_clocks_Clock64P_getTime__F

/* Module_startup */
#undef txn_clocks_Clock64P_Module_startup
#define txn_clocks_Clock64P_Module_startup txn_clocks_Clock64P_Module_startup__F

/* Instance_init */
#undef txn_clocks_Clock64P_Instance_init
#define txn_clocks_Clock64P_Instance_init txn_clocks_Clock64P_Instance_init__F

/* Instance_finalize */
#undef txn_clocks_Clock64P_Instance_finalize
#define txn_clocks_Clock64P_Instance_finalize txn_clocks_Clock64P_Instance_finalize__F

/* per-module runtime symbols */
#undef Module__MID
#define Module__MID txn_clocks_Clock64P_Module__id__C
#undef Module__DGSINCL
#define Module__DGSINCL txn_clocks_Clock64P_Module__diagsIncluded__C
#undef Module__DGSENAB
#define Module__DGSENAB txn_clocks_Clock64P_Module__diagsEnabled__C
#undef Module__DGSMASK
#define Module__DGSMASK txn_clocks_Clock64P_Module__diagsMask__C
#undef Module__LOGDEF
#define Module__LOGDEF txn_clocks_Clock64P_Module__loggerDefined__C
#undef Module__LOGOBJ
#define Module__LOGOBJ txn_clocks_Clock64P_Module__loggerObj__C
#undef Module__LOGFXN4
#define Module__LOGFXN4 txn_clocks_Clock64P_Module__loggerFxn4__C
#undef Module__LOGFXN8
#define Module__LOGFXN8 txn_clocks_Clock64P_Module__loggerFxn8__C
#undef Module__G_OBJ
#define Module__G_OBJ txn_clocks_Clock64P_Module__gateObj__C
#undef Module__G_PRMS
#define Module__G_PRMS txn_clocks_Clock64P_Module__gatePrms__C
#undef Module__GP_create
#define Module__GP_create txn_clocks_Clock64P_Module_GateProxy_create
#undef Module__GP_delete
#define Module__GP_delete txn_clocks_Clock64P_Module_GateProxy_delete
#undef Module__GP_enter
#define Module__GP_enter txn_clocks_Clock64P_Module_GateProxy_enter
#undef Module__GP_leave
#define Module__GP_leave txn_clocks_Clock64P_Module_GateProxy_leave
#undef Module__GP_query
#define Module__GP_query txn_clocks_Clock64P_Module_GateProxy_query


#endif /* txn_clocks_Clock64P__INTERNAL____ */
