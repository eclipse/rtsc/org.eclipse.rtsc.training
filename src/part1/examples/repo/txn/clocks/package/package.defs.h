/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */

#ifndef txn_clocks__
#define txn_clocks__


/*
 * ======== module txn.clocks.Clock64P ========
 */

typedef struct txn_clocks_Clock64P_Fxns__ txn_clocks_Clock64P_Fxns__;
typedef const txn_clocks_Clock64P_Fxns__* txn_clocks_Clock64P_Module;


#endif /* txn_clocks__ */ 
