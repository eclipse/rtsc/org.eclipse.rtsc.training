/*
 *  Do not modify this file; it is automatically 
 *  generated and any modifications will be overwritten.
 *
 * @(#) xdc-t32
 */
import java.util.*;
import org.mozilla.javascript.*;
import xdc.services.intern.xsr.*;
import xdc.services.spec.*;

public class txn_clocks
{
    static final String VERS = "@(#) xdc-t32\n";

    static final Proto.Elm $$T_Bool = Proto.Elm.newBool();
    static final Proto.Elm $$T_Num = Proto.Elm.newNum();
    static final Proto.Elm $$T_Str = Proto.Elm.newStr();
    static final Proto.Elm $$T_Obj = Proto.Elm.newObj();

    static final Proto.Fxn $$T_Met = new Proto.Fxn(null, null, 0, -1, false);
    static final Proto.Map $$T_Map = new Proto.Map($$T_Obj);
    static final Proto.Arr $$T_Vec = new Proto.Arr($$T_Obj);

    static final XScriptO $$DEFAULT = Value.DEFAULT;
    static final Object $$UNDEF = Undefined.instance;

    static final Proto.Obj $$Package = (Proto.Obj)Global.get("$$Package");
    static final Proto.Obj $$Module = (Proto.Obj)Global.get("$$Module");
    static final Proto.Obj $$Instance = (Proto.Obj)Global.get("$$Instance");
    static final Proto.Obj $$Params = (Proto.Obj)Global.get("$$Params");

    static final Object $$objFldGet = Global.get("$$objFldGet");
    static final Object $$objFldSet = Global.get("$$objFldSet");
    static final Object $$proxyGet = Global.get("$$proxyGet");
    static final Object $$proxySet = Global.get("$$proxySet");
    static final Object $$delegGet = Global.get("$$delegGet");
    static final Object $$delegSet = Global.get("$$delegSet");

    Scriptable xdcO;
    Session ses;
    Value.Obj om;

    boolean isROV;
    boolean isCFG;

    Proto.Obj pkgP;
    Value.Obj pkgV;

    ArrayList<Object> imports = new ArrayList<Object>();
    ArrayList<Object> loggables = new ArrayList<Object>();
    ArrayList<Object> mcfgs = new ArrayList<Object>();
    ArrayList<Object> proxies = new ArrayList<Object>();
    ArrayList<Object> sizes = new ArrayList<Object>();
    ArrayList<Object> tdefs = new ArrayList<Object>();

    void $$IMPORTS()
    {
        Global.callFxn("loadPackage", xdcO, "acme.utils.clocks");
        Global.callFxn("loadPackage", xdcO, "xdc");
        Global.callFxn("loadPackage", xdcO, "xdc.corevers");
    }

    void $$OBJECTS()
    {
        pkgP = (Proto.Obj)om.bind("txn.clocks.Package", new Proto.Obj());
        pkgV = (Value.Obj)om.bind("txn.clocks", new Value.Obj("txn.clocks", pkgP));
    }

    void Clock64P$$OBJECTS()
    {
        Proto.Obj po, spo;
        Value.Obj vo;

        po = (Proto.Obj)om.bind("txn.clocks.Clock64P.Module", new Proto.Obj());
        vo = (Value.Obj)om.bind("txn.clocks.Clock64P", new Value.Obj("txn.clocks.Clock64P", po));
        pkgV.bind("Clock64P", vo);
        // decls 
    }

    void Clock64P$$CONSTS()
    {
        // module Clock64P
        om.bind("txn.clocks.Clock64P.getTime", new Extern("txn_clocks_Clock64P_getTime__E", "xdc_Bits32(*)(xdc_Void)", true, false, "txn.clocks.Clock64P.getTime"));
    }

    void Clock64P$$CREATES()
    {
        Proto.Fxn fxn;
        StringBuilder sb;

    }

    void Clock64P$$FUNCTIONS()
    {
        Proto.Fxn fxn;

    }

    void Clock64P$$SIZES()
    {
        Proto.Str so;
        Object fxn;

    }

    void Clock64P$$TYPES()
    {
        Scriptable cap;
        Proto.Obj po;
        Proto.Str ps;
        Proto.Typedef pt;
        Object fxn;

        cap = (Scriptable)Global.callFxn("loadCapsule", xdcO, "txn/clocks/Clock64P.xs");
        om.bind("txn.clocks.Clock64P$$capsule", cap);
        po = (Proto.Obj)om.find("txn.clocks.Clock64P.Module");
        po.init("txn.clocks.Clock64P.Module", om.find("acme.utils.clocks.IClock.Module"));
                po.addFld("$hostonly", $$T_Num, 0, "r");
        if (isCFG) {
        }//isCFG
                fxn = Global.get(cap, "module$use");
                if (fxn != null) om.bind("txn.clocks.Clock64P$$module$use", true);
                if (fxn != null) po.addFxn("module$use", $$T_Met, fxn);
                fxn = Global.get(cap, "module$meta$init");
                if (fxn != null) om.bind("txn.clocks.Clock64P$$module$meta$init", true);
                if (fxn != null) po.addFxn("module$meta$init", $$T_Met, fxn);
                fxn = Global.get(cap, "module$static$init");
                if (fxn != null) om.bind("txn.clocks.Clock64P$$module$static$init", true);
                if (fxn != null) po.addFxn("module$static$init", $$T_Met, fxn);
                fxn = Global.get(cap, "module$validate");
                if (fxn != null) om.bind("txn.clocks.Clock64P$$module$validate", true);
                if (fxn != null) po.addFxn("module$validate", $$T_Met, fxn);
    }

    void Clock64P$$ROV()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("txn.clocks.Clock64P");
    }

    void $$SINGLETONS()
    {
        pkgP.init("txn.clocks.Package", (Proto.Obj)om.find("xdc.IPackage.Module"));
        pkgP.bind("$capsule", $$UNDEF);
        pkgV.init2(pkgP, "txn.clocks", Value.DEFAULT, false);
        pkgV.bind("$name", "txn.clocks");
        pkgV.bind("$category", "Package");
        pkgV.bind("$$qn", "txn.clocks.");
        pkgV.bind("$vers", Global.newArray());
        Value.Map atmap = (Value.Map)pkgV.getv("$attr");
        atmap.seal("length");
        imports.clear();
        imports.add(Global.newArray("acme.utils.clocks", Global.newArray()));
        pkgV.bind("$imports", imports);
        StringBuilder sb = new StringBuilder();
        sb.append("var pkg = xdc.om['txn.clocks'];\n");
        sb.append("if (pkg.$vers.length >= 3) {\n");
            sb.append("pkg.$vers.push(Packages.xdc.services.global.Vers.getDate(xdc.csd() + '/..'));\n");
        sb.append("}\n");
        sb.append("pkg.build.libraries = [\n");
            sb.append("'lib/txn.clocks.a64P',\n");
        sb.append("];\n");
        sb.append("pkg.build.libDesc = [\n");
            sb.append("['lib/txn.clocks.a64P', {target: 'ti.targets.C64P'}],\n");
        sb.append("];\n");
        sb.append("if('suffix' in xdc.om['xdc.IPackage$$LibDesc']) {\n");
            sb.append("pkg.build.libDesc['lib/txn.clocks.a64P'].suffix = '64P';\n");
        sb.append("}\n");
        Global.eval(sb.toString());
    }

    void Clock64P$$SINGLETONS()
    {
        Proto.Obj po;
        Value.Obj vo;

        vo = (Value.Obj)om.find("txn.clocks.Clock64P");
        po = (Proto.Obj)om.find("txn.clocks.Clock64P.Module");
        vo.init2(po, "txn.clocks.Clock64P", $$DEFAULT, false);
        vo.bind("Module", po);
        vo.bind("$category", "Module");
        vo.bind("$capsule", om.find("txn.clocks.Clock64P$$capsule"));
        vo.bind("$package", om.find("txn.clocks"));
        tdefs.clear();
        proxies.clear();
        mcfgs.clear();
        mcfgs.add("Module__diagsEnabled");
        mcfgs.add("Module__diagsIncluded");
        mcfgs.add("Module__diagsMask");
        mcfgs.add("Module__gateObj");
        mcfgs.add("Module__gatePrms");
        mcfgs.add("Module__id");
        mcfgs.add("Module__loggerDefined");
        mcfgs.add("Module__loggerObj");
        mcfgs.add("Module__loggerFxn4");
        mcfgs.add("Module__loggerFxn8");
        mcfgs.add("Module__startupDoneFxn");
        mcfgs.add("Object__count");
        mcfgs.add("Object__heap");
        mcfgs.add("Object__sizeof");
        mcfgs.add("Object__table");
        vo.bind("TimeValue", om.find("acme.utils.clocks.IClock.TimeValue"));
        vo.bind("$$tdefs", Global.newArray(tdefs.toArray()));
        vo.bind("$$proxies", Global.newArray(proxies.toArray()));
        vo.bind("$$mcfgs", Global.newArray(mcfgs.toArray()));
        ((Value.Arr)pkgV.getv("$modules")).add(vo);
        ((Value.Arr)om.find("$modules")).add(vo);
        vo.bind("$$instflag", 0);
        vo.bind("$$iobjflag", 0);
        vo.bind("$$sizeflag", 1);
        vo.bind("$$dlgflag", 0);
        vo.bind("$$iflag", 1);
        vo.bind("$$romcfgs", "|");
        if (isCFG) {
            Proto.Str ps = (Proto.Str)vo.find("Module_State");
            if (ps != null) vo.bind("$object", ps.newInstance());
            vo.bind("$$meta_iobj", 1);
        }//isCFG
        vo.bind("getTime", om.find("txn.clocks.Clock64P.getTime"));
        vo.bind("$$fxntab", Global.newArray("txn_clocks_Clock64P_Handle__label__E", "txn_clocks_Clock64P_Module__startupDone__E", "txn_clocks_Clock64P_Object__create__E", "txn_clocks_Clock64P_Object__delete__E", "txn_clocks_Clock64P_Object__destruct__E", "txn_clocks_Clock64P_Object__get__E", "txn_clocks_Clock64P_Object__first__E", "txn_clocks_Clock64P_Object__next__E", "txn_clocks_Clock64P_Params__init__E", "txn_clocks_Clock64P_Proxy__abstract__E", "txn_clocks_Clock64P_Proxy__delegate__E", "txn_clocks_Clock64P_getTime__E"));
        vo.bind("$$logEvtCfgs", Global.newArray());
        vo.bind("$$errorDescCfgs", Global.newArray());
        vo.bind("$$assertDescCfgs", Global.newArray());
        Value.Map atmap = (Value.Map)vo.getv("$attr");
        atmap.setElem("", true);
        atmap.seal("length");
        vo.bind("MODULE_STARTUP$", 1);
        vo.bind("PROXY$", 0);
        loggables.clear();
        loggables.add(Global.newObject("name", "getTime", "entry", "", "exit", "%d"));
        vo.bind("$$loggables", loggables.toArray());
        pkgV.bind("Clock64P", vo);
        ((Value.Arr)pkgV.getv("$unitNames")).add("Clock64P");
    }

    void $$INITIALIZATION()
    {
        Value.Obj vo;

        if (isCFG) {
        }//isCFG
        Global.callFxn("module$meta$init", (Scriptable)om.find("txn.clocks.Clock64P"));
        Global.callFxn("init", pkgV);
        ((Value.Obj)om.getv("txn.clocks.Clock64P")).bless();
        ((Value.Arr)om.find("$packages")).add(pkgV);
    }

    public void exec( Scriptable xdcO, Session ses )
    {
        this.xdcO = xdcO;
        this.ses = ses;
        om = (Value.Obj)xdcO.get("om", null);

        Object o = om.geto("$name");
        String s = o instanceof String ? (String)o : null;
        isCFG = s != null && s.equals("cfg");
        isROV = s != null && s.equals("rov");

        $$IMPORTS();
        $$OBJECTS();
        Clock64P$$OBJECTS();
        Clock64P$$CONSTS();
        Clock64P$$CREATES();
        Clock64P$$FUNCTIONS();
        Clock64P$$SIZES();
        Clock64P$$TYPES();
        if (isROV) {
            Clock64P$$ROV();
        }//isROV
        $$SINGLETONS();
        Clock64P$$SINGLETONS();
        $$INITIALIZATION();
    }
}
