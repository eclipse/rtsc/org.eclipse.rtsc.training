/*
 *  ======== txn/clocks/Clock64P.xdc ========
 */

import acme.utils.clocks.IClock;
 
/*! TMS320C64+ IClock module */
@ModuleStartup
module Clock64P inherits IClock {
    /* no additional features or internal details */
};
