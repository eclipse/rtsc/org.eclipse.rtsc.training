/*
 *  ======== txn/clocks/package.xdc ========
 */

requires acme.utils.clocks; 

/*! TI-specific clock modules [RTSC Interface Primer] */
package txn.clocks {
    module Clock64P;
}
