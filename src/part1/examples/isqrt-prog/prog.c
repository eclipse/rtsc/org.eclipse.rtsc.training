/*
 *  ======== isqrt-prog/prog.c ========
 */

#include <acme/utils/Bench.h>
#include <charlie/sqrtlib/isqrt.h>
#include <xdc/runtime/System.h>
 
int main()
{
    unsigned int x, y;

    x = 1000;
    System_printf("==> isqrt(%d)\n", x);

    Bench_begin("cycle count:");
    y = isqrt(x);
    Bench_end();

    System_printf("<== %d\n", y);
    return 0;
}
